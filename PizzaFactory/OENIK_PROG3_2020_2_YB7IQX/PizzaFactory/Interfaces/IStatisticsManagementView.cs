﻿// <copyright file="IStatisticsManagementView.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace PizzaFactory.Program.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PizzaFactory.Logic.Classes;

    /// <summary>
    /// Organizes the Console GUI operatios regarding the statistics management processes.
    /// </summary>
    public interface IStatisticsManagementView
    {
        /// <summary>
        /// Manages the GUI part of the selection process.
        /// Shows the possible operations (queries) that can be selected and executed.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenToggle">Value of the toggle button, defines whether we show the async or basic menu.</param>
        void ViewStatistics(string[] givenArgs, StatisticsManagement givenLogic, bool givenToggle);

        /// <summary>
        /// Manages the GUI part of the selection process.
        /// Simply executes and prints the result of the selected query.
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        void RenewableMoney(StatisticsManagement givenLogic);

        /// <summary>
        /// Async method!
        /// Manages the GUI part of the selection process.
        /// Simply executes and prints the result of the selected query.
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        void RenewableMoneyAsync(StatisticsManagement givenLogic);

        /// <summary>
        /// Manages the GUI part of the selection process.
        /// Based on the user's input executes and prints the result of the query.
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        void PizzaContains(StatisticsManagement givenLogic);

        /// <summary>
        /// Async method!
        /// Manages the GUI part of the selection process.
        /// Based on the user's input executes and prints the result of the query.
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        void PizzaContainsAsync(StatisticsManagement givenLogic);

        /// <summary>
        /// Manages the GUI part of the selection process.
        /// Based on the user's input executes and prints the result of the query.
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        void PizzaToppings(StatisticsManagement givenLogic);

        /// <summary>
        /// Async method!
        /// Manages the GUI part of the selection process.
        /// Based on the user's input executes and prints the result of the query.
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        void PizzaToppingsAsync(StatisticsManagement givenLogic);
    }
}
