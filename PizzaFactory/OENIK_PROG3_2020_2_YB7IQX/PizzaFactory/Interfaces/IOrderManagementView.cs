﻿// <copyright file="IOrderManagementView.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Program.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Data.Models.Human;
    using PizzaFactory.Logic.Classes;

    /// <summary>
    /// Organizes the Console GUI operations regarding the ordering process.
    /// </summary>
    public interface IOrderManagementView
    {
        /// <summary>
        /// Manages the GUI part of the customer selection process.
        /// Selects an already existing customer. If the phone number belongs to a person who is not a customer yet the user can extend that person to become a customer.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        void SelectCustomer(string[] givenArgs, OrderManagement givenLogic);

        /// <summary>
        /// Manages the GUI part of the customer addition process.
        /// Creates a new customer based on the provided person data.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenPerson">Based on what person data should we create the customer.</param>
        void AddCustomer(string[] givenArgs, OrderManagement givenLogic, Person givenPerson);

        /// <summary>
        /// Manages the GUI part of the customer finalization process.
        /// Prompts whether the user wants to continue with the selected customer or not.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenCustomer">For what customer should we continue the order process.</param>
        void ContinueWithCustomer(string[] givenArgs, OrderManagement givenLogic, Customer givenCustomer);

        /// <summary>
        /// Manages the GUI part of the pizza selection process.
        /// Prints the entire menu on the screen from which the user can select the pizzas.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        void SelectPizzas(string[] givenArgs, OrderManagement givenLogic);

        /// <summary>
        /// Manages the GUI part of the shopping cart management process.
        /// Divided into two suboperations: set and get.
        /// Where 'set' means we add a new item (pizza) to the cart.
        /// Where 'get' means we retrieve all (current) elements of the cart.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenFunc">Which inside function we want to use this methode for.</param>
        /// <param name="givenItem">Selected pizza item that should be added to the order.</param>
        void ManageShoppingCart(string[] givenArgs, OrderManagement givenLogic, string givenFunc, Pizza givenItem);

        /// <summary>
        /// Manages the GUI part of the order finalization process.
        /// The user decides whether the order is correct or not.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        void OrderCheckout(string[] givenArgs, OrderManagement givenLogic);

        /// <summary>
        /// Manages the GUI part of the order finalization process.
        /// Prints the entire order to the screen.
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        void OrderPreview(OrderManagement givenLogic);

        /// <summary>
        /// Manages the GUI part of the order overview process.
        /// Prints all the orders to the screen.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        void ViewOrders(string[] givenArgs, OrderManagement givenLogic);
    }
}
