﻿// <copyright file="IUserManagementView.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Program.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PizzaFactory.Data.Models.Human;
    using PizzaFactory.Logic.Classes;

    /// <summary>
    /// Organizes the Console GUI operations regarding the user management process.
    /// </summary>
    public interface IUserManagementView
    {
        /// <summary>
        /// Manages the GUI part of the employee management process.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        void ManageUsers(string[] givenArgs, UserManagement givenLogic);

        /// <summary>
        /// Manages the GUI part of the employee modification process.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenEmployee">Based on what employee data should we modify the employee.</param>
        void ModifyEmployee(string[] givenArgs, UserManagement givenLogic, Employee givenEmployee);

        /// <summary>
        /// Manages the GUI part of the employee's role modification process.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenEmployee">Based on what employee data should we modify the employee.</param>
        void ModifyEmployeeRole(string[] givenArgs, UserManagement givenLogic, Employee givenEmployee);

        /// <summary>
        /// Manages the GUI part of the employee's personal data modification process.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenEmployee">Based on what employee data should we modify the employee.</param>
        void ModifyEmployeePerson(string[] givenArgs, UserManagement givenLogic, Employee givenEmployee);

        /// <summary>
        /// Manages the GUI part of the employee's personal data modification process.
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenEmployee">Based on what employee data should we modify the employee.</param>
        /// <param name="givenFunc">Predefined function determining what is going to be done.</param>
        void ModifyEmployeePersonPart(UserManagement givenLogic, Employee givenEmployee, string givenFunc);

        /// <summary>
        /// /// Manages the GUI part of the sucessfull operation message.
        /// </summary>
        /// <param name="givenMessage">Given sentence we would like to print out in green as sucess result.</param>
        void SuccessfullOperationMessage(string givenMessage);

        /// <summary>
        /// Manages the GUI part of the employee creation process.
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        void AddEmployee(UserManagement givenLogic);
    }
}
