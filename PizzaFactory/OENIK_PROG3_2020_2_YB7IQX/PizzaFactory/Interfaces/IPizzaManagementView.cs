﻿// <copyright file="IPizzaManagementView.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Program.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Logic.Classes;

    /// <summary>
    /// Organizes the Console GUI operatios regarding the pizza management processes.
    /// </summary>
    public interface IPizzaManagementView
    {
        /// <summary>
        /// Manages the GUI part of the selection process.
        /// Selects an already existing Pizza which can be modified or removed later on.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        void ViewPizzas(string[] givenArgs, PizzaManagement givenLogic);

        /// <summary>
        /// Manages the GUI part of the selected process.
        /// The user can chose the required option from a list, where he or she also has the possiblity,
        /// to completely remove an item.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenItem">Which item we want to change.</param>
        void ModifyOptions(string[] givenArgs, PizzaManagement givenLogic, Pizza givenItem);

        /// <summary>
        /// Manages the GUI part of the selected process.
        /// Based on the provided func and its value the program tries to perform the operation.
        /// If something goes wrong the program will notify the user and prompt for correction.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenItem">Which item we want to change.</param>
        /// <param name="givenFunc">Which operation we want to perform, get or set.</param>
        void ModifyProcess(string[] givenArgs, PizzaManagement givenLogic, Pizza givenItem, string givenFunc);

        /// <summary>
        /// Manages the GUI part of the selected process.
        /// Guides the user through the process of a pizza creation.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        void AddPizza(string[] givenArgs, PizzaManagement givenLogic);

        /// <summary>
        /// Manages the GUI part of the selected process.
        /// After the successfull simple properties declaration, the user should choose the ingredients.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        void SelectIngredients(string[] givenArgs, PizzaManagement givenLogic);

        /// <summary>
        /// Manages the GUI part of the selected process.
        /// Guides trough the selection of each ingredient.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenFunc">Which operation we want to perform, get or set.</param>
        /// <param name="givenItem">Which item we want to change.</param>
        void ManageIngredientSelection(string[] givenArgs, PizzaManagement givenLogic, string givenFunc, Ingredient givenItem);

        /// <summary>
        /// Manages the GUI part of the selection process.
        /// Assembles the provided data into one finalized object that is being printed.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        void FinalizePizzaAddition(string[] givenArgs, PizzaManagement givenLogic);

        /// <summary>
        /// Manage the GUI part of the selection process.
        /// Prints the provided data to the screen.
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        void PizzaAdditionPreview(PizzaManagement givenLogic);

        /// <summary>
        /// Manages the GUI part of the selected process.
        /// Simply prints the specific successfull xy message to the console as a response.
        /// </summary>
        /// <param name="givenMessage">Message to be printed.</param>
        void SuccessfullOperationMessage(string givenMessage);
    }
}
