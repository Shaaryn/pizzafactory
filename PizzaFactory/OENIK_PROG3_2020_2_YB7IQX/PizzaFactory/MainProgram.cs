﻿// <copyright file="MainProgram.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text.Json;
    using ConsoleTools;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Data.Models.Human;
    using PizzaFactory.Logic.Classes;
    using PizzaFactory.Logic.Exceptions;
    using PizzaFactory.Program.Classes;
    using PizzaFactory.Repository.Classes;

    // public class Pizzaa
    // {
    //    public string Name { get; set; }

    // public int Size { get; set; }

    // public int Price { get; set; }

    // public float FurtherExpenses { get; set; }

    // public float SaleMultiplier { get; set; }

    // public override string ToString()
    //    {
    //        return $"{this.Name} - {this.Size}cm - HUF{this.Price} - {this.FurtherExpenses}f - {this.SaleMultiplier}f";
    //    }
    // }

    /// <summary>
    /// .
    /// </summary>
    public static class MainProgram
    {
        private static Employee user;
        private static PizzaFactoryDbContext ctx;

        /// <summary>
        /// Main menu of the application.
        /// In order to see and be able to use a user must log in first.
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        public static void MainMenu(string[] givenArgs)
        {
            Console.Clear();
            var mainMenu = new ConsoleMenu(givenArgs, level: 1).
                Add("Use API", () => IsAuthorized(givenArgs, "api")).
                Add("View Orders", () => IsAuthorized(givenArgs, "vo")).
                Add("Create Order", () => IsAuthorized(givenArgs, "co")).
                Add("Manage Users", () => IsAuthorized(givenArgs, "mu")).
                Add("Manage Pizzas", () => IsAuthorized(givenArgs, "mp")).
                Add("Statistics", () => IsAuthorized(givenArgs, "st")).
                Add("Log out", () => Main(givenArgs)).
                Configure(c =>
                {
                    c.Selector = " => ";
                    c.Title = $" Logged in as {user.Person.FullName} (Role : {user.RoleOfEmployee})";
                    c.EnableWriteTitle = true;
                });

            mainMenu.Show();
        }

        private static void Main(string[] args)
        {
            ctx = new PizzaFactoryDbContext();

            EmployeeRepository empRep = new EmployeeRepository(ctx);
            LogIn logInLogic = new LogIn(empRep);

            var menu = new ConsoleMenu(args, level: 0).
                    Add(" Log In ", () => LogInUser(args, logInLogic)).
                    Add(" Quit ", () => Environment.Exit(0)).
                    Configure(c =>
                    {
                        c.Title = "Log In panel -- Please log in to your account!\n" +
                        "protip #1: Use 'admin' which account lets you use al lthe options.\n" +
                        "protip #2: Use 'Bogdán Árpád' which account can only acces the order related options.";
                        c.EnableWriteTitle = true;
                    });
            menu.Show();
        }

        private static void LogInUser(string[] givenArgs, LogIn givenLogic)
        {
            user = new Employee();
            bool isLoggedIn = false;

            Console.WriteLine("Welcome to the PizzaFactoy log in page! Please provide your crediential(s) below.");
            Console.WriteLine("(Only your username for now)");

            do
            {
                string userName = Console.ReadLine();

                try
                {
                    user = givenLogic.GetOnePerson(userName);

                    if (user != null)
                    {
                        isLoggedIn = true;
                    }
                }
                catch (LogInEmployeeNotFoundException e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("#" + e.Message);
                    Console.WriteLine("Incorrect username!");
                    Console.ResetColor();
                    Console.WriteLine("Please enter your username: ");
                }
            }
            while (!isLoggedIn);

            MainMenu(givenArgs);
        }

        private static void IsAuthorized(string[] givenArgs, string optionAcr)
        {
            if (user != null)
            {
                PizzeriaRepository pizzeriaRepo = new PizzeriaRepository(ctx);
                EmployeeRepository empoloyeeRepo = new EmployeeRepository(ctx);
                PersonRepository perRep = new PersonRepository(ctx);
                CustomerRepository cusRep = new CustomerRepository(ctx);
                IngredientRepository ingRep = new IngredientRepository(ctx);
                PizzaRepository pizRep = new PizzaRepository(ctx);
                OrderRepository ordRep = new OrderRepository(ctx);
                ConnectorOrderPizzaRepository copRepo = new ConnectorOrderPizzaRepository(ctx);
                ConnectorPizzaIngredientRepository cpiRepo = new ConnectorPizzaIngredientRepository(ctx);

                OrderManagement orderLogic = new OrderManagement(pizzeriaRepo, perRep, cusRep, pizRep, ordRep, copRepo, user);
                OrderManagementView orderView = new OrderManagementView();

                UserManagement userLogic = new UserManagement(empoloyeeRepo, perRep);
                UserManagementView userView = new UserManagementView();

                StatisticsManagement statLogic = new StatisticsManagement(pizzeriaRepo, perRep, cusRep, pizRep, empoloyeeRepo, ordRep, ingRep, copRepo, cpiRepo);
                StatisticsManagementView statView = new StatisticsManagementView();

                PizzaManagement pizLogic = new PizzaManagement(pizRep, ingRep, cpiRepo, copRepo);
                PizzaManagementView pizView = new PizzaManagementView();

                switch (optionAcr)
                {
                    case "api":
                        // string url = "https://localhost:44309/PizzaApi";
                        // Console.WriteLine("Waiting...");
                        // Console.ReadKey();

                        // JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

                        // using (HttpClient client = new HttpClient())
                        // {
                        //    string json = client.GetStringAsync(url + "/all").Result;
                        //    var list = JsonSerializer.Deserialize<List<Pizzaa>>(json, jsonOptions);
                        //    foreach (var item in list)
                        //    {
                        //        Console.WriteLine(item);
                        //    }

                        // Console.ReadKey();

                        // Dictionary<string, string> postData;
                        //    string response;

                        // postData = new Dictionary<string, string>();
                        //    postData.Add(nameof(Pizzaa.Name), "NyalvaFalva pizza");
                        //    postData.Add(nameof(Pizzaa.Size), "32");
                        //    postData.Add(nameof(Pizzaa.Price), "2310");
                        //    postData.Add(nameof(Pizzaa.FurtherExpenses), "1");
                        //    postData.Add(nameof(Pizzaa.SaleMultiplier), "1.2");

                        // response = client.PostAsync(url + "/add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                        //    json = client.GetStringAsync(url + "/all").Result;
                        //    Console.WriteLine("ADD: " + response);
                        //    Console.WriteLine("ALL: " + json);
                        //    Console.ReadKey();
                        // }
                        break;
                    case "mu":
                        if (user.AccessLevelValue == (int)Role.Admin)
                        {
                            userView.ManageUsers(givenArgs, userLogic);
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("You are not authorized to use this function!");
                            Console.WriteLine("You will be returned in 2 seconds.");
                            System.Threading.Thread.Sleep(2000);
                            Console.ResetColor();
                            return;
                        }

                        break;
                    case "st":
                        if (user.AccessLevelValue <= (int)Role.Smanager)
                        {
                            statView.ViewStatistics(givenArgs, statLogic, false);
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("You are not authorized to use this function!");
                            Console.WriteLine("You will be returned in 2 seconds.");
                            System.Threading.Thread.Sleep(2000);
                            Console.ResetColor();
                            return;
                        }

                        break;
                    case "vo":
                        if (user.AccessLevelValue <= (int)Role.Assistant)
                        {
                            orderView.ViewOrders(givenArgs, orderLogic);
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("You are not authorized to use this function!");
                            Console.WriteLine("You will be returned in 2 seconds.");
                            System.Threading.Thread.Sleep(2000);
                            Console.ResetColor();
                            return;
                        }

                        break;
                    case "mp":
                        if (user.AccessLevelValue <= (int)Role.Smanager)
                        {
                            pizView.ViewPizzas(givenArgs, pizLogic);
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("You are not authorized to use this function!");
                            Console.WriteLine("You will be returned in 2 seconds.");
                            System.Threading.Thread.Sleep(2000);
                            Console.ResetColor();
                            return;
                        }

                        break;
                    case "co":
                        if (user.AccessLevelValue <= (int)Role.Cook)
                        {
                            var createOrderMenu = new ConsoleMenu(givenArgs, level: 2)
                                .Add(" Select Customer ", () => orderView.SelectCustomer(givenArgs, orderLogic))
                                .Add(" Add new Customer ", () => orderView.AddCustomer(givenArgs, orderLogic, null))
                                .Add(" Cancel ", ConsoleMenu.Close);
                            createOrderMenu.Show();
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("You are not authorized to use this function!");
                            Console.WriteLine("You will be returned in 2 seconds.");
                            System.Threading.Thread.Sleep(2000);
                            Console.ResetColor();
                            return;
                        }

                        break;
                }
            }
            else
            {
                throw new InvalidOperationException();
            }
        }
    }
}
