﻿// <copyright file="PizzaManagementView.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Program.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Text;
    using ConsoleTools;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Logic.Classes;
    using PizzaFactory.Program.Interfaces;
    using PizzaFactory.Repository.Exceptions;

    /// <summary>
    /// Implementation of the IPizzaManagementView interface.
    /// </summary>
    public class PizzaManagementView : IPizzaManagementView
    {
        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        public void ViewPizzas(string[] givenArgs, PizzaManagement givenLogic)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                var pizzaMenu = new ConsoleMenu(givenArgs, level: 2).
                    Configure(c =>
                    {
                        c.Title = "Please select the pizza below that you would like to modify.";
                        c.EnableWriteTitle = true;
                    });

                IList<Pizza> allPizzas = givenLogic.GetAllPizzas();

                foreach (Pizza item in allPizzas)
                {
                    pizzaMenu.Add($"{item.ToString()}", (thisMenu) =>
                    {
                        this.ModifyOptions(givenArgs, givenLogic, item);
                        thisMenu.CloseMenu();
                    });
                }

                pizzaMenu.Add("Add new pizza", (thisMenu) =>
                {
                    this.AddPizza(givenArgs, givenLogic);
                    this.ViewPizzas(givenArgs, givenLogic);
                    thisMenu.CloseMenu();
                }).
                Add("Back", ConsoleMenu.Close);
                pizzaMenu.Show();
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenItem">Which item we want to change.</param>
        public void ModifyOptions(string[] givenArgs, PizzaManagement givenLogic, Pizza givenItem)
        {
            if (givenLogic == null || givenItem == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                ConsoleMenu modifyMenu = new ConsoleMenu(givenArgs, level: 3).
                    Add($"Modify pizza name [{givenItem.Name}]", (thisMenu) =>
                    {
                        this.ModifyProcess(givenArgs, givenLogic, givenItem, "name");
                        thisMenu.CloseMenu();
                    }).
                    Add($"Modify further expenses [{givenItem.FurtherExpenses}]", (thisMenu) =>
                    {
                        this.ModifyProcess(givenArgs, givenLogic, givenItem, "expenses");
                        thisMenu.CloseMenu();
                    }).
                    Add($"Modify sale [{givenItem.SaleMultiplier}]", (thisMenu) =>
                    {
                        this.ModifyProcess(givenArgs, givenLogic, givenItem, "sale");
                        thisMenu.CloseMenu();
                    }).
                    Add($"Modify size [{givenItem.Size}]", (thisMenu) =>
                    {
                        this.ModifyProcess(givenArgs, givenLogic, givenItem, "size");
                        thisMenu.CloseMenu();
                    }).
                    Add("Remove this pizza", (thisMenu) =>
                    {
                        givenLogic.RemovePizza(givenItem);
                        this.SuccessfullOperationMessage("Pizza removed successfully!");
                        this.ViewPizzas(givenArgs, givenLogic);
                        thisMenu.CloseMenu();
                    }).
                    Add("Back", ConsoleMenu.Close).
                    Configure(c =>
                    {
                        c.Title = "Select an option below.";
                        c.EnableWriteTitle = true;
                    });
                modifyMenu.Show();
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenItem">Which item we want to change.</param>
        /// <param name="givenFunc">Which operation we want to perform, get or set.</param>
        public void ModifyProcess(string[] givenArgs, PizzaManagement givenLogic, Pizza givenItem, string givenFunc)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                bool isSuccessfull = false;
                do
                {
                    try
                    {
                        Console.WriteLine("Please provide a new value for {0}", givenFunc);
                        string newValue = Console.ReadLine();

                        givenLogic.ModifyPizza(givenItem, givenFunc, newValue);
                        isSuccessfull = true;
                    }
                    catch (UnderValueException e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine("Please try again!");
                    }
                    catch (ArgumentNullException e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine("Please try again!");
                    }
                }
                while (!isSuccessfull);

                this.SuccessfullOperationMessage($" The proeprty '{givenFunc}' was changed successfully.");

                this.ModifyOptions(givenArgs, givenLogic, givenItem);
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        public void AddPizza(string[] givenArgs, PizzaManagement givenLogic)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                Console.WriteLine("Please provide the pizza's properties below!");
                Console.WriteLine("(1/6) The name of the pizza:");
                givenLogic.NewPizza.Name = Console.ReadLine();
                Console.WriteLine("(2/6) Further expenses of the pizza:");
                givenLogic.NewPizza.FurtherExpenses = float.Parse(Console.ReadLine());
                Console.WriteLine("(3/6) Sale value of the pizza:");
                givenLogic.NewPizza.SaleMultiplier = float.Parse(Console.ReadLine());
                Console.WriteLine("(4/6) Size of the pizza:");
                givenLogic.NewPizza.Size = int.Parse(Console.ReadLine());
                Console.WriteLine("(5/6) Price of the pizza:");
                givenLogic.NewPizza.Price = int.Parse(Console.ReadLine());

                this.SelectIngredients(givenArgs, givenLogic);
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        public void SelectIngredients(string[] givenArgs, PizzaManagement givenLogic)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                IList<Ingredient> availableIngredients = givenLogic.GetAllIngredients();

                var selectIngredientsMenu = new ConsoleMenu(givenArgs, level: 3).
                    Add("Selected ingredients ->", () => this.ManageIngredientSelection(givenArgs, givenLogic, "get", null));

                foreach (Ingredient item in availableIngredients)
                {
                    selectIngredientsMenu.Add($"{item}", () => this.ManageIngredientSelection(givenArgs, givenLogic, "set", item));
                }

                selectIngredientsMenu.Add("Finish pizza addition process.", () => this.FinalizePizzaAddition(givenArgs, givenLogic)).
                    Configure(c =>
                    {
                        c.Title = "Select the ingredients that you want to place on the pizza as a topping.";
                        c.EnableWriteTitle = true;
                    });
                selectIngredientsMenu.Show();
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenFunc">Which operation we want to perform, get or set.</param>
        /// <param name="givenItem">Which item we want to change.</param>
        public void ManageIngredientSelection(string[] givenArgs, PizzaManagement givenLogic, string givenFunc, Ingredient givenItem)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                switch (givenFunc)
                {
                    case "get":
                        Collection<Ingredient> currentIngredients = givenLogic.GetSelectedIngredients();

                        var selectedItemsMenu = new ConsoleMenu(givenArgs, level: 4).
                            Configure(c =>
                            {
                                c.Title = "Here you can see the already selected items.\n" +
                                    "If you wish to remove an item, just simply select them.";
                                c.EnableWriteTitle = true;
                            });

                        for (int i = 0; i < currentIngredients.Count; i++)
                        {
                            selectedItemsMenu.Add($"{currentIngredients[i].Name}", (thisMenu) =>
                            {
                                givenLogic.RemoveSelectedItem(thisMenu.CurrentItem.Index);
                                this.ManageIngredientSelection(givenArgs, givenLogic, "get", null);
                                thisMenu.CloseMenu();
                            });
                        }

                        selectedItemsMenu.Add("Back", selectedItemsMenu.CloseMenu);
                        selectedItemsMenu.Show();
                        break;
                    case "set":

                        givenLogic.AddSelectedItem(givenItem);

                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine(" Item added successfully! ");
                        Console.ResetColor();
                        System.Threading.Thread.Sleep(1250);
                        break;
                }
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        public void FinalizePizzaAddition(string[] givenArgs, PizzaManagement givenLogic)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else if (givenLogic.GetSelectedIngredients().Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("You should not create pizzas without ingredients.");
                Console.WriteLine("Hit enter to return.");
                Console.ResetColor();
                Console.ReadKey();

                this.SelectIngredients(givenArgs, givenLogic);
            }
            else
            {
                var finalizePizza = new ConsoleMenu(givenArgs, level: 5).
                    Add("Show pizza", () =>
                    {
                        this.PizzaAdditionPreview(givenLogic);
                        this.FinalizePizzaAddition(givenArgs, givenLogic);
                    }).
                    Add("Create the pizza", () =>
                    {
                        givenLogic.SubmitNewPizza();
                        MainProgram.MainMenu(givenArgs);    // PUBLIC STATIC FUNCTION
                    }).
                    Add("No, let me change some ingredients.", () => this.SelectIngredients(givenArgs, givenLogic)).
                    Configure(c =>
                    {
                        c.Title = "Here you can see a sumamry of the assembled pizza.\n" +
                                  "Please decide whether it is correct or not.";
                        c.EnableWriteTitle = true;
                    });

                finalizePizza.Show();
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        public void PizzaAdditionPreview(PizzaManagement givenLogic)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                Console.WriteLine("Here you can see what values you provided for each proeprty:");
                Console.WriteLine("---Pizza name :" + givenLogic.NewPizza.Name);
                Console.WriteLine("---Pizza size :" + givenLogic.NewPizza.Size);
                Console.WriteLine("---Pizza further expenses :" + givenLogic.NewPizza.FurtherExpenses);
                Console.WriteLine("---Pizza sale multiplier :" + givenLogic.NewPizza.SaleMultiplier);
                Console.WriteLine("---Pizza price :" + givenLogic.NewPizza.Price);
                Collection<Ingredient> selectedIngredients = givenLogic.GetSelectedIngredients();
                foreach (Ingredient item in selectedIngredients)
                {
                    Console.WriteLine($"---{item}");
                }

                Console.WriteLine("Hit enter to return.");
                Console.ReadKey();
                return;
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenMessage">Message to be printed.</param>
        public void SuccessfullOperationMessage(string givenMessage)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(givenMessage);
            Console.ResetColor();
            System.Threading.Thread.Sleep(1250);
        }
    }
}
