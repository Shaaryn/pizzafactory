﻿// <copyright file="OrderManagementView.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Program.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Text;
    using ConsoleTools;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Data.Models.Human;
    using PizzaFactory.Logic.Classes;
    using PizzaFactory.Program.Interfaces;

    /// <summary>
    /// Implementation of the IOrderManagementView interface.
    /// </summary>
    public class OrderManagementView : IOrderManagementView
    {
        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        public void SelectCustomer(string[] givenArgs, OrderManagement givenLogic)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                Console.WriteLine("** SelectCustomer **");
                Console.WriteLine("Enter the customer's phone number below:");
                string phoneNumber = Console.ReadLine();

                Customer selectedCus;
                Person selectedPerson;
                (selectedCus, selectedPerson) = givenLogic.SearchCustomer(phoneNumber);

                if (selectedCus != null)
                {
                    this.ContinueWithCustomer(givenArgs, givenLogic, selectedCus);
                }
                else if (selectedPerson != null)
                {
                    var yesNOQuestion = new ConsoleMenu(givenArgs, level: 3).
                        Add("Yes, create the customer!", (thisMenu) =>
                        {
                            this.AddCustomer(givenArgs, givenLogic, selectedPerson);
                            thisMenu.CloseMenu();
                        }).
                        Add("No, let me go back.", ConsoleMenu.Close).
                        Configure(c =>
                        {
                            c.Title = "The customer you want to access does not exists! \n" +
                            "But I have found the person corresponding to the phone number above. (see below) \n" +
                            selectedPerson.ToString() + "\n" +
                            "Would you like to add the person above as a customer?";
                            c.EnableWriteTitle = true;
                        });
                    yesNOQuestion.Show();
                }
                else
                {
                    var yesNOQuestion = new ConsoleMenu(givenArgs, level: 3).
                        Add("Yes, let me try again!", (thisMenu) =>
                        {
                            this.SelectCustomer(givenArgs, givenLogic);
                            thisMenu.CloseMenu();
                        }).
                        Add("No, let me go back.", ConsoleMenu.Close).
                        Configure(c =>
                        {
                            c.Title = "Couldn't find the customer you are looking for! :c \n" +
                            "Would you like to try again?";
                            c.EnableWriteTitle = true;
                        });
                    yesNOQuestion.Show();
                }
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenPerson">Based on what person data should we create the customer.</param>
        public void AddCustomer(string[] givenArgs, OrderManagement givenLogic, Person givenPerson)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                Customer selectedCustomer;
                string phoneNumber;
                string fullName;
                string address;

                if (givenPerson == null)
                {
                    Console.WriteLine("** AddCustomer **");
                    Console.WriteLine("In the following steps, please provide the data of the customer.");
                    Console.WriteLine("----------");
                    Console.WriteLine("Phone number:     [Syntax : '701234567']");
                    phoneNumber = Console.ReadLine();
                    Console.WriteLine("Full name:     [Syntax: 'Deák Dániel']");
                    fullName = Console.ReadLine();
                    Console.WriteLine("Full address:     [Syntax: '2225 Üllő, Deák Ferenc utca 33/a 1/3']");
                    address = Console.ReadLine();
                    selectedCustomer = givenLogic.AddCustomer(phoneNumber, fullName, address);
                }
                else
                {
                    Console.WriteLine("** AddCustomer **");
                    Console.WriteLine("Adding the already found person as a customer...");
                    selectedCustomer = givenLogic.AddCustomer(givenPerson.PhoneNumber, givenPerson.FullName, givenPerson.Address);
                }

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(" Successfully added a new customer! ");
                Console.ResetColor();
                System.Threading.Thread.Sleep(1250);

                this.ContinueWithCustomer(givenArgs, givenLogic, selectedCustomer);
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenCustomer">For what customer should we continue the order process.</param>
        public void ContinueWithCustomer(string[] givenArgs, OrderManagement givenLogic, Customer givenCustomer)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                var yesNOQuestion = new ConsoleMenu(givenArgs, level: 3).
                                Add("Yes, lets order some pizzas.", () =>
                                {
                                    givenLogic.OrderingCustomer = givenCustomer;
                                    this.SelectPizzas(givenArgs, givenLogic);
                                }).
                                Add("No, let me go back and start it over.", ConsoleMenu.Close).
                                Configure(c =>
                                {
                                    c.Title = "You have the following customer selected : \n" +
                                    givenCustomer.Person.ToString() +
                                    "\n Would you like to continue?";
                                    c.EnableWriteTitle = true;
                                });
                yesNOQuestion.Show();
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        public void SelectPizzas(string[] givenArgs, OrderManagement givenLogic)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                IList<Pizza> availablePizzas = givenLogic.GetAllPizzas();

                var selectPizzaMenu = new ConsoleMenu(givenArgs, level: 4);

                selectPizzaMenu.Add("Shopping cart.", () => this.ManageShoppingCart(givenArgs, givenLogic, "get", null));

                foreach (Pizza item in availablePizzas)
                {
                    selectPizzaMenu.Add($"{item}", () => this.ManageShoppingCart(givenArgs, givenLogic, "set", item));
                }

                selectPizzaMenu.Add("Continue to checkout.", () => this.OrderCheckout(givenArgs, givenLogic)).Show();
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenFunc">Which inside function we want to use this methode for.</param>
        /// <param name="givenItem">Selected pizza item that should be added to the order.</param>
        public void ManageShoppingCart(string[] givenArgs, OrderManagement givenLogic, string givenFunc, Pizza givenItem)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                switch (givenFunc)
                {
                    case "get":
                        List<Pizza> currentOrders;
                        List<int> currentAmounts;
                        (currentOrders, currentAmounts) = givenLogic.GetShoppingCart();

                        var shoppingCart = new ConsoleMenu(givenArgs, level: 5);
                        for (int i = 0; i < currentOrders.Count; i++)
                        {
                            shoppingCart.Add($"{currentOrders[i].Name}, Qnt : {currentAmounts[i]}", (thisMenu) =>
                            {
                                // Using i is not possible since that would result in an outer variable trap.
                                givenLogic.RemoveShoppingCartItem(thisMenu.CurrentItem.Index);
                                this.ManageShoppingCart(givenArgs, givenLogic, "get", null);
                                thisMenu.CloseMenu();
                            });
                        }

                        shoppingCart.Add("Go back", shoppingCart.CloseMenu);
                        shoppingCart.Configure(c =>
                        {
                            c.Title = "Here you can see the content of the sopping cart.\n" +
                            "If you wish to remove an item, just simple select them.";
                            c.EnableWriteTitle = true;
                        });

                        shoppingCart.Show();
                        break;

                    case "set":
                        int amount = -1;
                        bool isCorrectValue = false;
                        do
                        {
                            Console.WriteLine("Please provide an amount below!");
                            try
                            {
                                amount = Convert.ToInt32(Console.ReadLine());
                                isCorrectValue = true;
                            }
                            catch (FormatException e)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine(e.Message);
                                Console.WriteLine("You must enter a number!");
                                Console.ResetColor();
                            }
                        }
                        while (!isCorrectValue);

                        givenLogic.SetShoppingCartItem(givenItem, amount);

                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine(" Item added successfully! ");
                        Console.ResetColor();
                        System.Threading.Thread.Sleep(1250);
                        break;
                }
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        public void OrderCheckout(string[] givenArgs, OrderManagement givenLogic)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else if (givenLogic.OrderedPizzas.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("You should not make empty orders.");
                Console.WriteLine("Hit enter to return.");
                Console.ResetColor();
                Console.ReadKey();

                this.SelectPizzas(givenArgs, givenLogic);
            }
            else
            {
                var checkoutMenu = new ConsoleMenu(givenArgs, level: 6).
                    Add(" Show order ", () =>
                    {
                        this.OrderPreview(givenLogic);
                        this.OrderCheckout(givenArgs, givenLogic);
                    }).
                    Add(" Yes, submit the order as it is.", () =>
                    {
                        givenLogic.PlaceOrder();
                        MainProgram.MainMenu(givenArgs);    // PUBLIC STATIC FUNCTION
                    }).
                    Add(" No, let me change the selected pizzas.", () => this.SelectPizzas(givenArgs, givenLogic)).
                    Configure(c =>
                    {
                        c.Title = "Here you can see a sumamry of your order.\n" +
                                  "Please decide whether it is correct or not.";
                        c.EnableWriteTitle = true;
                    });

                checkoutMenu.Show();
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        public void OrderPreview(OrderManagement givenLogic)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                Console.WriteLine("-----Selected customer :");
                Console.WriteLine(givenLogic.OrderingCustomer.ToString() + "\n");
                Console.WriteLine("-----Selected pizzas :");
                List<Pizza> selectedPizzas;
                List<int> selectedAmounts;
                (selectedPizzas, selectedAmounts) = givenLogic.GetShoppingCart();
                for (int i = 0; i < selectedPizzas.Count; i++)
                {
                    Console.WriteLine($"  {selectedAmounts[i]}x     " + selectedPizzas[i].ToString());
                }

                Console.WriteLine("Hit enter to return.");
                Console.ReadKey();
                return;
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        public void ViewOrders(string[] givenArgs, OrderManagement givenLogic)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                var overviewMenu = new ConsoleMenu(givenArgs, level: 2)
                    .Configure(c =>
                    {
                        c.Title = "Here you can see all placed orders.\n" +
                                  "By selecting an order you can cancel them (storno).";
                        c.EnableWriteTitle = true;
                    });

                Collection<Order> allOrders = givenLogic.GetAllOrders();

                if (allOrders.Count == 0)
                {
                    Console.WriteLine("No orders to show!");
                    Console.WriteLine("Place some orders before attending this option!");
                    Console.WriteLine("Hit enter to return.");
                    Console.ReadKey();
                    return;
                }
                else
                {
                    for (int i = 0; i < allOrders.Count; i++)
                    {
                        if (allOrders[i].IsStorno)
                        {
                            overviewMenu.Add($" [-STORNO-] {allOrders[i]}", (thisMenu) =>
                            {
                                // Empty since it is a cancelled order.
                            });
                        }
                        else
                        {
                            overviewMenu.Add($"{allOrders[i]}", (thisMenu) =>
                            {
                                // Using i is not possible since that would result in an outer variable trap.
                                givenLogic.RemoveOrder(allOrders[thisMenu.CurrentItem.Index].Id);
                                this.ViewOrders(givenArgs, givenLogic);
                                thisMenu.CloseMenu();
                            });
                        }
                    }

                    overviewMenu.Add("Back", ConsoleMenu.Close);
                    overviewMenu.Show();

                    return;
                }
            }
        }
    }
}
