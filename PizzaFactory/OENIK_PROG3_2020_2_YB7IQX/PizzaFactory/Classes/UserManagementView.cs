﻿// <copyright file="UserManagementView.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Program.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Text;
    using ConsoleTools;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Data.Models.Human;
    using PizzaFactory.Logic.Classes;
    using PizzaFactory.Program.Interfaces;

    /// <summary>
    /// Implementation of the IUserManagementView interface.
    /// </summary>
    public class UserManagementView : IUserManagementView
    {
        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        public void ManageUsers(string[] givenArgs, UserManagement givenLogic)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                var usersMenu = new ConsoleMenu(givenArgs, level: 2).
                Add(" Hire a new employee ", (thisMenu) =>
                {
                    this.AddEmployee(givenLogic);
                    thisMenu.CloseMenu();
                }).
                Configure(c =>
                {
                    c.Title = "Select an employee to modify, or add a new one.";
                    c.EnableWriteTitle = true;
                });

                Collection<Employee> allUsers = givenLogic.GetAllUsers();

                foreach (Employee item in allUsers)
                {
                    usersMenu.Add($"{item}", () =>
                    {
                        this.ModifyEmployee(givenArgs, givenLogic, item);
                        usersMenu.CloseMenu();
                    });
                }

                usersMenu.Add("Back ", ConsoleMenu.Close);
                usersMenu.Show();
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenEmployee">Based on what employee data should we modify the employee.</param>
        public void ModifyEmployee(string[] givenArgs, UserManagement givenLogic, Employee givenEmployee)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                var modifyUserMenu = new ConsoleMenu(givenArgs, level: 3).
               Configure(c =>
               {
                   c.Title = $"You are about to modify :\n {givenEmployee}";
                   c.EnableWriteTitle = true;
               });

                modifyUserMenu.
                    Add("Modify personal data", (thisMenu) =>
                    {
                        this.ModifyEmployeePerson(givenArgs, givenLogic, givenEmployee);
                        thisMenu.CloseMenu();
                    }).
                    Add("Modify role (also modifies access level accordingly)", (thisMenu) =>
                    {
                        this.ModifyEmployeeRole(givenArgs, givenLogic, givenEmployee);
                        thisMenu.CloseMenu();
                    }).
                    Add("Fire / retire employee", (thisMenu) =>
                    {
                        givenLogic.ModifyEmployeeRole(givenEmployee, "rof");
                        this.SuccessfullOperationMessage("Retired / Fired employee successfully.");
                        thisMenu.CloseMenu();
                    }).
                    Add("Back", modifyUserMenu.CloseMenu);

                modifyUserMenu.Show();
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenEmployee">Based on what employee data should we modify the employee.</param>
        public void ModifyEmployeeRole(string[] givenArgs, UserManagement givenLogic, Employee givenEmployee)
        {
            if (givenLogic == null || givenEmployee == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                var modifyUserRoleMenu = new ConsoleMenu(givenArgs, level: 4).
                Add("Make admin", (thisMenu) =>
                {
                    givenLogic.ModifyEmployeeRole(givenEmployee, "admin");
                    this.SuccessfullOperationMessage("Changed role successfully.");
                    thisMenu.CloseMenu();
                }).
                Add("Make shift manager", (thisMenu) =>
                {
                    givenLogic.ModifyEmployeeRole(givenEmployee, "smanager");
                    this.SuccessfullOperationMessage("Changed role successfully.");
                    thisMenu.CloseMenu();
                }).
                Add("Make dispatcher", (thisMenu) =>
                {
                    givenLogic.ModifyEmployeeRole(givenEmployee, "dispatcher");
                    this.SuccessfullOperationMessage("Changed role successfully.");
                    thisMenu.CloseMenu();
                }).
                Add("Make cook", (thisMenu) =>
                {
                    givenLogic.ModifyEmployeeRole(givenEmployee, "cook");
                    this.SuccessfullOperationMessage("Changed role successfully.");
                    thisMenu.CloseMenu();
                }).
                Add("Make assistant", (thisMenu) =>
                {
                    givenLogic.ModifyEmployeeRole(givenEmployee, "assistant");
                    this.SuccessfullOperationMessage("Changed role successfully.");
                    thisMenu.CloseMenu();
                }).
                Add("Back", ConsoleMenu.Close);

                modifyUserRoleMenu.Show();
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenEmployee">Based on what employee data should we modify the employee.</param>
        public void ModifyEmployeePerson(string[] givenArgs, UserManagement givenLogic, Employee givenEmployee)
        {
            if (givenLogic == null || givenEmployee == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                var modifyUserPersonMenu = new ConsoleMenu(givenArgs, level: 4).
                Add($"{givenEmployee.Person.FullName}", (thisMenu) =>
                {
                    this.ModifyEmployeePersonPart(givenLogic, givenEmployee, "name");
                    this.ManageUsers(givenArgs, givenLogic);
                    thisMenu.CloseMenu();
                }).
                Add($"{givenEmployee.Person.PhoneNumber}", (thisMenu) =>
                {
                    this.ModifyEmployeePersonPart(givenLogic, givenEmployee, "phone");
                    this.ManageUsers(givenArgs, givenLogic);
                    thisMenu.CloseMenu();
                }).
                Add($"{givenEmployee.Person.Address}", (thisMenu) =>
                {
                    this.ModifyEmployeePersonPart(givenLogic, givenEmployee, "address");
                    this.ManageUsers(givenArgs, givenLogic);
                    thisMenu.CloseMenu();
                }).
                Add("Back", ConsoleMenu.Close).
                Configure(c =>
                {
                    c.Title = "Select which part of the person's data would you like to modify.";
                    c.EnableWriteTitle = true;
                });
                modifyUserPersonMenu.Show();
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenEmployee">Based on what employee data should we modify the employee.</param>
        /// <param name="givenFunc">Predefined function determining what is going to be done.</param>
        public void ModifyEmployeePersonPart(UserManagement givenLogic, Employee givenEmployee, string givenFunc)
        {
            if (givenLogic == null || givenEmployee == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                switch (givenFunc)
                {
                    case "name":
                        Console.WriteLine("Please provide the new name of the Employee :");
                        string newFullName = Console.ReadLine();

                        givenLogic.ModifyEmployeePerson(givenEmployee, "name", newFullName);

                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine(" Employee's person data changed successfully.");
                        Console.ResetColor();
                        System.Threading.Thread.Sleep(1250);
                        break;
                    case "address":
                        Console.WriteLine("Please provide the new address of the Employee :");
                        string newAddress = Console.ReadLine();

                        givenLogic.ModifyEmployeePerson(givenEmployee, "address", newAddress);

                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine(" Employee's person data changed successfully.");
                        Console.ResetColor();
                        System.Threading.Thread.Sleep(1250);
                        break;
                    case "phone":
                        Console.WriteLine("Please provide the new phone number of the Employee :");
                        string newPhoneNumber = Console.ReadLine();

                        givenLogic.ModifyEmployeePerson(givenEmployee, "phone", newPhoneNumber);

                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine(" Employee's person data changed successfully.");
                        Console.ResetColor();
                        System.Threading.Thread.Sleep(1250);
                        break;
                }

                return;
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenMessage">Given sentence we would like to print out in green as sucess result.</param>
        public void SuccessfullOperationMessage(string givenMessage)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(givenMessage);
            Console.ResetColor();
            System.Threading.Thread.Sleep(1250);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenLogic">Based on which logic shoul
        /// d we execute the operations.</param>
        public void AddEmployee(UserManagement givenLogic)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                Console.WriteLine("** AddEmployee **");
                Console.WriteLine("In the following steps, please provide the data of the employee.");
                Console.WriteLine("----------");
                Console.WriteLine("Phone number:     [Syntax : '701234567']");
                string phoneNumber = Console.ReadLine();
                Console.WriteLine("Full name:     [Syntax: 'Deák Dániel']");
                string fullName = Console.ReadLine();
                Console.WriteLine("Full address:     [Syntax: '2225 Üllő, Deák Ferenc utca 33/a 1/3']");
                string address = Console.ReadLine();
                Console.WriteLine("Role:     [Syntax: 'admin' / 'smanager' / 'dispatcher' / 'cook' / 'assistant']");
                string role;
                bool isCorrect = false;

                do
                {
                    role = Console.ReadLine();
                    if (role == "admin" || role == "smanager" || role == "dispatcher" || role == "cook" || role == "assistant")
                    {
                        isCorrect = true;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"there is no '{role}' role. Please try again.");
                        Console.ResetColor();
                    }
                }
                while (!isCorrect);

                givenLogic.AddEmployee(phoneNumber, fullName, address, role);
            }
        }
    }
}
