﻿// <copyright file="StatisticsManagementView.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Program.Classes
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using ConsoleTools;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Logic.Classes;
    using PizzaFactory.Program.Interfaces;

    /// <summary>
    /// Implementation of the IStatisticsManagementView.
    /// </summary>
    public class StatisticsManagementView : IStatisticsManagementView
    {
        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenArgs">Arguments of the main program.</param>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        /// <param name="givenToggle">Value of the toggle button, defines whether we show the async or basic menu.</param>
        public void ViewStatistics(string[] givenArgs, StatisticsManagement givenLogic, bool givenToggle)
        {
            bool isAsyncToggled = givenToggle;

            if (!isAsyncToggled)
            {
                var statisticsMenu = new ConsoleMenu(givenArgs, level: 2).
                    Add(" How much do our valuable employees spend in our pizzeria ? ", () => this.RenewableMoney(givenLogic)).
                    Add(" What pizza are we looking for ?", () => this.PizzaContains(givenLogic)).
                    Add(" Topping lovers ", () => this.PizzaToppings(givenLogic)).
                    Add(" Toggle async : off ", (thisMenu) =>
                        {
                            this.ViewStatistics(givenArgs, givenLogic, true);
                            thisMenu.CloseMenu();
                        }).
                    Add(" Back ", ConsoleMenu.Close);
                statisticsMenu.Show();
            }
            else if (isAsyncToggled)
            {
                var statisticsMenu = new ConsoleMenu(givenArgs, level: 2).
                    Add(" How much do our valuable employees spend in our pizzeria ? ", () => this.RenewableMoneyAsync(givenLogic)).
                    Add(" What pizza are we looking for ?", () => this.PizzaContainsAsync(givenLogic)).
                    Add(" Topping lovers ", () => this.PizzaToppingsAsync(givenLogic)).
                    Add(" Toggle async : on ", (thisMenu) =>
                        {
                            this.ViewStatistics(givenArgs, givenLogic, false);
                            thisMenu.CloseMenu();
                        }).
                    Add(" Back ", ConsoleMenu.Close).
                    Configure(c =>
                  {
                      c.Title = "| ! ASYNC ! |";
                      c.EnableWriteTitle = true;
                      c.ItemForegroundColor = ConsoleColor.DarkRed;
                  });
                statisticsMenu.Show();
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        public void RenewableMoney(StatisticsManagement givenLogic)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                Console.WriteLine("----- Renewable money -----");
                IList<RenewableEmployees> renewEmp = givenLogic.RenewableMoney();

                foreach (RenewableEmployees item in renewEmp)
                {
                    Console.WriteLine(item + " --" + item.SpentQ);
                }

                Console.ReadKey();
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        public void RenewableMoneyAsync(StatisticsManagement givenLogic)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("----- |ASYNC| Renewable money |ASYNC| -----");
                Console.ResetColor();
                IList<RenewableEmployees> renewEmp = givenLogic.RenewableMoney();

                foreach (RenewableEmployees item in renewEmp)
                {
                    Console.WriteLine(item + " --" + item.SpentQ);
                }

                Console.ReadKey();
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        public void PizzaContains(StatisticsManagement givenLogic)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                Console.WriteLine("----- Pizza Contains -----");
                Console.WriteLine("Here you can search for pizzas that have ingredients which are in a specific range of price.");
                Console.WriteLine("You can also define a substring to search for that has to be in at least one of the ingredients that the pizza contains.");
                Console.WriteLine("Please specify a substring to search for! (like : 'orn', 'h' or 'aviar'");
                string stringValue = Console.ReadLine();

                int lowerValue;
                int upperValue;
                bool isCorrect = false;
                do
                {
                    Console.WriteLine("Please define the lower bound of the search :");
                    lowerValue = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("and the upper bound of the search :");
                    upperValue = Convert.ToInt32(Console.ReadLine());

                    if (lowerValue > upperValue)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("The lower value cannot be bigger than the upper value!");
                        Console.ResetColor();
                    }
                    else
                    {
                        isCorrect = true;
                    }
                }
                while (!isCorrect);

                IList<Pizza> result;

                result = givenLogic.PizzaSearch(stringValue, lowerValue, upperValue);

                if (result.Count == 0 || result == null)
                {
                    Console.WriteLine("Couldn't find anything with the given parameters. :(");
                    Console.WriteLine(stringValue + " -- " + lowerValue + " -- " + upperValue);
                }
                else
                {
                    foreach (var item in result)
                    {
                        Console.WriteLine(item);
                    }
                }

                Console.WriteLine("Hit enter to return.");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        public void PizzaContainsAsync(StatisticsManagement givenLogic)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("----- |ASYNC| Pizza Contains |ASYNC| -----");
                Console.ResetColor();
                Console.WriteLine("Here you can search for pizzas that have ingredients which are in a specific range of price.");
                Console.WriteLine("You can also define a substring to search for that has to be in at least one of the ingredients that the pizza contains.");
                Console.WriteLine("Please specify a substring to search for! (like : 'orn', 'h' or 'aviar'");
                string stringValue = Console.ReadLine();

                int lowerValue;
                int upperValue;
                bool isCorrect = false;
                do
                {
                    Console.WriteLine("Please define the lower bound of the search :");
                    lowerValue = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("and the upper bound of the search :");
                    upperValue = Convert.ToInt32(Console.ReadLine());

                    if (lowerValue > upperValue)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("The lower value cannot be bigger than the upper value!");
                        Console.ResetColor();
                    }
                    else
                    {
                        isCorrect = true;
                    }
                }
                while (!isCorrect);

                IList<Pizza> result;

                result = givenLogic.PizzaSearch(stringValue, lowerValue, upperValue);

                if (result.Count == 0 || result == null)
                {
                    Console.WriteLine("Couldn't find anything with the given parameters. :(");
                    Console.WriteLine(stringValue + " -- " + lowerValue + " -- " + upperValue);
                }
                else
                {
                    foreach (var item in result)
                    {
                        Console.WriteLine(item);
                    }
                }

                Console.WriteLine("Hit enter to return.");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        public void PizzaToppings(StatisticsManagement givenLogic)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                Console.WriteLine("----- Pizza Toppigs -----");
                Console.WriteLine("What topping are you searching for? ");
                Console.WriteLine("You may chose from the followings : ");
                foreach (var item in givenLogic.GetAllIngredients())
                {
                    Console.Write(item + " ");
                }

                Console.WriteLine();

                string searchFor;
                bool isCorrect = false;

                do
                {
                    searchFor = Console.ReadLine();

                    foreach (Ingredient item in givenLogic.GetAllIngredients())
                    {
                        if (searchFor == item.ToString())
                        {
                            isCorrect = true;
                        }
                    }

                    if (!isCorrect)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"there is no '{searchFor}' topping. Please try again.");
                        Console.ResetColor();
                    }
                }
                while (!isCorrect);

                IList<Pizza> matchingPizzas;
                IList<string> orderedCustomers;

                (matchingPizzas, orderedCustomers) = givenLogic.ToppingLovers(searchFor);

                Console.WriteLine($"The following pizzas cointain '{searchFor}'.");
                foreach (var pizza in matchingPizzas)
                {
                    Console.WriteLine(pizza);
                }

                Console.WriteLine("The following customers ordered the most of these pizzas in descending order:");
                foreach (var customer in orderedCustomers)
                {
                    Console.WriteLine($"{customer}");
                }

                Console.ReadKey();
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenLogic">Based on which logic should we execute the operations.</param>
        public void PizzaToppingsAsync(StatisticsManagement givenLogic)
        {
            if (givenLogic == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went realy realy wrong.");
                Console.WriteLine("Im going to shut everything down now.");
                Console.WriteLine("Hit enter to shutdown.");
                Console.ReadKey();
                Environment.Exit(-1);
                Console.ResetColor();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("----- |ASYNC| Pizza Toppigs |ASYNC| -----");
                Console.ResetColor();
                Console.WriteLine("What topping are you searching for? ");
                Console.WriteLine("You may chose from the followings : ");
                foreach (var item in givenLogic.GetAllIngredients())
                {
                    Console.Write(item + " ");
                }

                Console.WriteLine();

                string searchFor;
                bool isCorrect = false;

                do
                {
                    searchFor = Console.ReadLine();

                    foreach (Ingredient item in givenLogic.GetAllIngredients())
                    {
                        if (searchFor == item.ToString())
                        {
                            isCorrect = true;
                        }
                    }

                    if (!isCorrect)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"there is no '{searchFor}' topping. Please try again.");
                        Console.ResetColor();
                    }
                }
                while (!isCorrect);

                IList<Pizza> matchingPizzas;
                IList<string> orderedCustomers;

                (matchingPizzas, orderedCustomers) = givenLogic.ToppingLovers(searchFor);

                Console.WriteLine($"The following pizzas cointain '{searchFor}'.");
                foreach (var pizza in matchingPizzas)
                {
                    Console.WriteLine(pizza);
                }

                Console.WriteLine("The following customers ordered the most of these pizzas in descending order:");
                foreach (var customer in orderedCustomers)
                {
                    Console.WriteLine($"{customer}");
                }

                Console.ReadKey();
            }
        }
    }
}
