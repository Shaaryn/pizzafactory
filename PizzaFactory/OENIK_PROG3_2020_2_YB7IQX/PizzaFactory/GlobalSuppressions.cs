﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "Based on the amount and variety of the text I dont see this as an optimal solution. Maybe if I were started this process at the very first line.")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "I dont have the energy and time to research this topic.")]