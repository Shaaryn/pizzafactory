﻿// <copyright file="MainTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using Moq;
    using NUnit.Framework;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Data.Models.Connector;
    using PizzaFactory.Data.Models.Human;
    using PizzaFactory.Logic.Classes;
    using PizzaFactory.Repository;
    using PizzaFactory.Repository.Classes;
    using PizzaFactory.Repository.Interfaces;

    /// <summary>
    /// Organizes the required test of the program.
    /// </summary>
    [TestFixture]
    public class MainTest
    {
        private Collection<Pizzeria> _pizzerias;
        private Collection<Employee> _employees;
        private Collection<Customer> _customers;
        private Collection<Person> _people;
        private Collection<Ingredient> _ingredients;
        private Collection<Pizza> _pizzas;
        private Collection<Order> _orders;
        private Collection<PizzaIngredientConnector> _cpis;
        private Collection<OrderPizzaConnector> _cops;

        /// <summary>
        /// Gets or sets collection of testable pizzerias.
        /// </summary>
        public Collection<Pizzeria> Pizzerias
        {
            get { return this._pizzerias; }
            set { this._pizzerias = value; }
        }

        /// <summary>
        /// Gets or sets collection of testable employee.
        /// </summary>
        public Collection<Employee> Employees
        {
            get { return this._employees; }
            set { this._employees = value; }
        }

        /// <summary>
        /// Gets or sets collection of testable customers.
        /// </summary>
        public Collection<Customer> Customers
        {
            get { return this._customers; }
            set { this._customers = value; }
        }

        /// <summary>
        /// Gets or sets collection of testable people.
        /// </summary>
        public Collection<Person> People
        {
            get { return this._people; }
            set { this._people = value; }
        }

        /// <summary>
        /// Gets or sets collection of testable ingredients.
        /// </summary>
        public Collection<Ingredient> Ingredients
        {
            get { return this._ingredients; }
            set { this._ingredients = value; }
        }

        /// <summary>
        /// Gets or sets collection of testable pizzas.
        /// </summary>
        public Collection<Pizza> Pizzas
        {
            get { return this._pizzas; }
            set { this._pizzas = value; }
        }

        /// <summary>
        /// Gets or sets collection of testable Orders.
        /// </summary>
        public Collection<Order> Orders
        {
            get { return this._orders; }
            set { this._orders = value; }
        }

        /// <summary>
        /// Gets or sets collection of testable Cpis.
        /// </summary>
        public Collection<PizzaIngredientConnector> Cpis
        {
            get { return this._cpis; }
            set { this._cpis = value; }
        }

        /// <summary>
        /// Gets or sets collection of testable Cops.
        /// </summary>
        public Collection<OrderPizzaConnector> Cops
        {
            get { return this._cops; }
            set { this._cops = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainTest"/> class.
        /// </summary>
        public MainTest()
        {
            this.PopulateTestSource();
        }

        /// <summary>
        /// Populates the source sets with seed data.
        /// </summary>
        public void PopulateTestSource()
        {
            // Pizzeria
            Pizzeria theone = new Pizzeria
            {
                Id = 1,
                Name = "The One Pizza Factory",
                PersonId = 1,
                Address = "1055 Budapest, Kossuth Lajos tér 1-3.",
                EstablishedDate = DateTime.Now,
            };

            // Employee
            Employee employee1 = new Employee
            {
                Id = 1,
                PersonId = 3,
                RoleOfEmployee = Role.Admin,
                RoleOfEmployeeId = (int)Role.Admin,
                AccessLevel = Authorization.Admin,
                AccessLevelValue = (int)Authorization.Admin,
                StartOfEmployment = DateTime.Now,
            };

            // Customer
            Customer customer1 = new Customer
            {
                Id = 1,
                PersonId = 3,
                RegisteredOn = DateTime.Now,
            };

            // Person
            Person owner = new Person { Id = 1, FullName = "Péter Árpád", Address = "1222 Budapest, Karéj utca 25.", PhoneNumber = "209862283" };
            Person administrator = new Person { Id = 2, FullName = "Nagy Ádám", Address = "2225 Üllő, Deák Ferenc utca 33/a", PhoneNumber = "123456789", EmployeeId = 1, Employee = employee1, CustomerId = 1, Customer = customer1 };
            Person cook = new Person { Id = 3, FullName = "Bogdán Árpád", Address = "1234 Monor, Vas utca 35.", PhoneNumber = "308746998" };

            // Cpi
            PizzaIngredientConnector cpi1 = new PizzaIngredientConnector { Id = 1, PizzaId = 1, IngredientId = 1 };
            PizzaIngredientConnector cpi2 = new PizzaIngredientConnector { Id = 2, PizzaId = 1, IngredientId = 3 };

            PizzaIngredientConnector cpi3 = new PizzaIngredientConnector { Id = 3, PizzaId = 2, IngredientId = 2 };
            PizzaIngredientConnector cpi4 = new PizzaIngredientConnector { Id = 4, PizzaId = 2, IngredientId = 3 };

            PizzaIngredientConnector cpi5 = new PizzaIngredientConnector { Id = 5, PizzaId = 3, IngredientId = 1 };
            PizzaIngredientConnector cpi6 = new PizzaIngredientConnector { Id = 6, PizzaId = 3, IngredientId = 2 };
            PizzaIngredientConnector cpi7 = new PizzaIngredientConnector { Id = 7, PizzaId = 3, IngredientId = 3 };

            // Ingredient
            Ingredient ing1 = new Ingredient { Id = 1, Name = "cheese", Cost = 250 };
            Ingredient ing2 = new Ingredient { Id = 2, Name = "corn", Cost = 200 };
            Ingredient ing3 = new Ingredient { Id = 3, Name = "ham", Cost = 350 };

            // Pizza
            Pizza pizza1 = new Pizza { Id = 1, Name = "1. Finom pizza", Size = 30, Price = 1450, SaleMultiplier = 0.9f, FurtherExpenses = 1.0f };
            Pizza pizza2 = new Pizza { Id = 2, Name = "2. Nagyszerű pizza", Size = 45, Price = 2250, SaleMultiplier = 1.0f, FurtherExpenses = 1.1f };
            Pizza pizza3 = new Pizza { Id = 3, Name = "3. Korrekt pizza", Size = 30, Price = 1780, SaleMultiplier = 1.0f, FurtherExpenses = 1.2f };

            // Cop
            OrderPizzaConnector cop1 = new OrderPizzaConnector { Id = 1, OrderId = 1, PizzaId = 3, Amount = 2, TotalPrice = 3560 };
            OrderPizzaConnector cop2 = new OrderPizzaConnector { Id = 2, OrderId = 1, PizzaId = 7, Amount = 1, TotalPrice = 1800 };

            OrderPizzaConnector cop3 = new OrderPizzaConnector { Id = 3, OrderId = 2, PizzaId = 2, Amount = 2, TotalPrice = 4500 };
            OrderPizzaConnector cop4 = new OrderPizzaConnector { Id = 4, OrderId = 2, PizzaId = 1, Amount = 1, TotalPrice = 1450 };
            OrderPizzaConnector cop5 = new OrderPizzaConnector { Id = 5, OrderId = 2, PizzaId = 5, Amount = 1, TotalPrice = 1950 };

            OrderPizzaConnector cop6 = new OrderPizzaConnector { Id = 6, OrderId = 3, PizzaId = 6, Amount = 1, TotalPrice = 1780 };

            // Order
            Order order1 = new Order { Id = 1, PizzeriaId = 1, EmployeeId = 1, CustomerId = customer1.Id, TimeStamp = DateTime.Now.AddDays(-97), IsStorno = false };
            Order order2 = new Order { Id = 2, PizzeriaId = 1, EmployeeId = 2, CustomerId = customer1.Id, TimeStamp = DateTime.Now.AddDays(-31), IsStorno = false };
            Order order3 = new Order { Id = 3, PizzeriaId = 1, EmployeeId = 3, CustomerId = customer1.Id, TimeStamp = DateTime.Now.AddDays(-11), IsStorno = false };

            customer1.Orders = new Collection<Order>() { order1, order2, order3 };
            customer1.Person = administrator;

            this.Pizzerias = new Collection<Pizzeria>() { theone };
            this.Employees = new Collection<Employee>() { employee1 };
            this.Customers = new Collection<Customer>() { customer1 };
            this.People = new Collection<Person>() { owner, administrator, cook };
            this.Ingredients = new Collection<Ingredient>() { ing1, ing2, ing3 };
            this.Pizzas = new Collection<Pizza>() { pizza1, pizza2, pizza3 };
            this.Orders = new Collection<Order>() { order1, order2, order3 };
            this.Cpis = new Collection<PizzaIngredientConnector>() { cpi1, cpi2, cpi3, cpi4, cpi5, cpi6, cpi7 };
            this.Cops = new Collection<OrderPizzaConnector>() { cop1, cop2, cop3, cop4, cop5, cop6 };
        }
    }
}
