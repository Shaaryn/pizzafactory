﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.NamingRules", "SA1309:Field names should not begin with underscore", Justification = "Its a habit I inherited from the internet, helps me keep track of the variables that should not be changed or if changed should be handled with care.")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "I intend to use its set proeprty as well.")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "I cant place it before or after the properties, and methods, so where could I...", Scope = "member", Target = "~M:PizzaFactory.Logic.Tests.MainTest.#ctor")]
