﻿// <copyright file="OrderManagementTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Logic.Tests.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Moq;
    using NUnit.Framework;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Data.Models.Human;
    using PizzaFactory.Logic.Classes;
    using PizzaFactory.Repository;
    using PizzaFactory.Repository.Classes;
    using PizzaFactory.Repository.Interfaces;

    /// <summary>
    /// Tests for the OrderManagement.cs class.
    /// </summary>
    [TestFixture]
    public static class OrderManagementTests
    {
        /// <summary>
        /// Tests the PlaceOrder() method in the OrderManagement.
        /// Tested CRUDs:
        ///     -OrderRepo.Insert().
        ///     -PizzaRepo.GetOne().
        ///     -CusRepo.GiveRegisteredOn().
        /// </summary>
        [Test]
        public static void OrderManagementPlaceOrderFunctionTest()
        {
            MainTest main = new MainTest();
            IQueryable<Order> orders = main.Orders.AsQueryable();
            Mock<IOrderRepository> mockedOrdRepo = new Mock<IOrderRepository>(MockBehavior.Loose);
            IQueryable<Pizzeria> pizzerias = main.Pizzerias.AsQueryable();
            Mock<IPizzeriaRepository> mockedPizzeriaRepo = new Mock<IPizzeriaRepository>(MockBehavior.Loose);
            IQueryable<Customer> customers = main.Customers.AsQueryable();
            Mock<ICustomerRepository> mockedCusRepo = new Mock<ICustomerRepository>(MockBehavior.Loose);
            Employee orderingEmployee = main.Employees.FirstOrDefault();

            mockedOrdRepo.Setup(ordRepo => ordRepo.Insert(It.IsAny<Order>()));
            mockedPizzeriaRepo.Setup(pizzeriaRepo => pizzeriaRepo.GetOne(It.IsAny<int>())).Returns(pizzerias.FirstOrDefault());
            mockedCusRepo.Setup(cusRepo => cusRepo.GiveRegisteredOn(It.IsAny<int>()));

            OrderManagement logic = new OrderManagement(mockedPizzeriaRepo.Object, null, mockedCusRepo.Object, null, mockedOrdRepo.Object, null, orderingEmployee);

            // Act
            logic.PlaceOrder();

            mockedOrdRepo.Verify(ordRepo => ordRepo.Insert(It.IsAny<Order>()), Times.Once);
            mockedPizzeriaRepo.Verify(pizzeriaRepo => pizzeriaRepo.GetOne(It.IsAny<int>()), Times.Exactly(2));
            mockedCusRepo.Verify(cusRepo => cusRepo.GiveRegisteredOn(It.IsAny<int>()), Times.AtMostOnce);
        }

        /// <summary>
        /// Tests the RemoveOrder() method in the OrderManagement.
        /// Tested CRUDs:
        ///     -OrderRepo.Storno().
        /// </summary>
        [Test]
        public static void OrderManagementRemoveOrderFunctionTest()
        {
            MainTest main = new MainTest();
            IQueryable<Order> orders = main.Orders.AsQueryable();
            Mock<IOrderRepository> mockedOrdRepo = new Mock<IOrderRepository>(MockBehavior.Loose);

            mockedOrdRepo.Setup(ordRepo => ordRepo.Storno(It.IsAny<int>()));

            OrderManagement logic = new OrderManagement(null, null, null, null, mockedOrdRepo.Object, null, null);

            logic.RemoveOrder(1);

            mockedOrdRepo.Verify(ordRepo => ordRepo.Storno(It.IsAny<int>()), Times.AtMostOnce);
        }

        /// <summary>
        /// Tests the AddCustomer() method in the OrderManagement.
        /// Tested CRUDs:
        ///     -OrderRepo.Storno().
        /// </summary>
        [Test]
        public static void OrderManagementAddCustomer()
        {
            MainTest main = new MainTest();
            IQueryable<Person> people = main.People.AsQueryable();
            Mock<IPersonRepository> mockedPerRepo = new Mock<IPersonRepository>(MockBehavior.Loose);
            IQueryable<Customer> customers = main.Customers.AsQueryable();
            Mock<ICustomerRepository> mockedCusRepo = new Mock<ICustomerRepository>(MockBehavior.Loose);

            // Test person data ((non-existent))
            string testPhone1 = "852963741";
            string testName1 = "TestName1";
            string testAddress1 = "TestAddress1";
            Person testPerson1 = new Person() { Id = 2, FullName = testName1, Address = testAddress1, PhoneNumber = testPhone1 };

            // Test person data ((existent))
            string testPhone2 = "308746998";
            string testName2 = "Bogdán Árpád";
            string testAdress2 = "1234 Monor, Vas utca 35.";
            Person testPerson2 = new Person() { Id = 4, FullName = testName2, Address = testAdress2, PhoneNumber = testPhone2 };

            mockedPerRepo.Setup(perRepo => perRepo.GetAll()).Returns(people);
            mockedPerRepo.Setup(perRepo => perRepo.GetOne(It.IsAny<int>())).Returns(testPerson2);
            mockedPerRepo.Setup(perRepo => perRepo.GetOneByPhone(It.IsAny<string>())).Returns(testPerson2);
            mockedPerRepo.Setup(perRepo => perRepo.Insert(It.IsAny<Person>()));
            mockedCusRepo.Setup(cusRepo => cusRepo.Insert(It.IsAny<Customer>()));

            OrderManagement logic = new OrderManagement(null, mockedPerRepo.Object, mockedCusRepo.Object, null, null, null, null);

            logic.AddCustomer(testPhone2, testName2, testAdress2);

            mockedPerRepo.Verify(perRepo => perRepo.GetAll(), Times.AtMostOnce);
            mockedPerRepo.Verify(perRepo => perRepo.GetOne(It.IsAny<int>()), Times.AtLeastOnce);
            mockedPerRepo.Verify(perRepo => perRepo.GetOneByPhone(It.IsAny<string>()), Times.AtMostOnce);
            mockedPerRepo.Verify(perRepo => perRepo.Insert(It.IsAny<Person>()), Times.AtMostOnce);
            mockedCusRepo.Verify(cusRepo => cusRepo.Insert(It.IsAny<Customer>()), Times.AtMostOnce);
        }
    }
}
