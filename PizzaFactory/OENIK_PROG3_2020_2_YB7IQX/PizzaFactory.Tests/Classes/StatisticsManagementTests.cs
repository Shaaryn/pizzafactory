﻿// <copyright file="StatisticsManagementTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Logic.Tests.Classes
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Moq;
    using NUnit.Framework;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Data.Models.Connector;
    using PizzaFactory.Data.Models.Human;
    using PizzaFactory.Logic.Classes;
    using PizzaFactory.Repository;
    using PizzaFactory.Repository.Classes;
    using PizzaFactory.Repository.Interfaces;

    /// <summary>
    /// Tests for the PizzaManagement.cs class.
    /// </summary>
    [TestFixture]
    public static class StatisticsManagementTests
    {
        /// <summary>
        /// Tests the RenewableMoney() method if the StatisticsManagement.
        /// NON-CRUD Test.
        /// </summary>
        [Test]
        public static void StatisticsManagementLogicRenewableMoneyFunctionTest()
        {
            MainTest main = new MainTest();
            IQueryable<Person> people = main.People.AsQueryable();
            Mock<IPersonRepository> mockedPerRepo = new Mock<IPersonRepository>(MockBehavior.Loose);
            IQueryable<Employee> employees = main.Employees.AsQueryable();
            Mock<IEmployeeRepository> mockedEmpRepo = new Mock<IEmployeeRepository>(MockBehavior.Loose);
            IQueryable<Customer> customers = main.Customers.AsQueryable();
            Mock<ICustomerRepository> mockedCusRepo = new Mock<ICustomerRepository>(MockBehavior.Loose);

            mockedPerRepo.Setup(perRepo => perRepo.GetAll()).Returns(people);
            mockedEmpRepo.Setup(empRepo => empRepo.GetAll()).Returns(employees);
            mockedCusRepo.Setup(cusRepo => cusRepo.GetAll()).Returns(customers);

            StatisticsManagement logic = new StatisticsManagement(null, mockedPerRepo.Object, mockedCusRepo.Object, null, mockedEmpRepo.Object, null, null, null, null);

            var result = logic.RenewableMoney();

            Assert.That(result.Count == 1);
            Assert.That(result.Select(x => x.PersonQ.Id), Does.Contain(2));

            mockedPerRepo.Verify(perRepo => perRepo.GetAll(), Times.AtMostOnce);
            mockedEmpRepo.Verify(empRepo => empRepo.GetAll(), Times.AtMostOnce);
            mockedCusRepo.Verify(cusRepo => cusRepo.GetAll(), Times.AtMostOnce);
        }

        /// <summary>
        /// Tests the PizzaSearch() method if the StatisticsManagement.
        /// NON-CRUD Test.
        /// </summary>
        [Test]
        public static void StatisticsManagementLogicPizzaSearchFunctionTest()
        {
            MainTest main = new MainTest();
            IQueryable<Pizza> pizzas = main.Pizzas.AsQueryable();
            Mock<IPizzaRepository> mockedPizRepo = new Mock<IPizzaRepository>(MockBehavior.Loose);
            IQueryable<Ingredient> ingredients = main.Ingredients.AsQueryable();
            Mock<IIngredientRepository> mockedIngRepo = new Mock<IIngredientRepository>(MockBehavior.Loose);
            IQueryable<PizzaIngredientConnector> cpis = main.Cpis.AsQueryable();
            Mock<IConnectorPizzaIngredientRepository> mockedCpiRepo = new Mock<IConnectorPizzaIngredientRepository>(MockBehavior.Loose);

            mockedPizRepo.Setup(pizRepo => pizRepo.GetAll()).Returns(pizzas);
            mockedIngRepo.Setup(ingRepo => ingRepo.GetAll()).Returns(ingredients);
            mockedCpiRepo.Setup(cpiRepo => cpiRepo.GetAll()).Returns(cpis);

            StatisticsManagement logic = new StatisticsManagement(null, null, null, mockedPizRepo.Object, null, null, mockedIngRepo.Object, null, mockedCpiRepo.Object);

            var result = logic.PizzaSearch("ch", 201, 251);     // Cheese (Cost: 250)

            Assert.That(result.Count == 2);
            Assert.That(result[0] == pizzas.ToList()[0]);
            Assert.That(result[1] == pizzas.ToList()[2]);

            mockedPizRepo.Verify(pizRepo => pizRepo.GetAll(), Times.AtMostOnce);
            mockedIngRepo.Verify(ingRepo => ingRepo.GetAll(), Times.AtMostOnce);
            mockedCpiRepo.Verify(cpiRepo => cpiRepo.GetAll(), Times.AtMostOnce);

            mockedPizRepo.Verify(pizRepo => pizRepo.GetOne(It.IsAny<int>()), Times.Never);
        }

        /// <summary>
        /// Tests the ToppingLovers() method if the StatisticsManagement.
        /// NON-CRUD Test.
        /// </summary>
        [Test]
        public static void StatisticsManagementLogicToppingLoversFunctionTest()
        {
            MainTest main = new MainTest();
            IQueryable<Pizza> pizzas = main.Pizzas.AsQueryable();
            Mock<IPizzaRepository> mockedPizRepo = new Mock<IPizzaRepository>(MockBehavior.Loose);
            IQueryable<Ingredient> ingredients = main.Ingredients.AsQueryable();
            Mock<IIngredientRepository> mockedIngRepo = new Mock<IIngredientRepository>(MockBehavior.Loose);
            IQueryable<PizzaIngredientConnector> cpis = main.Cpis.AsQueryable();
            Mock<IConnectorPizzaIngredientRepository> mockedCpiRepo = new Mock<IConnectorPizzaIngredientRepository>(MockBehavior.Loose);

            IQueryable<Customer> customers = main.Customers.AsQueryable();
            Mock<ICustomerRepository> mockedCusRepo = new Mock<ICustomerRepository>(MockBehavior.Loose);
            IQueryable<Order> orders = main.Orders.AsQueryable();
            Mock<IOrderRepository> mockedOrdRepo = new Mock<IOrderRepository>(MockBehavior.Loose);
            IQueryable<OrderPizzaConnector> cops = main.Cops.AsQueryable();
            Mock<IConnectorOrderPizzaRepository> mockedCopRepo = new Mock<IConnectorOrderPizzaRepository>(MockBehavior.Loose);

            mockedPizRepo.Setup(pizRepo => pizRepo.GetAll()).Returns(pizzas);
            mockedIngRepo.Setup(ingRepo => ingRepo.GetAll()).Returns(ingredients);
            mockedCpiRepo.Setup(cpiRepo => cpiRepo.GetAll()).Returns(cpis);
            mockedCusRepo.Setup(cusRepo => cusRepo.GetAll()).Returns(customers);
            mockedOrdRepo.Setup(ordRepo => ordRepo.GetAll()).Returns(orders);
            mockedCopRepo.Setup(copRepo => copRepo.GetAll()).Returns(cops);

            StatisticsManagement logic = new StatisticsManagement(null, null, mockedCusRepo.Object, mockedPizRepo.Object, null, mockedOrdRepo.Object, mockedIngRepo.Object, mockedCopRepo.Object, mockedCpiRepo.Object);
            IList<Pizza> result1;
            IList<string> result2;

            (result1, result2) = logic.ToppingLovers("cheese");

            Assert.That(result1.Count == 2);
            Assert.That(result1, Does.Contain(pizzas.ToList()[0]));
            Assert.That(result2, Does.Contain(customers.ToList()[0].Person.FullName));

            mockedPizRepo.Verify(pizRepo => pizRepo.GetAll(), Times.AtMostOnce);
            mockedIngRepo.Verify(ingRepo => ingRepo.GetAll(), Times.AtMostOnce);
            mockedCpiRepo.Verify(cpiRepo => cpiRepo.GetAll(), Times.AtMostOnce);
            mockedCusRepo.Verify(cusRepo => cusRepo.GetAll(), Times.AtMostOnce);
            mockedOrdRepo.Verify(ordRepo => ordRepo.GetAll(), Times.AtMostOnce);
            mockedCopRepo.Verify(copRepo => copRepo.GetAll(), Times.AtMostOnce);

            mockedPizRepo.Verify(pizRepo => pizRepo.GetOne(It.IsAny<int>()), Times.Never);
            mockedCusRepo.Verify(cusRepo => cusRepo.Remove(It.IsAny<Customer>()), Times.Never);
        }
    }
}
