﻿// <copyright file="UserManagementTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Logic.Tests.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Moq;
    using NUnit.Framework;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Data.Models.Human;
    using PizzaFactory.Logic.Classes;
    using PizzaFactory.Repository;
    using PizzaFactory.Repository.Interfaces;

    /// <summary>
    /// Tests for the UserManagement.cs class.
    /// </summary>
    [TestFixture]
    public sealed class UserManagementTests
    {
        /// <summary>
        /// Tests the AddEmployee() method of the UserManagement.
        /// Tested CRUDS:
        ///     -PersonRepo.GetAll().
        ///     -PersonRepo.Insert().
        ///     -PersonRepo.GetOneByPhone().
        ///     -PersonRepo.GetOne().
        ///     -EmployeeRepo.Insert().
        ///     -EmployeeRepo.ChangeRole().
        /// </summary>
        [Test]
        public static void UserManagementLogicAddEmployeeFunctionTest()
        {
            MainTest main = new MainTest();
            IQueryable<Person> people = main.People.AsQueryable();
            Mock<IPersonRepository> mockedPerRepo = new Mock<IPersonRepository>(MockBehavior.Loose);
            IQueryable<Employee> employees = main.Employees.AsQueryable();
            Mock<IEmployeeRepository> mockedEmpRepo = new Mock<IEmployeeRepository>(MockBehavior.Loose);

            // Test person data
            string testPhone2 = "852963741";
            string testName2 = "TestName2";
            string testAddress2 = "TestAddress2";
            string testRole2 = "assistant";
            Person testPerson2 = new Person() { Id = 2, FullName = testName2, Address = testAddress2, PhoneNumber = testPhone2 };

            mockedPerRepo.Setup(perRepo => perRepo.GetAll()).Returns(people);
            mockedPerRepo.Setup(perRepo => perRepo.GetOneByPhone(It.IsAny<string>())).Returns(testPerson2);
            mockedPerRepo.Setup(perRepo => perRepo.GetOne(It.IsAny<int>())).Returns(testPerson2);
            mockedEmpRepo.Setup(empRepo => empRepo.GetAll()).Returns(employees);

            UserManagement userLogic = new UserManagement(mockedEmpRepo.Object, mockedPerRepo.Object);

            // Act
            userLogic.AddEmployee(testPhone2, testName2, testAddress2, testRole2);
            var resultSet = userLogic.GetAllUsers();

            mockedPerRepo.Verify(perRepo => perRepo.GetAll(), Times.Once);
            mockedPerRepo.Verify(perRepo => perRepo.GetOneByPhone(It.IsAny<string>()), Times.Once);
            mockedPerRepo.Verify(perRepo => perRepo.GetOne(It.IsAny<int>()), Times.Once);
            mockedPerRepo.Verify(perRepo => perRepo.Insert(It.IsAny<Person>()), Times.Once);
            mockedEmpRepo.Verify(empRepo => empRepo.Insert(It.IsAny<Employee>()), Times.Once);
            mockedEmpRepo.Verify(empRepo => empRepo.ChangeRole(It.IsAny<int>(), It.IsAny<string>()), Times.Once);

            mockedPerRepo.Verify(perRepo => perRepo.ChangeAddress(It.IsAny<int>(), It.IsAny<string>()), Times.Never);
            mockedEmpRepo.Verify(empRepo => empRepo.Resignation(It.IsAny<int>(), It.IsAny<DateTime>()), Times.Never);
        }
    }
}
