﻿// <copyright file="PizzaManagementTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Logic.Tests.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Moq;
    using NUnit.Framework;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Logic.Classes;
    using PizzaFactory.Repository.Classes;
    using PizzaFactory.Repository.Interfaces;

    /// <summary>
    /// Tests for the PizzaManagement.cs class.
    /// </summary>
    [TestFixture]
    public static class PizzaManagementTests
    {
        /// <summary>
        /// Tests the ModifyPizza() method if the PizzaManagement.
        /// Tested CRUDs:
        ///     -PizzaRepo.ChangeExpenses().
        ///     -PizzaRepo.ChangeSale().
        /// </summary>
        [Test]
        public static void PizzaManagementLogicModifyPizzaFunctionTest()
        {
            MainTest main = new MainTest();
            IQueryable<Pizza> pizzas = main.Pizzas.AsQueryable();
            Mock<IPizzaRepository> mockedPizRepo = new Mock<IPizzaRepository>(MockBehavior.Loose);

            mockedPizRepo.Setup(pizRepo => pizRepo.ChangePizzaName(It.IsAny<int>(), It.IsAny<string>()));
            mockedPizRepo.Setup(pizRepo => pizRepo.ChangeSale(It.IsAny<int>(), It.IsAny<float>()));

            PizzaManagement logic = new PizzaManagement(mockedPizRepo.Object, null, null, null);

            logic.ModifyPizza(pizzas.FirstOrDefault(), "name", "1. Change Name Pizza");
            logic.ModifyPizza(pizzas.FirstOrDefault(), "sale", "0.8");

            mockedPizRepo.Verify(pizRepo => pizRepo.ChangePizzaName(It.IsAny<int>(), It.IsAny<string>()), Times.AtMostOnce);
            mockedPizRepo.Verify(pizRepo => pizRepo.ChangeSale(It.IsAny<int>(), It.IsAny<float>()), Times.AtMostOnce);
        }

        /// <summary>
        /// Tests the RemovePizza() method if the PizzaManagement.
        /// Tested CRUDs:
        ///     -PizzaRepo.Remove().
        /// </summary>
        [Test]
        public static void PizzaManagementLogicRemovePizza()
        {
            MainTest main = new MainTest();
            IQueryable<Pizza> pizzas = main.Pizzas.AsQueryable();
            Mock<IPizzaRepository> mockedPizRepo = new Mock<IPizzaRepository>(MockBehavior.Loose);

            mockedPizRepo.Setup(pizRepo => pizRepo.Remove(It.IsAny<Pizza>()));

            PizzaManagement logic = new PizzaManagement(mockedPizRepo.Object, null, null, null);

            logic.RemovePizza(pizzas.FirstOrDefault());

            mockedPizRepo.Verify(pizRepo => pizRepo.Remove(It.IsAny<Pizza>()), Times.AtMostOnce);
        }
    }
}
