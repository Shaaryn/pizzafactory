﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Web.Models
{
    using AutoMapper;

    /// <summary>
    /// Mapper factory.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// Mapping route for the properties.
        /// </summary>
        /// <returns>IMapper.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<PizzaFactory.Data.Models.Pizza, PizzaFactory.Web.Models.Pizza>().
                    ForMember(dest => dest.Id, map => map.MapFrom(src => src.Id)).
                    ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name)).
                    ForMember(dest => dest.Size, map => map.MapFrom(src => src.Size)).
                    ForMember(dest => dest.Price, map => map.MapFrom(src => src.Price)).
                    ForMember(dest => dest.FurtherExpenses, map => map.MapFrom(src => src.FurtherExpenses)).
                    ForMember(dest => dest.SaleMultiplier, map => map.MapFrom(src => src.SaleMultiplier)).ReverseMap();
                    });
            return config.CreateMapper();
        }
    }
}
