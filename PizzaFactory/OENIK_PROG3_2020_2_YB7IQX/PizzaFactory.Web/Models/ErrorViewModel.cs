// <copyright file="ErrorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Web.Models
{
    /// <summary>
    /// Ny�lv�nval�.
    /// </summary>
    public class ErrorViewModel
    {
        /// <summary>
        /// Gets or sets EzLegal�bb�rthet�K�dR�sz.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Gets a value indicating whether nemTudomCuppressLenneAJobbVagyEzAmit�ppCsin�lok...
        /// </summary>
        public bool ShowRequestId => !string.IsNullOrEmpty(this.RequestId);
    }
}
