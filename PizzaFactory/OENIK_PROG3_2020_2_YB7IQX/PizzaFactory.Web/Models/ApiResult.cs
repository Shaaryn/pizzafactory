﻿// <copyright file="ApiResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Web.Models
{
    /// <summary>
    /// Class responsible to store the result.
    /// </summary>
    public class ApiResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether operation was successfull or not.
        /// </summary>
        public bool OperationResult { get; set; }
    }
}
