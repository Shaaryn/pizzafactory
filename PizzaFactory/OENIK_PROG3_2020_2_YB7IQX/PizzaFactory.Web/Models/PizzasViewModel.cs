﻿// <copyright file="PizzasViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Web.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// VM of the Pizza model.
    /// </summary>
    public class PizzasViewModel
    {
        /// <summary>
        /// Gets or sets the currently edited pizza.
        /// </summary>
        public Pizza EditedPizza { get; set; }

        /// <summary>
        /// Gets or sets the list of all pizzas.
        /// </summary>
        public List<Pizza> ListOfPizzas { get; set; }
    }
}
