﻿// <copyright file="Pizza.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Web.Models
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// The Pizza class holds information regarding a specific pizza.
    /// </summary>
    public class Pizza
    {
        /// <summary>
        /// Gets or sets primary key of the person class, on which the other attributes depend.
        /// </summary>
        [Display(Name = "Pizza id")]
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets name of the pizza which includes its number (not always the same as its Id field).
        /// </summary>
        [Display(Name = "Pizza name")]
        [Required]
        [StringLength(30, MinimumLength = 5)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets size of the pizza which can either be 30 or 45.
        /// </summary>
        [Display(Name = "Pizza size")]
        [Required]
        public int Size { get; set; }

        /// <summary>
        /// Gets or sets price of the pizza which is calcualted by the sum of the ingredients it contains which sum is multiplied by the FurtherExpenses field.
        /// </summary>
        [Display(Name = "Pizza price")]
        [Required]
        public int Price { get; set; }

        /// <summary>
        /// Gets or sets price multiplier, it contains the cost of a pizza box, manpower wage, oven kWh, etc.
        /// </summary>
        [Display(Name = "Pizza further expenses")]
        [Required]
        public float FurtherExpenses { get; set; }

        /// <summary>
        /// Gets or sets defines a multiplier that affects the price of the pizza, usually decreasing it since it is a sale.
        /// </summary>
        [Display(Name = "Pizza sale multiplier")]
        [Required]
        public float SaleMultiplier { get; set; }
    }
}
