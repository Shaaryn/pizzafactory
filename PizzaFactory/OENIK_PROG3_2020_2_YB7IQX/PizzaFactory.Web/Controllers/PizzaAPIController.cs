﻿// <copyright file="PizzaAPIController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Web.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using PizzaFactory.Logic.Interfaces;
    using PizzaFactory.Web.Models;

    /// <summary>
    /// Controller for the API.
    /// </summary>
    public class PizzaAPIController : Controller
    {
        private IPizzaManagement logic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="PizzaAPIController"/> class.
        /// </summary>
        /// <param name="logic">Logic of the Pizza management.</param>
        /// <param name="mapper">Mapper that provides the conenction route service.</param>
        public PizzaAPIController(IPizzaManagement logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Fetches all pizzas from the database.
        /// </summary>
        /// <returns>Mapped Models.Pizza list.</returns>
        [HttpGet]// default : PizzaApi/GetAll
        [ActionName("all")]// GET PizzaApi/all
        public IEnumerable<Models.Pizza> GetAll()
        {
            var pizzas = this.logic.GetAllPizzas();
            return this.mapper.Map<IList<Data.Models.Pizza>, IList<Models.Pizza>>(pizzas);
        }

        /// <summary>
        /// Removes a pizza.
        /// </summary>
        /// <param name="id">Id of the pizaz we want to remove.</param>
        /// <returns>Operation result relative to this task.</returns>
        [HttpGet]// default : GET  PizzaApi/DeleteAPizza
        [ActionName("del")]// GET PizzaApi/delete
        public ApiResult DeleteAPizza(int id)
        {
            try
            {
                this.logic.RemovePizza(this.logic.GetOnePizza(id));
            }
            catch (System.Exception)
            {
                return new ApiResult() { OperationResult = false };
                throw;
            }

            return new ApiResult() { OperationResult = true };
        }

        /// <summary>
        /// Adds a pizza.
        /// </summary>
        /// <param name="pizza">Pizza that contains the values.</param>
        /// <returns>Operation result relative to this task.</returns>
        [HttpPost]// default : POST PizzaApi/AddAPizza
        [ActionName("add")]// POST PizzaApi/add
        public ApiResult AddAPizza(Models.Pizza pizza)
        {
            try
            {
                this.logic.AddPizza(pizza.Name, pizza.Size, pizza.Price, pizza.FurtherExpenses, pizza.SaleMultiplier);
            }
            catch (System.Exception)
            {
                return new ApiResult() { OperationResult = false };
                throw;
            }

            return new ApiResult() { OperationResult = true };
        }

        /// <summary>
        /// Modifies a pizza.
        /// </summary>
        /// <param name="pizza">Pizza that contains the modified values.</param>
        /// <returns>Operation result relative to this task.</returns>
        [HttpPost]// default : POST PizzaApi/ModifyAPizza
        [ActionName("mod")]// POST PizzaApi/modify
        public ApiResult ModifyAPizza(Models.Pizza pizza)
        {
            try
            {
                this.logic.EditPizza(pizza.Id, this.mapper.Map<Models.Pizza, Data.Models.Pizza>(pizza));
            }
            catch (System.Exception)
            {
                return new ApiResult() { OperationResult = false };
                throw;
            }

            return new ApiResult() { OperationResult = true };
        }
    }
}
