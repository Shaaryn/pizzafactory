﻿// <copyright file="PizzasController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using PizzaFactory.Logic.Interfaces;
    using PizzaFactory.Web.Models;

    /// <summary>
    /// Controller for the MVC.
    /// </summary>
    public class PizzasController : Controller
    {
        private IPizzaManagement logic;
        private IMapper mapper;
        private PizzasViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="PizzasController"/> class.
        /// </summary>
        /// <param name="logic">Logic of the Pizza management.</param>
        /// <param name="mapper">Mapper that provides the conenction route service.</param>
        public PizzasController(IPizzaManagement logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;

            this.vm = new PizzasViewModel();
            this.vm.EditedPizza = new Models.Pizza();

            var pizzas = logic.GetAllPizzas();
            this.vm.ListOfPizzas = mapper.Map<IList<Data.Models.Pizza>, List<Models.Pizza>>(pizzas);
        }

        /// <summary>
        /// Index action.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("PizzasIndex", this.vm);
        }

        /// <summary>
        /// Details action.
        /// </summary>
        /// <param name="id">id of the pizza.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult Details(int id)
        {
            return this.View("PizzasDetails", this.GetPizzaModel(id));
        }

        /// <summary>
        /// Delete action.
        /// </summary>
        /// <param name="id">id of the pizza.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult Remove(int id)
        {
            this.TempData["editResult"] = "Delete failed.";
            this.logic.RemovePizza(this.logic.GetOnePizza(id));
            this.TempData["editResult"] = "Delete succeeded.";
            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Edit action.
        /// </summary>
        /// <param name="id">id of the pizza.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult Edit(int id)
        {
            this.TempData["editAction"] = "Edit";
            this.vm.EditedPizza = this.GetPizzaModel(id);
            return this.View("PizzasIndex", this.vm);
        }

        /// <summary>
        /// Edit diff.
        /// </summary>
        /// <param name="pizza">Pizza we are editing.</param>
        /// <param name="editAction">Action.</param>
        /// <returns>ActionResult.</returns>
        // POST Crud/Edit + send 1 car
        [HttpPost]
        public ActionResult Edit(Models.Pizza pizza, string editAction)

        // Because of Model Binder
        {
            if (this.ModelState.IsValid && pizza != null)
            {
                this.TempData["editResult"] = "Edit sucessful.";
                if (editAction == "AddNew")
                {
                    try
                    {
                        this.logic.AddPizza(pizza.Name, pizza.Size, pizza.Price, pizza.FurtherExpenses, pizza.SaleMultiplier);
                    }
                    catch (ArgumentException ex)
                    {
                        this.TempData["editResult"] = "Insert FAIL: " + ex.Message;
                    }
                }
                else
                {
                    this.logic.EditPizza(pizza.Id, this.mapper.Map<Models.Pizza, Data.Models.Pizza>(pizza));
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.vm.EditedPizza = pizza;
                return this.View("PizzasIndex", this.vm);
            }
        }

        private Models.Pizza GetPizzaModel(int id)
        {
            Data.Models.Pizza onePizza = this.logic.GetOnePizza(id);
            return this.mapper.Map<Data.Models.Pizza, Models.Pizza>(onePizza);
        }
    }
}
