﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.NamingRules", "SA1309:Field names should not begin with underscore", Justification = "More understandable")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = ".")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = ".")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = ".")]
[assembly: SuppressMessage("Design", "CA1014:Mark assemblies with CLSCompliant", Justification = "No assemblyinfo.cs")]
