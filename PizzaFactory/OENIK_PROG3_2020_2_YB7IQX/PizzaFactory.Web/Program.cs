// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Web
{
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Hosting;

    /// <summary>
    /// .
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// .
        /// </summary>
        /// <param name="args">Asd.</param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

    /// <summary>
    /// .
    /// </summary>
    /// <param name="args">Asd.</param>
    /// <returns>Assd.</returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
