﻿// <copyright file="LogInEmployeeNotFoundException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Logic.Exceptions
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Exception for cases when we cant find the user (Employee).
    /// </summary>
    public class LogInEmployeeNotFoundException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LogInEmployeeNotFoundException"/> class.
        /// Empty ctor.
        /// </summary>
        public LogInEmployeeNotFoundException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogInEmployeeNotFoundException"/> class.
        /// Message only.
        /// </summary>
        /// <param name="message">String message of what could possibly raise the exception.</param>
        public LogInEmployeeNotFoundException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogInEmployeeNotFoundException"/> class.
        /// Message with the option of stating the inner exception.
        /// </summary>
        /// <param name="message">String message of what could possibly raise the exception.</param>
        /// <param name="innerException">Inner exception that can filter the cause further.</param>
        public LogInEmployeeNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
