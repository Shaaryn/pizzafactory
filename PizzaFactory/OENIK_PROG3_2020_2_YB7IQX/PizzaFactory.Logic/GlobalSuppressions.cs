﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.NamingRules", "SA1309:Field names should not begin with underscore", Justification = "A habit I inherited from the internet. Helps me keep track of those variables that requires priority care. Shouldn't be modifed or when modified should done that with care. Mostly used with the full property option but still helps me here.")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "I dont have the energy and time to research this topic.")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "I would like to keep it as a List since it is not a real 'collection'", Scope = "member", Target = "~P:PizzaFactory.Logic.Classes.OrderManagement.OrderedPizzas")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "I need the variable in another class to retrieve its contents.", Scope = "member", Target = "~P:PizzaFactory.Logic.Classes.RenewableEmployees.Order")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "I dont have the energy and time to research this topic.")]
