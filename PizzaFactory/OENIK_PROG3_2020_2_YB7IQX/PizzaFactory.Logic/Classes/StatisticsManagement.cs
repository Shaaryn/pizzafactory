﻿// <copyright file="StatisticsManagement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Logic.Classes
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Data.Models.Connector;
    using PizzaFactory.Data.Models.Human;
    using PizzaFactory.Logic.Interfaces;
    using PizzaFactory.Repository;
    using PizzaFactory.Repository.Classes;
    using PizzaFactory.Repository.Interfaces;

    /// <summary>
    /// Manages the Statistics related business logic.
    /// </summary>
    public class StatisticsManagement : IStatisticsManagement
    {
        private IPizzeriaRepository pizzeriaRepo;
        private IPersonRepository perRepo;
        private IEmployeeRepository empRepo;
        private ICustomerRepository cusRepo;
        private IPizzaRepository pizzaRepo;
        private IIngredientRepository ingRepo;
        private IOrderRepository ordRepo;
        private IConnectorOrderPizzaRepository copRepo;
        private IConnectorPizzaIngredientRepository cpiRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsManagement"/> class.
        /// </summary>
        /// <param name="givenPizzeriaR">Repository of the Pizzeria entities.</param>
        /// <param name="givenPerR">Repository of the Person entities.</param>
        /// <param name="givenCR">Repository of the Customer entities.</param>
        /// <param name="givenPizzaR">Repository of the Pizza entities.</param>
        /// <param name="givenER">Repository of the Employee entities.</param>
        /// <param name="givenOrdR">Repository of the Order entities.</param>
        /// <param name="givenIngRepo">Repository of the Ingredient entities.</param>
        /// <param name="givenCop">Repository of the COP entities.</param>
        /// <param name="givenCpi">Repository of the CPI entities.</param>
        public StatisticsManagement(IPizzeriaRepository givenPizzeriaR, IPersonRepository givenPerR, ICustomerRepository givenCR, IPizzaRepository givenPizzaR, IEmployeeRepository givenER, IOrderRepository givenOrdR, IIngredientRepository givenIngRepo, IConnectorOrderPizzaRepository givenCop, IConnectorPizzaIngredientRepository givenCpi)
        {
            // Dependency Injection.
            this.perRepo = givenPerR;
            this.empRepo = givenER;
            this.cusRepo = givenCR;
            this.pizzaRepo = givenPizzaR;
            this.pizzeriaRepo = givenPizzeriaR;
            this.ordRepo = givenOrdR;
            this.ingRepo = givenIngRepo;
            this.copRepo = givenCop;
            this.cpiRepo = givenCpi;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns>Collection of ingredients.</returns>
        public Collection<Ingredient> GetAllIngredients()
        {
            Collection<Ingredient> result = new Collection<Ingredient>(this.ingRepo.GetAll().ToList());
            return result;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns>List of pre-defined class that stores the specified values.</returns>
        public IList<RenewableEmployees> RenewableMoney()
        {
            IQueryable<Person> allPeople = this.perRepo.GetAll();
            IQueryable<Employee> allEmployees = this.empRepo.GetAll();
            IQueryable<Customer> allCustomers = this.cusRepo.GetAll();

            var customerEmployees = from person in allPeople
                                    join employee in allEmployees
                                    on person.EmployeeId equals employee.Id
                                    join customer in allCustomers
                                    on person.CustomerId equals customer.Id
                                    select new RenewableEmployees()
                                    {
                                        PersonQ = person,
                                        EmployeeQ = employee,
                                        CustomerQ = customer,
                                        Order = new Collection<Order>(customer.Orders.ToList()),
                                        SpentQ = customer.Orders.ToList().Sum(o => o.Cost),
                                    };

            return customerEmployees.ToList();
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns>List of pre-defined class that stores the specified values.</returns>
        public Task<IList<RenewableEmployees>> RenewableMoneyAsync()
        {
            return Task.Run(this.RenewableMoney);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenParam">Given substring that has to be in the name of the ingredient.</param>
        /// <param name="givenLower">Given lower bound of the ingredient cost.</param>
        /// <param name="givenUpper">Given upper bound of the ingredient cost.</param>
        /// <returns>List of matching elements.</returns>
        public IList<Pizza> PizzaSearch(string givenParam, int givenLower, int givenUpper)
        {
            IQueryable<Pizza> allPizza = this.pizzaRepo.GetAll();
            IQueryable<Ingredient> allIngred = this.ingRepo.GetAll();
            IQueryable<PizzaIngredientConnector> allCPI = this.cpiRepo.GetAll();

            var matchingPizzas = from cpi in allCPI
                                 join pizza in allPizza
                                 on cpi.PizzaId equals pizza.Id
                                 join ingredient in allIngred
                                 on cpi.IngredientId equals ingredient.Id
                                 let lowerBound = givenLower
                                 let upperBound = givenUpper
                                 let containsString = ingredient.Name.Contains(givenParam)
                                 where ingredient.Cost <= givenUpper && ingredient.Cost >= lowerBound
                                 where containsString == true
                                 select pizza;

            return matchingPizzas.ToList();
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenParam">Given substring that has to be in the name of the ingredient.</param>
        /// <param name="givenLower">Given lower bound of the ingredient cost.</param>
        /// <param name="givenUpper">Given upper bound of the ingredient cost.</param>
        /// <returns>List of matching elements.</returns>
        public Task<IList<Pizza>> PizzaSearchAsync(string givenParam, int givenLower, int givenUpper)
        {
            return Task.Run(() => this.PizzaSearch(givenParam, givenLower, givenUpper));
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>S
        /// <param name="givenParam">String representation of an ingredient.</param>
        /// <returns>Tuple of two lists with the first containing the pizzas and the second containing the customers.</returns>
        public (IList<Pizza>, IList<string>) ToppingLovers(string givenParam)
        {
            IQueryable<Pizza> allPizzas = this.pizzaRepo.GetAll();
            IQueryable<Ingredient> allIngred = this.ingRepo.GetAll();
            IQueryable<PizzaIngredientConnector> allCPI = this.cpiRepo.GetAll();

            IQueryable<Customer> allCustomers = this.cusRepo.GetAll();
            IQueryable<Order> allOrders = this.ordRepo.GetAll();
            IQueryable<OrderPizzaConnector> allOPC = this.copRepo.GetAll();

            var toppingPizzas = from cpi in allCPI
                                join pizza in allPizzas
                                on cpi.PizzaId equals pizza.Id
                                join ingred in allIngred
                                on cpi.IngredientId equals ingred.Id
                                where ingred.Name == givenParam
                                select pizza;

            var whichOrderContains = from opc in allOPC
                                     join order in allOrders
                                     on opc.OrderId equals order.Id
                                     join item in toppingPizzas
                                     on opc.PizzaId equals item.Id
                                     select opc;

            var whoOrderedThose = from order in allOrders
                                  join customer in allCustomers
                                  on order.CustomerId equals customer.Id
                                  join item in whichOrderContains
                                  on order.Id equals item.OrderId
                                  group order by order.CustomerId into grp
                                  orderby grp.Count() descending
                                  select new
                                  {
                                      CustomerId = grp.Key,
                                      Count = grp.Count(),
                                  };

            var giveThemNames = from customer in allCustomers
                                join item in whoOrderedThose
                                on customer.Id equals item.CustomerId
                                select customer.Person.FullName;

            return (toppingPizzas.ToList(), giveThemNames.ToList());
        }

        /// <summary>
        /// Async version of the ToppingLovers() method.
        /// </summary>
        /// <param name="givenParam">String representation of an ingredient.</param>
        /// <returns>Tuple of two lists with the first containing the pizzas and the second containing the customers.</returns>
        public Task<(IList<Pizza>, IList<string>)> ToppingLoversAync(string givenParam)
        {
            return Task.Run(() => this.ToppingLovers(givenParam));
        }
    }
}
