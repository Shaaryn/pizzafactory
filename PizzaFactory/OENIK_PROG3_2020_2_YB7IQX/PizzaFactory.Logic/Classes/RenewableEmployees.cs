﻿// <copyright file="RenewableEmployees.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Logic.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Text;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Data.Models.Human;

    /// <summary>
    /// Holds the query data of its corresponding linq query.
    /// </summary>
    public class RenewableEmployees
    {
        private Person _person;
        private Employee _employee;
        private Customer _customer;
        private Collection<Order> _orders;
        private int _totalSpent;

        /// <summary>
        /// Gets or sets the Person field of the data.
        /// </summary>
        public Person PersonQ
        {
            get { return this._person; }
            set { this._person = value; }
        }

        /// <summary>
        /// Gets or sets the Employee field of the data.
        /// </summary>
        public Employee EmployeeQ
        {
            get { return this._employee; }
            set { this._employee = value; }
        }

        /// <summary>
        /// Gets or sets the Employee field of the data.
        /// </summary>
        public Customer CustomerQ
        {
            get { return this._customer; }
            set { this._customer = value; }
        }

        /// <summary>
        /// Gets or sets the collection of orders of the data.
        /// </summary>
        public Collection<Order> Order
        {
            get { return this._orders; }
            set { this._orders = value; }
        }

        /// <summary>
        /// Gets or sets the spent money of the employee.
        /// </summary>
        public int SpentQ
        {
            get { return this._totalSpent; }
            set { this._totalSpent = value; }
        }

        /// <summary>
        /// Concatonates the fields into a compact string that represents the instance.
        /// </summary>
        /// <returns>formatted string.</returns>
        public override string ToString()
        {
            return $" {this._person.FullName} (Role : {this._employee.RoleOfEmployee}) renewed a total of {this._totalSpent} krajcár.";
        }

        /// <summary>
        /// We check if the Person and the Employee objects are equal or not.
        /// </summary>
        /// <param name="obj">Given object that we validate if it is a type of the expected class.</param>
        /// <returns>final boolean value of the operations.</returns>
        public override bool Equals(object obj)
        {
            if (obj is RenewableEmployees)
            {
                RenewableEmployees tmp = obj as RenewableEmployees;

                if (tmp.PersonQ.Equals(this._person) &&
                    tmp.EmployeeQ.Equals(this._employee) &&
                    tmp.CustomerQ.Equals(this._customer))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Every field is represented by an integer value which values are summarized and being returned.
        /// </summary>
        /// <returns>sum of integer values.</returns>
        public override int GetHashCode()
        {
            return this._person.GetHashCode() + this._employee.GetHashCode() + this._customer.GetHashCode();
        }
    }
}
