﻿// <copyright file="OrderManagement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Logic.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using Microsoft.Extensions.Caching.Memory;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Data.Models.Connector;
    using PizzaFactory.Data.Models.Human;
    using PizzaFactory.Logic.Interfaces;
    using PizzaFactory.Repository;
    using PizzaFactory.Repository.Classes;
    using PizzaFactory.Repository.Interfaces;

    /// <summary>
    /// Manages the Order related business logic.
    /// </summary>
    public class OrderManagement : IOrderManagement
    {
        private IPizzeriaRepository pizzeriaRepo;
        private IPersonRepository perRepo;
        private ICustomerRepository cusRepo;
        private IPizzaRepository pizzaRepo;
        private IOrderRepository ordRepo;
        private ConnectorOrderPizzaRepository copRepo;

        private Employee _orderingEmployee;
        private List<Pizza> _orderedPizzas;
        private List<int> _orderedAmounts;
        private Customer _orderingCustomer;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderManagement"/> class.
        /// </summary>
        /// <param name="givenPizzeriaR">Repository of the Pizzeria entities.</param>
        /// <param name="givenPerR">Repository of the Person entities.</param>
        /// <param name="givenCR">Repository of the Custoemr entities.</param>
        /// <param name="givenPizR">Repository of the Pizza entities.</param>
        /// <param name="givenOrdR">Repository of the Order entities.</param>
        /// <param name="givenCop">Repository of the ConnectorOrderPizza entities.</param>
        /// <param name="givenEmployee">Employee, who is placing the order.</param>
        public OrderManagement(IPizzeriaRepository givenPizzeriaR, IPersonRepository givenPerR, ICustomerRepository givenCR, IPizzaRepository givenPizR, IOrderRepository givenOrdR, ConnectorOrderPizzaRepository givenCop, Employee givenEmployee)
        {
            // Dependency Injection.
            this.pizzeriaRepo = givenPizzeriaR;
            this.perRepo = givenPerR;
            this.cusRepo = givenCR;
            this.pizzaRepo = givenPizR;
            this.ordRepo = givenOrdR;
            this.copRepo = givenCop;

            // Order instance data.
            this._orderingEmployee = givenEmployee;
            this._orderingCustomer = new Customer();
            this._orderedPizzas = new List<Pizza>();
            this._orderedAmounts = new List<int>();
        }

        /// <summary>
        /// Gets or sets the customer field of the current order's customer.
        /// </summary>
        public Customer OrderingCustomer
        {
            get { return this._orderingCustomer; }
            set { this._orderingCustomer = value; }
        }

        /// <summary>
        /// Gets the ordered pizzas of the current order.
        /// </summary>
        public List<Pizza> OrderedPizzas
        {
            get { return this._orderedPizzas; }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenPhoneNumber">Phone number which is the base of the search.</param>
        /// <returns>Selected customer or person entity.</returns>
        public (Customer, Person) SearchCustomer(string givenPhoneNumber)
        {
            Person selectedPerson = this.perRepo.GetOneByPhone(givenPhoneNumber);

            if (selectedPerson != null)
            {
                int? id = selectedPerson.CustomerId;

                switch (id.HasValue)
                {
                    case true:

                        Customer selectedCustomer = this.cusRepo.GetOne(id ?? -1);    // Cant be -1 cause it has a value (which cant be -1), but i dunno how else can I make the VS accept it.
                        return (selectedCustomer, null);

                    case false:

                        if (selectedPerson.GetHashCode() != 0)
                        {
                            return (null, selectedPerson);
                        }
                        else
                        {
                            return (null, null);
                        }
                }
            }
            else
            {
                return (null, null);
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenPhoneNumber">Phone number of the new person.</param>
        /// <param name="givenFullName">Full name of the new person.</param>
        /// <param name="givenAddress">Address of the new person.</param>
        /// <returns>Selected customer entity.</returns>
        public Customer AddCustomer(string givenPhoneNumber, string givenFullName, string givenAddress)
        {
            Person newPerson = new Person()
            {
                PhoneNumber = givenPhoneNumber,
                FullName = givenFullName,
                Address = givenAddress,
            };

            IQueryable<Person> allPeople = this.perRepo.GetAll();

            int match = -1;  // In case the person already exists.
            foreach (Person item in allPeople)
            {
                if (item.Equals(newPerson))
                {
                    match = item.Id;
                }
            }

            if (match == -1)
            {
                this.perRepo.Insert(newPerson);

                int id = this.perRepo.GetOneByPhone(givenPhoneNumber).Id;

                Customer newCustomer = new Customer() { PersonId = id, Person = this.perRepo.GetOne(id), RegisteredOn = DateTime.Now };
                this.cusRepo.Insert(newCustomer);

                return newCustomer;
            }
            else
            {
                Customer newCustomer = new Customer() { PersonId = match, Person = this.perRepo.GetOne(match), RegisteredOn = DateTime.Now };
                this.cusRepo.Insert(newCustomer);

                return newCustomer;
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns>IList of the available pizzas.</returns>
        public IList<Pizza> GetAllPizzas()
        {
            return this.pizzaRepo.GetAll().ToList();
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenItem">Pizza item that which will be added to the collection.</param>
        /// <param name="givenAmount">Quantity of the given pizza item.</param>
        public void SetShoppingCartItem(Pizza givenItem, int givenAmount)
        {
            this.OrderedPizzas.Add(givenItem);
            this._orderedAmounts.Add(givenAmount);

            return;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns>Returns a tuple containing the pizza collection and its corresponding amount collection.</returns>
        public (List<Pizza>, List<int>) GetShoppingCart()
        {
            return (this.OrderedPizzas, this._orderedAmounts);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenId">Id of the element we want to remove.</param>
        public void RemoveShoppingCartItem(int givenId)
        {
            this.OrderedPizzas.RemoveAt(givenId);
            this._orderedAmounts.RemoveAt(givenId);

            return;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public void PlaceOrder()
        {
            ICollection<OrderPizzaConnector> connectors = new Collection<OrderPizzaConnector>();
            int orderCost = 0;

            for (int i = 0; i < this.OrderedPizzas.Count; i++)
            {
                OrderPizzaConnector tmpItem = new OrderPizzaConnector()
                {
                    Pizza = this.OrderedPizzas[i],
                    PizzaId = this.OrderedPizzas[i].Id,
                    Amount = this._orderedAmounts[i],
                    TotalPrice = this.OrderedPizzas[i].Price * this._orderedAmounts[i],
                };
                connectors.Add(tmpItem);
            }

            foreach (OrderPizzaConnector item in connectors)
            {
                orderCost += item.TotalPrice;
            }

            Order newOrder = new Order()
            {
                TimeStamp = DateTime.Now,
                Cost = orderCost,
                Customer = this._orderingCustomer,
                CustomerId = this._orderingCustomer.Id,
                Employee = this._orderingEmployee,
                EmployeeId = this._orderingEmployee.Id,
                Pizzeria = this.pizzeriaRepo.GetOne(1),
                PizzeriaId = this.pizzeriaRepo.GetOne(1).Id,
                Connector = connectors,
            };
            this.ordRepo.Insert(newOrder);

            if (this._orderingCustomer.RegisteredOn == default)
            {
                this.cusRepo.GiveRegisteredOn(this._orderingCustomer.Id);
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns>Returns with a collection of order entities.</returns>
        public Collection<Order> GetAllOrders()
        {
            return new Collection<Order>(this.ordRepo.GetAll().ToList());
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenId">Id of the order we want to remove.</param>
        public void RemoveOrder(int givenId)
        {
            // Order toRemove = this.ordRepo.GetOne(givenId);
            // this.ordRepo.Remove(toRemove);
            this.ordRepo.Storno(givenId);

            return;
        }
    }
}
