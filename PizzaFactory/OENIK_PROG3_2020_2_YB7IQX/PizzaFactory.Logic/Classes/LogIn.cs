﻿// <copyright file="LogIn.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Logic.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Data.Models.Human;
    using PizzaFactory.Logic.Exceptions;
    using PizzaFactory.Logic.Interfaces;
    using PizzaFactory.Repository.Interfaces;

    /// <summary>
    /// Logical implementation of the LogIn operations.
    /// </summary>
    public class LogIn : ILogIn
    {
        private IEmployeeRepository employeeRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogIn"/> class.
        /// Dependency Injection (Employee repository).
        /// </summary>
        /// <param name="givenEmpRepo">Provided repository of the employees.</param>
        public LogIn(IEmployeeRepository givenEmpRepo)
        {
            this.employeeRepo = givenEmpRepo;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenFullName">Full name of the employee, it is they username.</param>
        /// <returns>Returns the logged in employee's personal data.</returns>
        public Employee GetOnePerson(string givenFullName)
        {
            Employee tmp = this.employeeRepo.GetOne(givenFullName);

            if (tmp != null)
            {
                return tmp;
            }
            else
            {
                throw new LogInEmployeeNotFoundException(" Couldn't find the user ! ");
            }
        }
    }
}
