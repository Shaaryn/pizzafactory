﻿// <copyright file="PizzaManagement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Logic.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Data.Models.Connector;
    using PizzaFactory.Logic.Interfaces;
    using PizzaFactory.Repository.Classes;
    using PizzaFactory.Repository.Interfaces;

    /// <summary>
    /// Manages the pizza's operations.
    /// </summary>
    public class PizzaManagement : IPizzaManagement
    {
        private IPizzaRepository pizRepo;
        private IIngredientRepository ingRepo;
        private ConnectorPizzaIngredientRepository cpiRepo;
        private ConnectorOrderPizzaRepository copRepo;

        private List<Ingredient> addPizzaSelectedIngredients;
        private Pizza newPizza;

        /// <summary>
        /// Initializes a new instance of the <see cref="PizzaManagement"/> class.
        /// </summary>
        /// <param name="givenPizRepo">Repository of the Pizza entities.</param>
        /// <param name="givenIngRepo">Repository of the Ingredients repository.</param>
        /// <param name="givenCPI">Repository of the CPI entities.</param>
        /// <param name="givenCOP">Repository of the COP entities.</param>
        public PizzaManagement(IPizzaRepository givenPizRepo, IIngredientRepository givenIngRepo, ConnectorPizzaIngredientRepository givenCPI, ConnectorOrderPizzaRepository givenCOP)
        {
            // Dependency injection
            this.pizRepo = givenPizRepo;
            this.ingRepo = givenIngRepo;
            this.cpiRepo = givenCPI;
            this.copRepo = givenCOP;

            // BL instance data
            this.addPizzaSelectedIngredients = new List<Ingredient>();
            this.newPizza = new Pizza();
        }

        /// <summary>
        /// Gets or sets the publicly visible property of the new pizza this BL can create.
        /// </summary>
        public Pizza NewPizza
        {
            get { return this.newPizza; }
            set { this.newPizza = value; }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenId">Id of the pizza we want to retrieve.</param>
        /// <returns>Returns a single pizza entity.</returns>
        public Pizza GetOnePizza(int givenId)
        {
            return this.pizRepo.GetOne(givenId);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenItem">Pizza entity that has to be removed.</param>
        public void RemovePizza(Pizza givenItem)
        {
            this.pizRepo.Remove(givenItem);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns>IList of the available pizzas.</returns>
        public IList<Pizza> GetAllPizzas()
        {
            return this.pizRepo.GetAll().ToList();
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns>IList of the available pizzas.</returns>
        public IList<Ingredient> GetAllIngredients()
        {
            return this.ingRepo.GetAll().ToList();
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns>Returns a collection of ingredients which have been selected.</returns>
        public Collection<Ingredient> GetSelectedIngredients()
        {
            return new Collection<Ingredient>(this.addPizzaSelectedIngredients.ToList());
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenId">Id of the pizza we want to retrieve.</param>
        public void RemoveSelectedItem(int givenId)
        {
            this.addPizzaSelectedIngredients.RemoveAt(givenId);
            return;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenItem">Selected item entity given by the View.</param>
        public void AddSelectedItem(Ingredient givenItem)
        {
            this.addPizzaSelectedIngredients.Add(givenItem);
            return;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenItem">Pizza entity which needs to be modified or removed.</param>
        /// <param name="givenFunc">Given function that defines what operation shall we execute in the function.</param>
        /// <param name="givenValue">Given value that represents the new value of the field we want to change.</param>
        public void ModifyPizza(Pizza givenItem, string givenFunc, string givenValue)
        {
            if (givenItem == null)
            {
                return;
            }
            else
            {
                switch (givenFunc)
                {
                    case "name":
                        this.pizRepo.ChangePizzaName(givenItem.Id, givenValue);
                        break;
                    case "expenses":
                        this.pizRepo.ChangeExpenses(givenItem.Id, float.Parse(givenValue));
                        break;
                    case "sale":
                        this.pizRepo.ChangeSale(givenItem.Id, float.Parse(givenValue));
                        break;
                    case "size":
                        this.pizRepo.ChangeSize(givenItem.Id, givenValue);
                        break;
                }
            }
        }

        /// <summary>
        /// Modifies the pizza determined by the id and replaces it with the modified given pizza.
        /// </summary>
        /// <param name="id">Id of the pizza in the database that we want to replace.</param>
        /// <param name="givenPizza">Modified pizza.</param>
        public void EditPizza(int id, Pizza givenPizza)
        {
            this.pizRepo.ChangePizza(id, givenPizza);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public void SubmitNewPizza()
        {
            this.pizRepo.Insert(this.newPizza);

            for (int i = 0; i < this.addPizzaSelectedIngredients.Count; i++)
            {
                PizzaIngredientConnector tmpConnector = new PizzaIngredientConnector()
                {
                    Pizza = this.newPizza,
                    PizzaId = this.newPizza.Id,
                    Ingredient = this.addPizzaSelectedIngredients[i],
                    IngredientId = this.addPizzaSelectedIngredients[i].Id,
                };
                this.cpiRepo.Insert(tmpConnector);
            }
        }

        /// <summary>
        /// Adds a new pizaz to the database.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="size">Size.</param>
        /// <param name="price">Price.</param>
        /// <param name="further">Further Expenses.</param>
        /// <param name="sale">Sale Multipleir.</param>
        public void AddPizza(string name, int size, int price, float further, float sale)
        {
            Pizza tmp = new Pizza() { Name = name, Size = size, Price = price, FurtherExpenses = further, SaleMultiplier = sale };
            this.pizRepo.Insert(tmp);
        }
    }
}
