﻿// <copyright file="UserManagement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Logic.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Data.Models.Human;
    using PizzaFactory.Logic.Interfaces;
    using PizzaFactory.Repository;
    using PizzaFactory.Repository.Interfaces;

    /// <summary>
    /// Manages the user related business logic.
    /// </summary>
    public class UserManagement : IUserManagement
    {
        private IEmployeeRepository empRepo;
        private IPersonRepository perRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserManagement"/> class.
        /// </summary>
        /// <param name="givenEmpRepo">Repository of the Emplyoee entites.</param>
        /// <param name="givenPerRepo">Repository of the Person entities.</param>
        public UserManagement(IEmployeeRepository givenEmpRepo, IPersonRepository givenPerRepo)
        {
            // Dependency Injection
            this.empRepo = givenEmpRepo;
            this.perRepo = givenPerRepo;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns>Collection of employees.</returns>
        public Collection<Employee> GetAllUsers()
        {
            return new Collection<Employee>(this.empRepo.GetAll().ToList());
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenEmployee">Employee we want to change.</param>
        /// <param name="givenFunc">Whether we want to chagne 'name', 'address' or 'phone'.</param>
        /// <param name="givenParam">Given parameter to change the personal data accordingly.</param>
        public void ModifyEmployeePerson(Employee givenEmployee, string givenFunc, string givenParam)
        {
            if (givenEmployee == null)
            {
                // Exception
            }
            else
            {
                switch (givenFunc)
                {
                    case "name":
                        this.perRepo.ChangeFullName(givenEmployee.Person.Id, givenParam);

                        break;
                    case "address":
                        this.perRepo.ChangeAddress(givenEmployee.Person.Id, givenParam);

                        break;
                    case "phone":
                        this.perRepo.ChangePhoneNumber(givenEmployee.Person.Id, givenParam);

                        break;
                }
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenEmployee">Employee we want to change.</param>
        /// <param name="givenRole">String representation of the role we want to change it to.</param>
        public void ModifyEmployeeRole(Employee givenEmployee, string givenRole)
        {
            if (givenEmployee == null)
            {
                // Exception
            }
            else
            {
                if (givenRole == "rof")
                {
                    this.empRepo.ChangeRole(givenEmployee.Id, givenRole);
                    this.empRepo.Resignation(givenEmployee.Id, DateTime.Now);
                }
                else
                {
                    this.empRepo.ChangeRole(givenEmployee.Id, givenRole);
                }
            }

            return;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenPhone">Phone number of the new employee.</param>
        /// <param name="givenName">Full name of the new employee.</param>
        /// <param name="givenAddress">Address of the new employee.</param>
        /// <param name="givenRole">Role of the new employee.</param>
        public void AddEmployee(string givenPhone, string givenName, string givenAddress, string givenRole)
        {
            Person newPerson = new Person()
            {
                FullName = givenName,
                PhoneNumber = givenPhone,
                Address = givenAddress,
            };

            IQueryable<Person> allPeople = this.perRepo.GetAll();

            int match = -1;  // In case the person already exists.
            foreach (Person item in allPeople)
            {
                if (item.Equals(newPerson))
                {
                    match = item.Id;
                }
            }

            if (match == -1)
            {
                this.perRepo.Insert(newPerson);

                int id = this.perRepo.GetOneByPhone(givenPhone).Id;

                // Enum.TryParse(givenRole, out Role employeeRole);
                Employee newEmployee = new Employee()
                {
                    PersonId = id,
                    Person = this.perRepo.GetOne(id),
                    StartOfEmployment = DateTime.Now,
                };
                this.empRepo.Insert(newEmployee);

                this.empRepo.ChangeRole(newEmployee.Id, givenRole);
            }
            else
            {
                // Exception ambiguity
            }
        }
    }
}
