﻿// <copyright file="IStatisticsManagement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Logic.Interfaces
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Text;
    using System.Threading.Tasks;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Logic.Classes;

    /// <summary>
    /// Interface for the StatisticsManager methods.
    /// </summary>
    public interface IStatisticsManagement
    {
        /// <summary>
        /// Simply gets all the available ingredients.
        /// </summary>
        /// <returns>Collection of ingredients.</returns>
        Collection<Ingredient> GetAllIngredients();

        /// <summary>
        /// LINQ functionality.
        /// Searches for those employees who are also custoemers.
        /// We sum up how much they spent back to the pizzeria and rank them based on that.
        /// </summary>
        /// <returns>List of pre-defined class that stores the specified values.</returns>
        IList<RenewableEmployees> RenewableMoney();

        /// <summary>
        /// Async version of the RenewableMoney() method.
        /// </summary>
        /// <returns>List of pre-defined class that stores the specified values.</returns>
        public Task<IList<RenewableEmployees>> RenewableMoneyAsync();

        /// <summary>
        /// LINQ functionality. !INPUT dependent!
        /// Based on the three values below searches through all the pizazs we have
        /// and returns the ones that match with the criterias.
        /// </summary>
        /// <param name="givenParam">Given substring that has to be in the name of the ingredient.</param>
        /// <param name="givenLower">Given lower bound of the ingredient cost.</param>
        /// <param name="givenUpper">Given upper bound of the ingredient cost.</param>
        /// <returns>List of matching elements.</returns>
        IList<Pizza> PizzaSearch(string givenParam, int givenLower, int givenUpper);

        /// <summary>
        /// Async version of the PizzaSearch() method.
        /// </summary>
        /// <param name="givenParam">Given substring that has to be in the name of the ingredient.</param>
        /// <param name="givenLower">Given lower bound of the ingredient cost.</param>
        /// <param name="givenUpper">Given upper bound of the ingredient cost.</param>
        /// <returns>List of matching elements.</returns>
        public Task<IList<Pizza>> PizzaSearchAsync(string givenParam, int givenLower, int givenUpper);

        /// <summary>
        /// LINQ functionality. !INPUT dependent!
        /// Based on the value below searches through most of the tables to find every pizza containing that ingredient.
        /// Based on the result set, we search through the orders and return the customers who ordered the msot of those pizzas.
        /// We retrieve their names and rank them.
        /// </summary>
        /// <param name="givenParam">String representation of an ingredient.</param>
        /// <returns>Tuple of two lists with the first containing the pizzas and the second containing the customers.</returns>
        (IList<Pizza>, IList<string>) ToppingLovers(string givenParam);

        /// <summary>
        /// Async version of the ToppingLovers() method.
        /// </summary>
        /// <param name="givenParam">String representation of an ingredient.</param>
        /// <returns>Tuple of two lists with the first containing the pizzas and the second containing the customers.</returns>
        public Task<(IList<Pizza>, IList<string>)> ToppingLoversAync(string givenParam);
    }
}
