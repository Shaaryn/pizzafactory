﻿// <copyright file="IPizzaManagement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Text;
    using PizzaFactory.Data.Models;

    /// <summary>
    /// Interface for the PizzaManagement methods.
    /// </summary>
    public interface IPizzaManagement
    {
        /// <summary>
        /// Gets all the stored pizzas.
        /// </summary>
        /// <returns>IList of the available pizzas.</returns>
        IList<Pizza> GetAllPizzas();

        /// <summary>
        /// Gets all the stored ingredients.
        /// </summary>
        /// <returns>IList of the available pizzas.</returns>
        IList<Ingredient> GetAllIngredients();

        /// <summary>
        /// Simply retrieves the used collections as a source for the GUI.
        /// </summary>
        /// <returns>Returns a collection of ingredients which have been selected.</returns>
        Collection<Ingredient> GetSelectedIngredients();

        /// <summary>
        /// Gets the specific pizza from the corresponding repository.
        /// </summary>
        /// <param name="givenId">Id of the pizza we want to retrieve.</param>
        /// <returns>Returns a single pizza entity.</returns>
        Pizza GetOnePizza(int givenId);

        /// <summary>
        /// Removes a specific pizza from the correspondingrepository.
        /// </summary>
        /// <param name="givenItem">Pizza entity that has to be removed.</param>
        void RemovePizza(Pizza givenItem);

        /// <summary>
        /// Removes the item selected in the GUI. (Dyanmically implemented in the View).
        /// </summary>
        /// <param name="givenId">Id of the pizza we want to retrieve.</param>
        void RemoveSelectedItem(int givenId);

        /// <summary>
        /// Inserts the slected item to the end of the list. (Dynamically implemented in the View).
        /// </summary>
        /// <param name="givenItem">Selected item entity given by the View.</param>
        void AddSelectedItem(Ingredient givenItem);

        /// <summary>
        /// Ensures the possiblity for each pizza to be changed.
        /// Any property can be changed except the price and the ingredients.
        /// </summary>
        /// <param name="givenItem">Pizza entity which needs to be modified or removed.</param>
        /// <param name="givenFunc">Given function that defines what operation shall we execute in the function.</param>
        /// <param name="givenValue">Given value that represents the new value of the field we want to change.</param>
        void ModifyPizza(Pizza givenItem, string givenFunc, string givenValue);

        /// <summary>
        /// Modifies the pizza determined by the id and replaces it with the modified given pizza.
        /// </summary>
        /// <param name="id">Id of the pizza in the database that we want to replace.</param>
        /// <param name="givenPizza">Modified pizza.</param>
        void EditPizza(int id, Pizza givenPizza);

        /// <summary>
        /// Saves the newly created pizza and inserts it to the database.
        /// </summary>
        void SubmitNewPizza();

        /// <summary>
        /// Adds a new pizaz to the database.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="size">Size.</param>
        /// <param name="price">Price.</param>
        /// <param name="further">Further Expenses.</param>
        /// <param name="sale">Sale Multipleir.</param>
        public void AddPizza(string name, int size, int price, float further, float sale);
    }
}
