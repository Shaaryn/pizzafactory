﻿// <copyright file="IUserManagement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Text;
    using PizzaFactory.Data.Models.Human;

    /// <summary>
    /// Interface for the UserManagement methods.
    /// </summary>
    public interface IUserManagement
    {
        /// <summary>
        /// Retrieves all employees(users) from the database.
        /// </summary>
        /// <returns>Colelction of employees.</returns>
        Collection<Employee> GetAllUsers();

        /// <summary>
        /// Modifies the person property of the employee based on the passed function and its parameter.
        /// </summary>
        /// <param name="givenEmployee">Employee we want to change.</param>
        /// <param name="givenFunc">Whether we want to chagne 'name', 'address' or 'phone'.</param>
        /// <param name="givenParam">Given parameter to change the personal data accordingly.</param>
        void ModifyEmployeePerson(Employee givenEmployee, string givenFunc, string givenParam);

        /// <summary>
        /// Simply overrides the role of the emplyoee to its new role.
        /// Also changes the authorization level.
        /// </summary>
        /// <param name="givenEmployee">Employee we want to change.</param>
        /// <param name="givenRole">String representation of the role we want to change it to.</param>
        void ModifyEmployeeRole(Employee givenEmployee, string givenRole);

        /// <summary>
        /// Creates a new employee entity based on the passed parameters.
        /// </summary>
        /// <param name="givenPhone">Phone number of the new employee.</param>
        /// <param name="givenName">Full name of the new employee.</param>
        /// <param name="givenAddress">Address of the new employee.</param>
        /// <param name="givenRole">Role of the new employee.</param>
        void AddEmployee(string givenPhone, string givenName, string givenAddress, string givenRole);
    }
}
