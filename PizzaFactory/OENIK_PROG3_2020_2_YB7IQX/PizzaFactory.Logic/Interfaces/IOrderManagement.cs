﻿// <copyright file="IOrderManagement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Text;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Data.Models.Human;

    /// <summary>
    /// Interface for the OrderManagement methods.
    /// </summary>
    public interface IOrderManagement
    {
        /// <summary>
        /// Searches through the custoemr entities in the database to see if there is any match.
        /// </summary>
        /// <param name="givenPhoneNumber">Phone number which is the base of the search.</param>
        /// <returns>Selected customer entity.</returns>
        (Customer, Person) SearchCustomer(string givenPhoneNumber);

        /// <summary>
        /// Inserts a new customer into the database.
        /// If the person already exists and is an employee, but not a customer then createst a conenction between those.
        /// If the person already exists as a customer we simply throw an error.
        /// </summary>
        /// <param name="givenPhoneNumber">Phone number of the new person.</param>
        /// <param name="givenFullName">Full name of the new person.</param>
        /// <param name="givenAddress">Address of the new person.</param>
        /// <returns>Selected customer entity.</returns>
        Customer AddCustomer(string givenPhoneNumber, string givenFullName, string givenAddress);

        /// <summary>
        /// Gets all the stored pizzas.
        /// </summary>
        /// <returns>IList of the available pizzas.</returns>
        IList<Pizza> GetAllPizzas();

        /// <summary>
        /// Adds the specified pizza and its corresponding amount to the lists.
        /// </summary>
        /// <param name="givenItem">Pizza item that which will be added to the collection.</param>
        /// <param name="givenAmount">Quantity of the given pizza item.</param>
        void SetShoppingCartItem(Pizza givenItem, int givenAmount);

        /// <summary>
        /// Simple retrieves the used colelctions as a source for the GUI.
        /// </summary>
        /// <returns>Returns a tuple containing the pizza collection and its corresponding amount collection.</returns>
        (List<Pizza>, List<int>) GetShoppingCart();

        /// <summary>
        /// Simple removes a specific element of the lists.
        /// </summary>
        /// <param name="givenId">Id of the element we want to remove.</param>
        void RemoveShoppingCartItem(int givenId);

        /// <summary>
        /// Generates the database data based on the previously provided data.
        /// </summary>
        void PlaceOrder();

        /// <summary>
        /// Retrieves a list of the currently stored (in Db) orders.
        /// </summary>
        /// <returns>Returns with a collection of order entities.</returns>
        Collection<Order> GetAllOrders();

        /// <summary>
        /// Removes a specific order from the database.
        /// </summary>
        /// <param name="givenId">Id of the order we want to remove.</param>
        void RemoveOrder(int givenId);
    }
}
