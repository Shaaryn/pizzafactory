﻿// <copyright file="ILogIn.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Data.Models.Human;

    /// <summary>
    /// Interface for the LogIn methods.
    /// </summary>
    public interface ILogIn
    {
        /// <summary>
        /// Based on the employee's full name, we search for and return his or her Person object, which represents his or her account.
        /// </summary>
        /// <param name="givenFullName">Full name of the employee, it is they username.</param>
        /// <returns>Returns the logged in employee's personal data.</returns>
        Employee GetOnePerson(string givenFullName);
    }
}
