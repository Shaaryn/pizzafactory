﻿// <copyright file="MainVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.WpfClient
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// ViewModel of the UI.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private PizzaVM selectedPizza;
        private ObservableCollection<PizzaVM> allPizzas;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
        {
            this.logic = new MainLogic();    // Ugly hidden dep. :/

            this.LoadCmd = new RelayCommand(() => this.AllPizzas = new ObservableCollection<PizzaVM>(this.logic.ApiGetPizzas()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDeleteUser(this.SelectedPizza));
            this.AddCmd = new RelayCommand(() => this.logic.EditPizza(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditPizza(this.SelectedPizza, this.EditorFunc));
        }

        /// <summary>
        /// Gets or sets the currently selected pizza.
        /// </summary>
        public PizzaVM SelectedPizza
        {
            get { return this.selectedPizza; }
            set { this.Set(ref this.selectedPizza, value); }
        }

        /// <summary>
        /// Gets or sets all the stored pizzas.
        /// </summary>
        public ObservableCollection<PizzaVM> AllPizzas
        {
            get { return this.allPizzas; }
            set { this.Set(ref this.allPizzas, value); }
        }

        /// <summary>
        /// Gets the Add command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets the delete command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets the editor command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets the load command.
        /// </summary>
        public ICommand LoadCmd { get; private set; }

        /// <summary>
        /// Gets or sets the editor func.
        /// </summary>
        public Func<PizzaVM, bool> EditorFunc { get; set; }
    }
}
