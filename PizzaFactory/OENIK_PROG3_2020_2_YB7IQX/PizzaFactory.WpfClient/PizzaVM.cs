﻿// <copyright file="PizzaVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.WpfClient
{
    using System.ComponentModel;

    /// <summary>
    /// The Pizza class holds information regarding a specific pizza.
    /// </summary>
    public class PizzaVM : INotifyPropertyChanged
    {
        private int id;
        private string name;
        private int size;
        private int price;
        private float furtherExpenses;
        private float saleMultiplier;

        /// <summary>
        /// Event handler responsible for the proeprty changed event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets primary key of the person class, on which the other attributes depend.
        /// </summary>
        public int Id
        {
            get
            {
                return this.id;
            }

            set
            {
                this.id = value;
                this.OnPropertyChanged(nameof(this.id));
            }
        }

        /// <summary>
        /// Gets or sets name of the pizza which includes its number (not always the same as its Id field).
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
                this.OnPropertyChanged(nameof(this.name));
            }
        }

        /// <summary>
        /// Gets or sets size of the pizza which can either be 30 or 45.
        /// </summary>
        public int Size
        {
            get
            {
                return this.size;
            }

            set
            {
                this.size = value;
                this.OnPropertyChanged(nameof(this.size));
            }
        }

        /// <summary>
        /// Gets or sets price of the pizza which is calcualted by the sum of the ingredients it contains which sum is multiplied by the FurtherExpenses field.
        /// </summary>
        public int Price
        {
            get
            {
                return this.price;
            }

            set
            {
                this.price = value;
                this.OnPropertyChanged(nameof(this.price));
            }
        }

        /// <summary>
        /// Gets or sets price multiplier, it contains the cost of a pizza box, manpower wage, oven kWh, etc.
        /// </summary>
        public float FurtherExpenses
        {
            get
            {
                return this.furtherExpenses;
            }

            set
            {
                this.furtherExpenses = value;
                this.OnPropertyChanged(nameof(this.furtherExpenses));
            }
        }

        /// <summary>
        /// Gets or sets defines a multiplier that affects the price of the pizza, usually decreasing it since it is a sale.
        /// </summary>
        public float SaleMultiplier
        {
            get
            {
                return this.saleMultiplier;
            }

            set
            {
                this.saleMultiplier = value;
                this.OnPropertyChanged(nameof(this.saleMultiplier));
            }
        }

        /// <summary>
        /// Copies the values of another Pizza <see cref="PizzaVM"/> to this instance.
        /// </summary>
        /// <param name="other">Other PizzaModel from which we want to copy.</param>
        public void CopyFrom(PizzaVM other)
        {
            if (other == null)
            {
                return;
            }

            // Using proeprties so we get notified.
            this.Id = other.Id;
            this.Name = other.Name;
            this.Size = other.Size;
            this.Price = other.Price;
            this.FurtherExpenses = other.FurtherExpenses;
            this.SaleMultiplier = other.SaleMultiplier;
        }

        private void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
