﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.Net.Http;
    using System.Text.Json;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Logic defining the BL for the WPF Client.
    /// </summary>
    public class MainLogic
    {
        private string url = "https://localhost:44309/PizzaApi";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// Api to get the pizzas.
        /// </summary>
        /// <returns>Collection of pizzas.</returns>
        public Collection<PizzaVM> ApiGetPizzas()
        {
            string json = this.client.GetStringAsync(this.url + "/all").Result;
            var collection = JsonSerializer.Deserialize<Collection<PizzaVM>>(json, this.jsonOptions);

            return collection;
        }

        /// <summary>
        /// Api for deleting a pizza.
        /// </summary>
        /// <param name="pizza">PizzaVM instance.</param>
        public void ApiDeleteUser(PizzaVM pizza)
        {
            bool success = false;

            if (pizza != null)
            {
                string json = this.client.GetStringAsync(this.url + "/del/" + pizza.Id).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            SendMessage(success);
        }

        /// <summary>
        /// Edits a pizza.
        /// </summary>
        /// <param name="pizza">PizzaVM instance.</param>
        /// <param name="editorFunc">Func to call. Requireing lambda.</param>
        public void EditPizza(PizzaVM pizza, Func<PizzaVM, bool> editorFunc)
        {
            PizzaVM clone = new PizzaVM();

            if (pizza != null)
            {
                clone.CopyFrom(pizza);
            }

            bool? success = editorFunc?.Invoke(clone);

            if (success == true)
            {
                if (pizza != null)
                {
                    success = this.ApiEditUser(clone, true);
                }
                else
                {
                    success = this.ApiEditUser(clone, false);
                }
            }

            SendMessage(success == true);
        }

        private static void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "ApiResult");
        }

        /// <summary>
        /// Api for editing a pizza.
        /// </summary>
        /// <param name="pizza">PizzaVM instance.</param>
        /// <param name="isEditing">True if user is only edited.</param>
        /// <returns>Returns true if the edition was successful.</returns>
        private bool ApiEditUser(PizzaVM pizza, bool isEditing)
        {
            if (pizza == null)
            {
                return false;
            }

            string fullUrl = this.url + (isEditing ? "/mod" : "/add");

            Dictionary<string, string> postData = new Dictionary<string, string>();

            if (isEditing)
            {
                postData.Add(nameof(PizzaVM.Id), pizza.Id.ToString(new CultureInfo("en-US")));
            }

            postData = new Dictionary<string, string>();
            postData.Add(nameof(PizzaVM.Name), pizza.Name);
            postData.Add(nameof(PizzaVM.Size), pizza.Size.ToString(new CultureInfo("en-US")));
            postData.Add(nameof(PizzaVM.Price), pizza.Price.ToString(new CultureInfo("en-US")));
            postData.Add(nameof(PizzaVM.FurtherExpenses), pizza.FurtherExpenses.ToString(new CultureInfo("en-US")));
            postData.Add(nameof(PizzaVM.SaleMultiplier), pizza.SaleMultiplier.ToString(new CultureInfo("en-US")));

            string json = this.client.PostAsync(fullUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }
    }
}
