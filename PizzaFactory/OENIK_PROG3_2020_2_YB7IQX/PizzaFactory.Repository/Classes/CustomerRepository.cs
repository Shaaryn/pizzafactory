﻿// <copyright file="CustomerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Data.Models.Human;
    using PizzaFactory.Repository.Interfaces;

    /// <summary>
    /// Repository class of the Customer.
    /// </summary>
    public sealed class CustomerRepository : RepositoryBase<Customer>, ICustomerRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerRepository"/> class.
        /// </summary>
        /// <param name="givenDbContext">Refers to base constructor's DbContext.</param>
        public CustomerRepository(DbContext givenDbContext)
            : base(givenDbContext)
        {
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the customer entity we want to get.</param>
        /// <returns>Entity instance.</returns>
        public override Customer GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(c => c.Id == id);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the customer entity we want to get.</param>
        public void GiveRegisteredOn(int id)
        {
            var cus = this.GetOne(id);

            cus.RegisteredOn = DateTime.Now;

            this.ctx.SaveChanges();
        }
    }
}
