﻿// <copyright file="PizzeriaRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Repository.Interfaces;

    /// <summary>
    /// Repository class of pizzeria where the declared methods in the interface are implemented.
    /// </summary>
    public class PizzeriaRepository : RepositoryBase<Pizzeria>, IPizzeriaRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PizzeriaRepository"/> class.
        /// </summary>
        /// <param name="givenDbContext">Refers to base constructor's DbContext.</param>
        public PizzeriaRepository(DbContext givenDbContext)
            : base(givenDbContext)
        {
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the pizzeria entity we want to change.</param>
        /// <returns>Entity instance.</returns>
        public override Pizzeria GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(p => p.Id == id);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the pizzeria we want to change.</param>
        /// <param name="newName">String value of the name that overrides the previous one.</param>
        public void ChangeName(int id, string newName)
        {
            if (newName != null)
            {
                var pizzeria = this.GetOne(id);

                pizzeria.Name = newName;
                this.ctx.SaveChanges();
            }
            else
            {
                throw new ArgumentNullException(newName, "New name cannot be empty!");
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the pizzeria we want to change.</param>
        /// <param name="newOwner">Person object that represents the new owner. This object will override the previous owner.</param>
        public void ChangeOwner(int id, Person newOwner)
        {
            if (newOwner != null)
            {
                var pizzeria = this.GetOne(id);

                pizzeria.Person = newOwner;
                pizzeria.PersonId = newOwner.Id;
                this.ctx.SaveChanges();
            }
            else
            {
                throw new ArgumentNullException(nameof(newOwner), "New owner is set to null.");
            }
        }
    }
}
