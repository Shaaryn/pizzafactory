﻿// <copyright file="PizzaRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Repository.Exceptions;
    using PizzaFactory.Repository.Interfaces;

    /// <summary>
    /// Repository class of pizza where the declared methods in the interface are implemented.
    /// </summary>
    public class PizzaRepository : RepositoryBase<Pizza>, IPizzaRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PizzaRepository"/> class.
        /// </summary>
        /// <param name="givenDbContext">Refers to base constructor's DbContext.</param>
        public PizzaRepository(DbContext givenDbContext)
            : base(givenDbContext)
        {
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the pizza entity we want to change.</param>
        /// <returns>Entity instance.</returns>
        public override Pizza GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(p => p.Id == id);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the pizza we want to change.</param>
        /// <param name="newExpenses">Float value of the expenses modifier that overrides the previous one.</param>
        public void ChangeExpenses(int id, float newExpenses)
        {
            if (newExpenses != 0.0f)
            {
                var pizza = this.GetOne(id);

                pizza.FurtherExpenses = newExpenses;
                this.ctx.SaveChanges();
            }
            else
            {
                throw new UnderValueException($"Setting the expenses value to '{newExpenses}' would cause that the pizza's price is 0 !");
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the pizza we want to change.</param>
        /// <param name="newName">String value of the pizza's name that overrides the previous one.</param>
        public void ChangePizzaName(int id, string newName)
        {
            if (newName != null && newName.Length != 0)
            {
                var pizza = this.GetOne(id);

                pizza.Name = newName;
                this.ctx.SaveChanges();
            }
            else
            {
                throw new ArgumentNullException(newName, "New name of the pizza cannot be empty.");
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the pizza we want to change.</param>
        /// <param name="newPrice">Integer value of the pizza's price that overrides the previous one.</param>
        public void ChangePrice(int id, int newPrice)
        {
            if (newPrice != 0)
            {
                var pizza = this.GetOne(id);

                pizza.Price = newPrice;
                this.ctx.SaveChanges();
            }
            else
            {
                throw new UnderValueException($"Setting the price to '{newPrice}' would cause that the pizza's price is 0 !");
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the pizza we want to change.</param>
        /// <param name="newSale">Float value of the pizza's sale modifier that overrides the previous one.</param>
        public void ChangeSale(int id, float newSale)
        {
            if (newSale != 0.0f)
            {
                var pizza = this.GetOne(id);

                pizza.SaleMultiplier = newSale;
                this.ctx.SaveChanges();
            }
            else
            {
                throw new UnderValueException($"Setting the expenses value to '{newSale}' would cause that the pizza's price is 0 !");
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the pizza we want to apply the changes to.</param>
        /// <param name="newSize">Given size value that overrides the previous one.</param>
        public void ChangeSize(int id, string newSize)
        {
            if (newSize == "30" || newSize == "45")
            {
                var pizza = this.GetOne(id);

                pizza.Size = Convert.ToInt32(newSize);
                this.ctx.SaveChanges();
            }
            else
            {
                throw new UnderValueException($"Setting the size to any other value than '30' or '45' is forbidden!");
            }
        }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        /// <param name="id">Id of the pizza in the database that we want to replace.</param>
        /// <param name="givenPizza">Modified pizza.</param>
        public void ChangePizza(int id, Pizza givenPizza)
        {
            if (id != default && givenPizza != null)
            {
                var pizza = this.GetOne(id);

                pizza.Name = givenPizza.Name;
                pizza.Size = givenPizza.Size;
                pizza.Price = givenPizza.Price;
                pizza.FurtherExpenses = givenPizza.FurtherExpenses;
                pizza.SaleMultiplier = givenPizza.SaleMultiplier;
                this.ctx.SaveChanges();
            }
        }
    }
}
