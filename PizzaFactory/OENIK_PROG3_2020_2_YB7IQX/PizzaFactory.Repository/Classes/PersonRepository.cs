﻿// <copyright file="PersonRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using PizzaFactory.Data.Models;

    /// <summary>
    /// Repository class of person where the declared methods in the interface are implemented.
    /// </summary>
    public class PersonRepository : RepositoryBase<Person>, IPersonRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonRepository"/> class.
        /// </summary>
        /// <param name="givenDbContext">Refers to base constructor's DbContext.</param>
        public PersonRepository(DbContext givenDbContext)
            : base(givenDbContext)
        {
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the person entity we want to change.</param>
        /// <returns>Entity instance.</returns>
        public override Person GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(p => p.Id == id);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenPhoneNumber">Phone number of the person entity we search for.</param>
        /// <returns>Entity instance.</returns>
        public Person GetOneByPhone(string givenPhoneNumber)
        {
            return this.GetAll().SingleOrDefault(p => p.PhoneNumber == givenPhoneNumber);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the person entity we want to change.</param>
        /// <param name="newAddress">String value of the address that overrides the previous one.</param>
        public void ChangeAddress(int id, string newAddress)
        {
            if (newAddress != null)
            {
                var person = this.GetOne(id);

                person.Address = newAddress;
                this.ctx.SaveChanges();
            }
            else
            {
                throw new ArgumentNullException(newAddress, "New address can't be empty.");
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the person entity we want to change.</param>
        /// <param name="newFullName">String value of the name that overrides the previous one.</param>
        public void ChangeFullName(int id, string newFullName)
        {
            if (newFullName != null)
            {
                var person = this.GetOne(id);

                person.FullName = newFullName;
                this.ctx.SaveChanges();
            }
            else
            {
                throw new ArgumentNullException(newFullName, "New name can't be empty.");
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the person entity we want to change.</param>
        /// <param name="newPhoneNumber">String value of the phone number that overrides the previous one.</param>
        public void ChangePhoneNumber(int id, string newPhoneNumber)
        {
            if (newPhoneNumber != null)
            {
                var person = this.GetOne(id);

                person.PhoneNumber = newPhoneNumber;
                this.ctx.SaveChanges();
            }
            else
            {
                throw new ArgumentNullException(newPhoneNumber, "New phone number can't be empty.");
            }
        }
    }
}
