﻿// <copyright file="RepositoryBase.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using PizzaFactory.Data.Models;

    /// <summary>
    /// Repository for generic operations.
    /// </summary>
    /// <typeparam name="T">Any entity class.</typeparam>
    public abstract class RepositoryBase<T> : IRepositoryBase<T>
        where T : class
    {
        /// <summary>
        /// Generic DbContext to work with.
        /// </summary>
        protected DbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryBase{T}"/> class.
        /// </summary>
        /// <param name="givenDbContext">Any DbContext, preferably a <see cref="PizzaFactoryDbContext"/>.</param>
        public RepositoryBase(DbContext givenDbContext)
        {
            this.ctx = givenDbContext;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns>queryable instance collection.</returns>
        public IQueryable<T> GetAll()
        {
            return this.ctx.Set<T>();
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the entity.</param>
        /// <returns>Entity instance.</returns>
        public abstract T GetOne(int id);

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="newEntity">given entity of type T to be inserted, added.</param>
        public void Insert(T newEntity)
        {
            this.ctx.Set<T>().Add(newEntity);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenEntity">entity of type T we want to remove.</param>
        public void Remove(T givenEntity)
        {
            this.ctx.Set<T>().Remove(givenEntity);
            this.ctx.SaveChanges();
        }
    }
}
