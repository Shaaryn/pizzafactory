﻿// <copyright file="OrderRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Repository.Interfaces;

    /// <summary>
    /// Repository class of the Order.
    /// </summary>
    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderRepository"/> class.
        /// </summary>
        /// <param name="givenDbContext">Refers to base constructor's DbContext.</param>
        public OrderRepository(DbContext givenDbContext)
            : base(givenDbContext)
        {
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the order entity we want to get.</param>
        /// <returns>Entity instance.</returns>
        public override Order GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(o => o.Id == id);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the order we want to cancel.</param>
        public void Storno(int id)
        {
            this.GetOne(id).IsStorno = true;
            return;
        }
    }
}
