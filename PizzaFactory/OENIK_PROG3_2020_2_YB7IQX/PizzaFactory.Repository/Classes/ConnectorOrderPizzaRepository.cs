﻿// <copyright file="ConnectorOrderPizzaRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using PizzaFactory.Data.Models.Connector;
    using PizzaFactory.Repository.Interfaces;

    /// <summary>
    /// Repository class of the connector table (Order-Pizza).
    /// </summary>
    public class ConnectorOrderPizzaRepository : RepositoryBase<OrderPizzaConnector>, IConnectorOrderPizzaRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectorOrderPizzaRepository"/> class.
        /// </summary>
        /// <param name="givenDbContext">Refers to base constructor's DbContext.</param>
        public ConnectorOrderPizzaRepository(DbContext givenDbContext)
            : base(givenDbContext)
        {
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the connection entity we want to change.</param>
        /// <returns>Entity instance.</returns>
        public override OrderPizzaConnector GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(c => c.Id == id);
        }
    }
}
