﻿// <copyright file="ConnectorPizzaIngredientRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using PizzaFactory.Data.Models.Connector;
    using PizzaFactory.Repository.Interfaces;

    /// <summary>
    /// Repository class of the connector table (Ingredient-Pizza).
    /// </summary>
    public class ConnectorPizzaIngredientRepository : RepositoryBase<PizzaIngredientConnector>, IConnectorPizzaIngredientRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectorPizzaIngredientRepository"/> class.
        /// </summary>
        /// <param name="givenDbContext">Refers to the base constructor's DbContext.</param>
        public ConnectorPizzaIngredientRepository(DbContext givenDbContext)
            : base(givenDbContext)
        {
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the connection entity we want to change.</param>
        /// <returns>Entity instance.</returns>
        public override PizzaIngredientConnector GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(c => c.Id == id);
        }
    }
}