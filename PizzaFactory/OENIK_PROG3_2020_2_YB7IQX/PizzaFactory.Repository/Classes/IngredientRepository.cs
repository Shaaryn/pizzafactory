﻿// <copyright file="IngredientRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Internal;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Repository.Interfaces;

    /// <summary>
    /// Repository class of ingredient where the declared methods in the interface are implemented.
    /// </summary>
    public class IngredientRepository : RepositoryBase<Ingredient>, IIngredientRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IngredientRepository"/> class.
        /// </summary>
        /// <param name="givenDbContext">Refers to base constructor's DbContext.</param>
        public IngredientRepository(DbContext givenDbContext)
            : base(givenDbContext)
        {
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the entity.</param>
        /// <returns>Entity instance.</returns>
        public override Ingredient GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(i => i.Id == id);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the person entity we want to change.</param>
        /// <param name="newCost">Integer value of the cost that overrides the previous one.</param>
        public void ChangeCost(int id, int newCost)
        {
            var ingredient = this.GetOne(id);

            ingredient.Cost = newCost;
            this.ctx.SaveChanges();
        }
    }
}
