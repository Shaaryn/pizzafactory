﻿// <copyright file="EmployeeRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using PizzaFactory.Data.Models;
    using PizzaFactory.Data.Models.Human;
    using PizzaFactory.Repository.Exceptions;
    using PizzaFactory.Repository.Interfaces;

    /// <summary>
    /// Repository class of employee where the declared methods in the interface are implemented.
    /// </summary>
    public class EmployeeRepository : RepositoryBase<Employee>, IEmployeeRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeRepository"/> class.
        /// </summary>
        /// <param name="givenDbbontext">Refers to base constructor's DbContext.</param>
        public EmployeeRepository(DbContext givenDbbontext)
            : base(givenDbbontext)
        {
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the entity.</param>
        /// <returns>Entity instance.</returns>
        public override Employee GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(e => e.Id == id);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="fullName">Full name of the person we want to find.</param>
        /// <returns>Returns an <see cref="Employee"/> which contains all data of the user.</returns>
        public Employee GetOne(string fullName)
        {
            return this.GetAll().SingleOrDefault(e => e.Person.FullName == fullName);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the person we want to apply the changes to.</param>
        /// <param name="givenRole">Given role that overrides the previous one.</param>
        public void ChangeRole(int id, string givenRole)
        {
            var emp = this.GetOne(id);

            if (emp == null)
            {
                // Own exception here
            }
            else
            {
                switch (givenRole)
                {
                    case "admin":
                        emp.RoleOfEmployee = Role.Admin;
                        emp.RoleOfEmployeeId = (int)Role.Admin;
                        emp.AccessLevel = Authorization.Admin;
                        emp.AccessLevelValue = (int)Authorization.Admin;
                        break;
                    case "smanager":
                        emp.RoleOfEmployee = Role.Smanager;
                        emp.RoleOfEmployeeId = (int)Role.Smanager;
                        emp.AccessLevel = Authorization.Manager;
                        emp.AccessLevelValue = (int)Authorization.Manager;
                        break;
                    case "dispatcher":
                        emp.RoleOfEmployee = Role.Dispatcher;
                        emp.RoleOfEmployeeId = (int)Role.Dispatcher;
                        emp.AccessLevel = Authorization.User;
                        emp.AccessLevelValue = (int)Authorization.User;
                        break;
                    case "cook":
                        emp.RoleOfEmployee = Role.Cook;
                        emp.RoleOfEmployeeId = (int)Role.Cook;
                        emp.AccessLevel = Authorization.User;
                        emp.AccessLevelValue = (int)Authorization.User;
                        break;
                    case "assistant":
                        emp.RoleOfEmployee = Role.Assistant;
                        emp.RoleOfEmployeeId = (int)Role.Assistant;
                        emp.AccessLevel = Authorization.Assistant;
                        emp.AccessLevelValue = (int)Authorization.Assistant;
                        break;
                    case "rof":
                        emp.AccessLevel = Authorization.RoF;
                        emp.AccessLevelValue = (int)Authorization.RoF;
                        break;
                }

                this.ctx.SaveChanges();
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the person we want to apply the changes to.</param>
        /// <param name="terminationDay">Day of the contract termination.</param>>
        public void Resignation(int id, DateTime terminationDay)
        {
            var emp = this.GetOne(id);

            if (emp == null)
            {
                throw new EmployeeNotFoundException("Employe does not exists!");
            }
            else
            {
                if (emp.EndOfEmployment == null)
                {
                    emp.EndOfEmployment = terminationDay;
                }
                else
                {
                    throw new EmployeeAlreadyTerminatedException("Employee has been terminated already!");
                }
            }
        }
    }
}
