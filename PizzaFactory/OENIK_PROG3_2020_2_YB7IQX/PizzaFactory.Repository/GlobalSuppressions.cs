﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "Said to be acceptable now", Scope = "member", Target = "~F:PizzaFactory.Repository.Classes.RepositoryBase`1.ctx")]
[assembly: SuppressMessage("Design", "CA1012:Abstract types should not have public constructors", Justification = "Said to be acceptable now", Scope = "type", Target = "~T:PizzaFactory.Repository.Classes.RepositoryBase`1")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "Said to be acceptable now", Scope = "member", Target = "~F:PizzaFactory.Repository.Classes.RepositoryBase`1.ctx")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "I dont have the energy or time to research this topic.", Scope = "member", Target = "~M:PizzaFactory.Repository.Classes.PizzaRepository.ChangeSize(System.Int32,System.String)")]
