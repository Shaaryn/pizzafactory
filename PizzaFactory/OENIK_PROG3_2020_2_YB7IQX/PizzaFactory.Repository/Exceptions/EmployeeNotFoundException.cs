﻿// <copyright file="EmployeeNotFoundException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Exceptions
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Exception for cases when the employee is not found.
    /// </summary>
    public class EmployeeNotFoundException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeNotFoundException"/> class.
        /// Empty ctor.
        /// </summary>
        public EmployeeNotFoundException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeNotFoundException"/> class.
        /// Message only.
        /// </summary>
        /// <param name="message">String message of what could possibly raise the exception.</param>
        public EmployeeNotFoundException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeNotFoundException"/> class.
        /// Message with the option of stating the inner exception.
        /// </summary>
        /// <param name="message">String message of what could possibly raise the exception.</param>
        /// <param name="innerException">Inner exception that can filter the cause further.</param>
        public EmployeeNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
