﻿// <copyright file="UnderValueException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Exceptions
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Exception class for cases when the user tries to set the value of the pizza to 0.
    /// </summary>
    public class UnderValueException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnderValueException"/> class.
        /// Empty ctor.
        /// </summary>
        public UnderValueException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnderValueException"/> class.
        /// Message only.
        /// </summary>
        /// <param name="message">String message of what could possibly raise the exception.</param>
        public UnderValueException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnderValueException"/> class.
        /// Message with the option of stating the inner exception.
        /// </summary>
        /// <param name="message">String message of what could possibly raise the exception.</param>
        /// <param name="innerException">Inner exception that can filter the cause further.</param>
        public UnderValueException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
