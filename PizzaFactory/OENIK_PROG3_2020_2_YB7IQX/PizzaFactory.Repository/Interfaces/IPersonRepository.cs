﻿// <copyright file="IPersonRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PizzaFactory.Data.Models;

    /// <summary>
    /// Interface for the Person methods.
    /// </summary>
    public interface IPersonRepository : IRepositoryBase<Person>
    {
        /// <summary>
        /// One entity instance of the given value.
        /// </summary>
        /// <param name="givenPhoneNumber">Phone number of the person entity we search for.</param>
        /// <returns>Entity instance.</returns>
        Person GetOneByPhone(string givenPhoneNumber);

        /// <summary>
        /// Changes the full name of the person.
        /// </summary>
        /// <param name="id">Id of the person we want to apply the changes to.</param>
        /// <param name="newFullName">Given name that overrides the previous one.</param>
        void ChangeFullName(int id, string newFullName);

        /// <summary>
        /// Changes the address of the person.
        /// </summary>
        /// <param name="id">Id of the person we want to apply the changes to.</param>
        /// <param name="newAddress">Given address that overrides the previous one.</param>
        void ChangeAddress(int id, string newAddress);

        /// <summary>
        /// Changes the phone number of the person.
        /// </summary>
        /// <param name="id">Id of the person we want to apply the changes to.</param>
        /// <param name="newPhoneNumber">Given phone number that overrides the pervious one.</param>
        void ChangePhoneNumber(int id, string newPhoneNumber);
    }
}
