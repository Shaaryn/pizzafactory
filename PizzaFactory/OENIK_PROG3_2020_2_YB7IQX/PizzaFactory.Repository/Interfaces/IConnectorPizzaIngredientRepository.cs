﻿// <copyright file="IConnectorPizzaIngredientRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PizzaFactory.Data.Models.Connector;

    /// <summary>
    /// Interface for the cpi methods.
    /// </summary>
    public interface IConnectorPizzaIngredientRepository : IRepositoryBase<PizzaIngredientConnector>
    {
        // Nothing to specify.
    }
}
