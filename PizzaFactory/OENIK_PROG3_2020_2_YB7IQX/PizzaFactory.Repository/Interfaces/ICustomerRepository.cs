﻿// <copyright file="ICustomerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PizzaFactory.Data.Models.Human;

    /// <summary>
    /// Interface for the customer methods.
    /// </summary>
    public interface ICustomerRepository : IRepositoryBase<Customer>
    {
        /// <summary>
        /// Provides the registration day (which is the date of the first order) of the customer.
        /// </summary>
        /// <param name="id">Id of the customer we want to apply the changes to.</param>
        void GiveRegisteredOn(int id);
    }
}
