﻿// <copyright file="IRepositoryBase.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Base repository layer interface. Functions here should be available and implemented in every other repository.
    /// </summary>
    /// <typeparam name="T">Any entity type class.</typeparam>
    public interface IRepositoryBase<T>
    where T : class
    {
        /// <summary>
        /// One entity instance of the given value.
        /// </summary>
        /// <param name="id">Id of the entity.</param>
        /// <returns>Entity instance.</returns>
        T GetOne(int id);

        /// <summary>
        /// Returns a queryable collection of the entity instances.
        /// </summary>
        /// <returns>queryable instance collection.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Inserts a new entity into the specific class (table).
        /// </summary>
        /// <param name="newEntity">given entity of type T to be inserted, added.</param>
        void Insert(T newEntity);

        /// <summary>
        /// Removes the given entity from the database.
        /// </summary>
        /// <param name="givenEntity">entity of type T we want to remove.</param>
        void Remove(T givenEntity);
    }
}