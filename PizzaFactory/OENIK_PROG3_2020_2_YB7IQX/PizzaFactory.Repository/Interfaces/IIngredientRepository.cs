﻿// <copyright file="IIngredientRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PizzaFactory.Data.Models;

    /// <summary>
    /// Interface for the ingredient methods.
    /// </summary>
    public interface IIngredientRepository : IRepositoryBase<Ingredient>
    {
        /// <summary>
        /// Changes the price of the given ingredient.
        /// </summary>
        /// <param name="id">Id of the ingredient we want to change.</param>
        /// <param name="newCost">Integer value of the cost that overrides the previous one.</param>
        void ChangeCost(int id, int newCost);
    }
}
