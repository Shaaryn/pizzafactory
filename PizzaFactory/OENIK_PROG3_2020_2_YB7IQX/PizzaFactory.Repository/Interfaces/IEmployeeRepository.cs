﻿// <copyright file="IEmployeeRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PizzaFactory.Data.Models.Human;

    /// <summary>
    /// Interface for the Employee methods.
    /// </summary>
    public interface IEmployeeRepository : IRepositoryBase<Employee>
    {
        /// <summary>
        /// Gets the entity of the specific person. Based on its name.
        /// </summary>
        /// <param name="fullName">String value of the employee's full name.</param>
        /// <returns>Employee object that contains all data of the employee.</returns>
        Employee GetOne(string fullName);

        /// <summary>
        /// Changes the role of the employee and sets the new authorization level accordingly.
        /// </summary>
        /// <param name="id">Id of the person we want to apply the changes to.</param>
        /// <param name="givenRole">Given role that overrides the previous one.</param>
        void ChangeRole(int id, string givenRole);

        /// <summary>
        /// Sets the day of contract termination.
        /// </summary>
        /// <param name="id">Id of the person we want to apply the changes to.</param>
        /// <param name="terminationDay">Day of the contract termination.</param>>
        void Resignation(int id, DateTime terminationDay);
    }
}
