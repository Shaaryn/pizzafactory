﻿// <copyright file="IOrderRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PizzaFactory.Data.Models;

    /// <summary>
    /// Interfac ec for the order methods.
    /// </summary>
    public interface IOrderRepository : IRepositoryBase<Order>
    {
        /// <summary>
        /// Cancels an order.
        /// </summary>
        /// <param name="id">Id of the order we want to cancel.</param>
        void Storno(int id);
    }
}
