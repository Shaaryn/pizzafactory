﻿// <copyright file="IPizzeriaRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PizzaFactory.Data.Models;

    /// <summary>
    /// Interface for the pizzeria methods.
    /// </summary>
    public interface IPizzeriaRepository : IRepositoryBase<Pizzeria>
    {
        /// <summary>
        /// Changes the name of the pizzeria.
        /// </summary>
        /// <param name="id">Id of the pizza we want to apply the changes to.</param>
        /// <param name="newName">Given name that overrides the pervious one.</param>
        void ChangeName(int id, string newName);

        /// <summary>
        /// Changes the owner of the pizzeria. If the person given as a parameter not existing, this method will create it.
        /// </summary>
        /// <param name="id">Id of the pizza we want to apply the changes to.</param>
        /// <param name="newOwner">Given person who takes over the ownership of the pizzeria.</param>
        void ChangeOwner(int id, Person newOwner);
    }
}
