﻿// <copyright file="IConnectorOrderPizzaRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PizzaFactory.Data.Models.Connector;

    /// <summary>
    /// Interface for the cop methods.
    /// </summary>
    public interface IConnectorOrderPizzaRepository : IRepositoryBase<OrderPizzaConnector>
    {
        // Nothing to implement.
    }
}
