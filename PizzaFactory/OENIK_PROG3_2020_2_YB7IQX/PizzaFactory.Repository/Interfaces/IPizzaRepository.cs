﻿// <copyright file="IPizzaRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PizzaFactory.Data.Models;

    /// <summary>
    /// Interface for the pizza methods.
    /// </summary>
    public interface IPizzaRepository : IRepositoryBase<Pizza>
    {
        /// <summary>
        /// Changes the name of the pizza.
        /// </summary>
        /// <param name="id">Id of the pizza we want to apply the changes to.</param>
        /// <param name="newName">Given name that overrides the previous one.</param>
        void ChangePizzaName(int id, string newName);

        /// <summary>
        /// Changes the expenses modificator of the pizza.
        /// </summary>
        /// <param name="id">Id of the pizza we want to apply the changes to.</param>
        /// <param name="newExpenses">Given expenses multiplier that overrides the previous one.</param>
        void ChangeExpenses(int id, float newExpenses);

        /// <summary>
        /// Changes the price of the pizza.
        /// </summary>
        /// <param name="id">Id of the pizza we want to apply the changes to.</param>
        /// <param name="newPrice">Given price that overrides the previous one.</param>
        void ChangePrice(int id, int newPrice);

        /// <summary>
        /// Changes the sale modificator of the pizza.
        /// </summary>
        /// <param name="id">Id of the pizza we want to apply the changes to.</param>
        /// <param name="newSale">Given sale multiplier that overrides the previous one.</param>
        void ChangeSale(int id, float newSale);

        /// <summary>
        /// Changes the size of the pizza.
        /// </summary>
        /// <param name="id">Id of the pizza we want to apply the changes to.</param>
        /// <param name="newSize">Given size value that overrides the previous one.</param>
        void ChangeSize(int id, string newSize);

        /// <summary>
        /// Replaces the specified pizza with the new modified pizza.
        /// </summary>
        /// <param name="id">Id of the pizza in the database that we want to replace.</param>
        /// <param name="givenPizza">Modified pizza.</param>
        public void ChangePizza(int id, Pizza givenPizza);
    }
}
