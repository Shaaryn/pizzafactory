﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "I intend to use the setter function.", Scope = "member", Target = "~P:PizzaFactory.Data.Models.Human.Customer.Orders")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "I intend to use the setter function.", Scope = "member", Target = "~P:PizzaFactory.Data.Models.Order.Connector")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "I intend to use the setter function.", Scope = "member", Target = "~P:PizzaFactory.Data.Models.Pizza.ConnectorOrder")]
