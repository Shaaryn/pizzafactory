﻿// <copyright file="PizzaFactoryDbContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using Castle.DynamicProxy.Generators;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.VisualBasic;
    using PizzaFactory.Data.Models.Connector;
    using PizzaFactory.Data.Models.Human;

    /// <summary>
    /// Manages the database implementation of the models (represented as entities).
    /// </summary>
    public partial class PizzaFactoryDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PizzaFactoryDbContext"/> class.
        /// </summary>
        /// <param name="options">parameter.</param>
        public PizzaFactoryDbContext([NotNullAttribute] DbContextOptions options)
            : base(options)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PizzaFactoryDbContext"/> class.
        /// </summary>
        public PizzaFactoryDbContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets stores cpi entities.
        /// </summary>
        public virtual DbSet<PizzaIngredientConnector> Cpi { get; set; }

        /// <summary>
        /// Gets or sets stores cop entities.
        /// </summary>
        public virtual DbSet<OrderPizzaConnector> Cop { get; set; }

        /// <summary>
        /// Gets or sets stores person entities.
        /// </summary>
        public virtual DbSet<Person> Person { get; set; }

        /// <summary>
        /// Gets or sets stores employee entities.
        /// </summary>
        public virtual DbSet<Employee> Employee { get; set; }

        /// <summary>
        /// Gets or sets stores customer entities.
        /// </summary>
        public virtual DbSet<Customer> Customer { get; set; }

        /// <summary>
        /// Gets or sets stores ingredients entities.
        /// </summary>
        public virtual DbSet<Ingredient> Ingredients { get; set; }

        /// <summary>
        /// Gets or sets stores pizza entities.
        /// </summary>
        public virtual DbSet<Pizza> Pizzas { get; set; }

        /// <summary>
        /// Gets or sets stores order entities.
        /// </summary>
        public virtual DbSet<Order> Order { get; set; }

        /// <summary>
        /// Gets or sets stores pizzeria entities.
        /// </summary>
        public virtual DbSet<Pizzeria> Pizzeria { get; set; }

        /// <summary>
        /// Configures the database to be used for this context.
        /// </summary>
        /// <param name="optionsBuilder">API surface options for the builder.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null && !optionsBuilder.IsConfigured)
            {
                optionsBuilder.
                    UseLazyLoadingProxies().
                    UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;
                                   AttachDbFilename=|DataDirectory|\PizzaFactoryDatabase.mdf;
                                   Integrated Security=True;
                                   MultipleActiveResultSets=True");
                optionsBuilder.EnableSensitiveDataLogging();

                // Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzaFactoryDatabase.mdf;Integrated Security=True
            }
        }

        /// <summary>
        /// Configures the entities in the DbSet.
        /// </summary>
        /// <param name="modelBuilder">API surface options for the builder.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder != null)
            {
                // [Person - Employee] 1 Employee - 1 Person, 1 Person - 1 Employee (1-1)
                modelBuilder.Entity<Employee>()
                    .HasOne<Person>(e => e.Person)
                    .WithOne(p => p.Employee)
                    .HasForeignKey<Person>(p => p.EmployeeId);

                // [Person - Customer] 1 Custoemr - 1 Person, 1 Person - 1 Custoemr (1-1)
                modelBuilder.Entity<Customer>()
                    .HasOne<Person>(c => c.Person)
                    .WithOne(p => p.Customer)
                    .HasForeignKey<Person>(p => p.CustomerId);

                // [Ingredient - Pizza] 1 Ingredient - N Pizza, 1 Pizza - N Ingredient (N-M)
                modelBuilder.Entity<PizzaIngredientConnector>().HasKey(conn => new { conn.IngredientId, conn.PizzaId });

                modelBuilder.Entity<PizzaIngredientConnector>()
                    .HasOne<Pizza>(conn => conn.Pizza)
                    .WithMany(p => p.ConnectorIngredient)
                    .HasForeignKey(conn => conn.PizzaId);

                modelBuilder.Entity<PizzaIngredientConnector>()
                    .HasOne<Ingredient>(conn => conn.Ingredient)
                    .WithMany(i => i.Connector)
                    .HasForeignKey(conn => conn.IngredientId);

                // [Order - Pizza] 1 Order - N Pizza, 1 Pizza - N Order (N-M)
                modelBuilder.Entity<OrderPizzaConnector>().HasKey(conn => new { conn.OrderId, conn.PizzaId });

                modelBuilder.Entity<OrderPizzaConnector>()
                    .HasOne<Order>(conn => conn.Order)
                    .WithMany(o => o.Connector)
                    .HasForeignKey(conn => conn.OrderId);

                modelBuilder.Entity<OrderPizzaConnector>()
                    .HasOne<Pizza>(conn => conn.Pizza)
                    .WithMany(p => p.ConnectorOrder)
                    .HasForeignKey(conn => conn.PizzaId);

                // [Order - Employee] 1 Order - 1 Employee, 1 Employee - N Order (1-N)
                modelBuilder.Entity<Order>()
                    .HasOne<Employee>(o => o.Employee)
                    .WithMany(e => e.Orders)
                    .HasForeignKey(o => o.EmployeeId);

                // [Order - Customer] 1 Order - 1 Customer, 1 Custoemr - N Order (1-N)
                modelBuilder.Entity<Order>()
                    .HasOne<Customer>(o => o.Customer)
                    .WithMany(c => c.Orders)
                    .HasForeignKey(o => o.CustomerId);

                // [Order - Pizzeria] 1 Order - 1 Pizzeria, 1 Pizzeria - N Orders (1-N)
                modelBuilder.Entity<Order>()
                    .HasOne<Pizzeria>(o => o.Pizzeria)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(o => o.PizzeriaId);

                // Seed data -- [Pizzeria]
                Pizzeria theone = new Pizzeria
                {
                    Id = 1,
                    Name = "The One Pizza Factory",
                    PersonId = 1,
                    Address = "1055 Budapest, Kossuth Lajos tér 1-3.",
                    EstablishedDate = DateTime.Now,
                };

                // Seed data -- [Employee]
                Employee administratorEmp = new Employee
                {
                    Id = 1,
                    PersonId = 2,
                    RoleOfEmployee = Role.Admin,
                    RoleOfEmployeeId = (int)Role.Admin,
                    AccessLevel = Authorization.Admin,
                    AccessLevelValue = (int)Authorization.Admin,
                    StartOfEmployment = DateTime.Now,
                };
                Employee shiftManager = new Employee
                {
                    Id = 2,
                    PersonId = 4,
                    RoleOfEmployee = Role.Smanager,
                    RoleOfEmployeeId = (int)Role.Smanager,
                    AccessLevel = Authorization.Manager,
                    AccessLevelValue = (int)Authorization.Manager,
                    StartOfEmployment = DateTime.Now,
                };
                Employee cookEmp = new Employee
                {
                    Id = 3,
                    PersonId = 3,
                    RoleOfEmployee = Role.Cook,
                    RoleOfEmployeeId = (int)Role.Cook,
                    AccessLevel = Authorization.User,
                    AccessLevelValue = (int)Authorization.User,
                    StartOfEmployment = DateTime.Now,
                };

                // Seed data -- [Customer]
                Customer shiftManager1 = new Customer
                {
                    Id = 1,
                    PersonId = 4,
                    RegisteredOn = DateTime.Now,
                };
                Customer customer1 = new Customer
                {
                    Id = 2,
                    PersonId = 5,
                    RegisteredOn = DateTime.Now,
                };
                Customer customer2 = new Customer
                {
                    Id = 3,
                    PersonId = 6,
                    RegisteredOn = DateTime.Now,
                };
                Customer customer3 = new Customer
                {
                    Id = 4,
                    PersonId = 7,
                    RegisteredOn = DateTime.Now,
                };

                // Seed data -- [Person]
                Person owner = new Person { Id = 1, FullName = "Péter Árpád", Address = "1222 Budapest, Karéj utca 25.", PhoneNumber = "209862283" };
                Person administrator = new Person { Id = 2, FullName = "admin", Address = "2225 Üllő, Deák Ferenc utca 33/a", PhoneNumber = "123456789", EmployeeId = 1 };
                Person cook = new Person { Id = 3, FullName = "Bogdán Árpád", Address = "1234 Monor, Vas utca 35.", PhoneNumber = "308746998", EmployeeId = 3 };
                Person customeremployee = new Person { Id = 4, FullName = "Nagy Ervin", Address = "Középfölde 45.", PhoneNumber = "706945231", CustomerId = 1, EmployeeId = 2 };
                Person person1 = new Person { Id = 5, FullName = "Vigh Olívia", Address = "1135 Budapest, Közép út 5.", PhoneNumber = "703566871", CustomerId = 2 };
                Person person2 = new Person { Id = 6, FullName = "Zsíros B. Ödön", Address = "8563 Zsámbék, Károly körút 17.", PhoneNumber = "302253899", CustomerId = 3 };
                Person person3 = new Person { Id = 7, FullName = "Cserepes Virág", Address = "1213 Budapest, Vagyon köz 74/a.", PhoneNumber = "205194688", CustomerId = 4 };

                // Seed data -- [PizzaIngredient Connector]
                PizzaIngredientConnector conn1 = new PizzaIngredientConnector { Id = 1, PizzaId = 1, IngredientId = 1 };
                PizzaIngredientConnector conn2 = new PizzaIngredientConnector { Id = 2, PizzaId = 1, IngredientId = 3 };

                PizzaIngredientConnector conn3 = new PizzaIngredientConnector { Id = 3, PizzaId = 2, IngredientId = 2 };
                PizzaIngredientConnector conn4 = new PizzaIngredientConnector { Id = 4, PizzaId = 2, IngredientId = 3 };

                PizzaIngredientConnector conn5 = new PizzaIngredientConnector { Id = 5, PizzaId = 3, IngredientId = 1 };
                PizzaIngredientConnector conn6 = new PizzaIngredientConnector { Id = 6, PizzaId = 3, IngredientId = 2 };
                PizzaIngredientConnector conn7 = new PizzaIngredientConnector { Id = 7, PizzaId = 3, IngredientId = 3 };

                PizzaIngredientConnector conn8 = new PizzaIngredientConnector { Id = 8, PizzaId = 4, IngredientId = 3 };
                PizzaIngredientConnector conn9 = new PizzaIngredientConnector { Id = 9, PizzaId = 4, IngredientId = 6 };
                PizzaIngredientConnector conn10 = new PizzaIngredientConnector { Id = 10, PizzaId = 4, IngredientId = 4 };

                PizzaIngredientConnector conn11 = new PizzaIngredientConnector { Id = 11, PizzaId = 5, IngredientId = 7 };
                PizzaIngredientConnector conn12 = new PizzaIngredientConnector { Id = 12, PizzaId = 5, IngredientId = 6 };

                PizzaIngredientConnector conn13 = new PizzaIngredientConnector { Id = 13, PizzaId = 6, IngredientId = 6 };
                PizzaIngredientConnector conn14 = new PizzaIngredientConnector { Id = 14, PizzaId = 6, IngredientId = 5 };

                PizzaIngredientConnector conn15 = new PizzaIngredientConnector { Id = 15, PizzaId = 7, IngredientId = 4 };
                PizzaIngredientConnector conn16 = new PizzaIngredientConnector { Id = 16, PizzaId = 7, IngredientId = 1 };
                PizzaIngredientConnector conn17 = new PizzaIngredientConnector { Id = 17, PizzaId = 7, IngredientId = 7 };

                // Seed data -- [Ingredient]
                Ingredient ing1 = new Ingredient { Id = 1, Name = "cheese", Cost = 250 };
                Ingredient ing2 = new Ingredient { Id = 2, Name = "corn", Cost = 200 };
                Ingredient ing3 = new Ingredient { Id = 3, Name = "ham", Cost = 350 };
                Ingredient ing4 = new Ingredient { Id = 4, Name = "salami", Cost = 400 };
                Ingredient ing5 = new Ingredient { Id = 5, Name = "pineapple", Cost = 200 };
                Ingredient ing6 = new Ingredient { Id = 6, Name = "chili", Cost = 325 };
                Ingredient ing7 = new Ingredient { Id = 7, Name = "caviar", Cost = 700 };

                // Seed data -- [Pizza]
                Pizza pizza1 = new Pizza { Id = 1, Name = "1. Finom pizza", Size = 30, Price = 1450, SaleMultiplier = 0.9f, FurtherExpenses = 1.0f };
                Pizza pizza2 = new Pizza { Id = 2, Name = "2. Nagyszerű pizza", Size = 45, Price = 2250, SaleMultiplier = 1.0f, FurtherExpenses = 1.1f };
                Pizza pizza3 = new Pizza { Id = 3, Name = "3. Korrekt pizza", Size = 30, Price = 1780, SaleMultiplier = 1.0f, FurtherExpenses = 1.2f };
                Pizza pizza4 = new Pizza { Id = 4, Name = "4. Vírusölő pizza", Size = 30, Price = 2250, SaleMultiplier = 1.0f, FurtherExpenses = 1.0f };
                Pizza pizza5 = new Pizza { Id = 5, Name = "5. apa(férfi)+anya(nő) pizza", Size = 45, Price = 1950, SaleMultiplier = 0.9f, FurtherExpenses = 1.2f };
                Pizza pizza6 = new Pizza { Id = 6, Name = "6. wants some democracy pizza", Size = 30, Price = 1780, SaleMultiplier = 0.85f, FurtherExpenses = 1.1f };
                Pizza pizza7 = new Pizza { Id = 7, Name = "7. nemzeti focializmus pizza", Size = 30, Price = 1800, SaleMultiplier = 1.0f, FurtherExpenses = 1.0f };

                // Seed data -- [OrderPizza Conentor]
                OrderPizzaConnector opc1 = new OrderPizzaConnector { Id = 1, OrderId = 1, PizzaId = 3, Amount = 2, TotalPrice = 3560 };
                OrderPizzaConnector opc2 = new OrderPizzaConnector { Id = 2, OrderId = 1, PizzaId = 7, Amount = 1, TotalPrice = 1800 };

                OrderPizzaConnector opc3 = new OrderPizzaConnector { Id = 3, OrderId = 2, PizzaId = 2, Amount = 2, TotalPrice = 4500 };
                OrderPizzaConnector opc4 = new OrderPizzaConnector { Id = 4, OrderId = 2, PizzaId = 1, Amount = 1, TotalPrice = 1450 };
                OrderPizzaConnector opc5 = new OrderPizzaConnector { Id = 5, OrderId = 2, PizzaId = 5, Amount = 1, TotalPrice = 1950 };

                OrderPizzaConnector opc6 = new OrderPizzaConnector { Id = 6, OrderId = 3, PizzaId = 6, Amount = 1, TotalPrice = 1780 };

                OrderPizzaConnector opc7 = new OrderPizzaConnector { Id = 7, OrderId = 4, PizzaId = 4, Amount = 3, TotalPrice = 6750 };
                OrderPizzaConnector opc8 = new OrderPizzaConnector { Id = 8, OrderId = 4, PizzaId = 3, Amount = 2, TotalPrice = 3560 };

                OrderPizzaConnector opc9 = new OrderPizzaConnector { Id = 9, OrderId = 5, PizzaId = 1, Amount = 2, TotalPrice = 2900 };
                OrderPizzaConnector opc10 = new OrderPizzaConnector { Id = 10, OrderId = 5, PizzaId = 6, Amount = 2, TotalPrice = 3560 };

                OrderPizzaConnector opc11 = new OrderPizzaConnector { Id = 11, OrderId = 6, PizzaId = 2, Amount = 4, TotalPrice = 10000 };

                OrderPizzaConnector opc12 = new OrderPizzaConnector { Id = 12, OrderId = 7, PizzaId = 7, Amount = 3, TotalPrice = 5400 };

                OrderPizzaConnector opc13 = new OrderPizzaConnector { Id = 13, OrderId = 8, PizzaId = 3, Amount = 4, TotalPrice = 7120 };
                OrderPizzaConnector opc14 = new OrderPizzaConnector { Id = 14, OrderId = 8, PizzaId = 2, Amount = 1, TotalPrice = 2250 };
                OrderPizzaConnector opc15 = new OrderPizzaConnector { Id = 15, OrderId = 8, PizzaId = 5, Amount = 3, TotalPrice = 5850 };
                OrderPizzaConnector opc16 = new OrderPizzaConnector { Id = 16, OrderId = 8, PizzaId = 4, Amount = 6, TotalPrice = 13500 };

                // Seed data -- [Order]
                Order order1 = new Order { Id = 1, PizzeriaId = 1, EmployeeId = 1, CustomerId = customer1.Id, TimeStamp = DateTime.Now.AddDays(-97), IsStorno = false };
                Order order2 = new Order { Id = 2, PizzeriaId = 1, EmployeeId = 2, CustomerId = customer2.Id, TimeStamp = DateTime.Now.AddDays(-31), IsStorno = false };
                Order order3 = new Order { Id = 3, PizzeriaId = 1, EmployeeId = 3, CustomerId = customer3.Id, TimeStamp = DateTime.Now.AddDays(-11), IsStorno = false };
                Order order4 = new Order { Id = 4, PizzeriaId = 1, EmployeeId = 3, CustomerId = shiftManager1.Id, TimeStamp = DateTime.Now.AddDays(-7), Cost = 10310, IsStorno = false };
                Order order5 = new Order { Id = 5, PizzeriaId = 1, EmployeeId = 2, CustomerId = customer3.Id, TimeStamp = DateTime.Now.AddDays(-14), IsStorno = false };
                Order order6 = new Order { Id = 6, PizzeriaId = 1, EmployeeId = 3, CustomerId = customer2.Id, TimeStamp = DateTime.Now.AddDays(-29), IsStorno = false };
                Order order7 = new Order { Id = 7, PizzeriaId = 1, EmployeeId = 3, CustomerId = shiftManager1.Id, TimeStamp = DateTime.Now.AddDays(-9), Cost = 5400, IsStorno = false };
                Order order8 = new Order { Id = 8, PizzeriaId = 1, EmployeeId = 2, CustomerId = customer1.Id, TimeStamp = DateTime.Now.AddDays(-16), IsStorno = false };

                modelBuilder.Entity<Pizzeria>().HasData(theone);
                modelBuilder.Entity<Order>().HasData(order1, order2, order3, order4, order5, order6, order7, order8);
                modelBuilder.Entity<Employee>().HasData(administratorEmp, shiftManager, cookEmp);
                modelBuilder.Entity<Customer>().HasData(shiftManager1, customer1, customer2, customer3);
                modelBuilder.Entity<Person>().HasData(owner, administrator, cook, customeremployee, person1, person2, person3);
                modelBuilder.Entity<Ingredient>().HasData(ing1, ing2, ing3, ing4, ing5, ing6, ing7);
                modelBuilder.Entity<Pizza>().HasData(pizza1, pizza2, pizza3, pizza4, pizza5, pizza6, pizza7);
                modelBuilder.Entity<PizzaIngredientConnector>().HasData(conn1, conn2, conn3, conn4, conn5, conn6, conn7, conn8, conn9, conn10, conn11, conn12, conn13, conn14, conn15, conn16, conn17);
                modelBuilder.Entity<OrderPizzaConnector>().HasData(opc1, opc2, opc3, opc4, opc5, opc6, opc7, opc8, opc9, opc10, opc11, opc12, opc13, opc14, opc15, opc16);
            }
        }
    }
}
