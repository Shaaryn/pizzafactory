﻿// <copyright file="OrderPizzaConnector.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Data.Models.Connector
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Runtime.CompilerServices;
    using System.Text;

    /// <summary>
    /// Serves as a bridge between the Order and Pizza classes that share a many to many relationship.
    /// </summary>
    public partial class OrderPizzaConnector
    {
        /// <summary>
        /// Gets or sets primary key of the OrderPizzaConnector class, on which the other attributes depend.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets foreign key of the order part.
        /// </summary>
        [ForeignKey(nameof(Order))]
        public int OrderId { get; set; }

        /// <summary>
        /// Gets or sets navigational property of the order.
        /// </summary>
        [NotMapped]
        public virtual Order Order { get; set; }

        /// <summary>
        /// Gets or sets foreign key of the pizza part.
        /// </summary>
        [ForeignKey(nameof(Pizza))]
        public int PizzaId { get; set; }

        /// <summary>
        /// Gets or sets navigational property of pizza.
        /// </summary>
        [NotMapped]
        public virtual Pizza Pizza { get; set; }

        /// <summary>
        /// Gets or sets amount ordered of the specific pizza.
        /// </summary>
        [Required]
        public int Amount { get; set; }

        /// <summary>
        /// Gets or sets sum price of this specific item.
        /// </summary>
        [Required]
        public int TotalPrice { get; set; }
    }
}
