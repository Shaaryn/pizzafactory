﻿// <copyright file="PizzaIngredientConnector.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Data.Models.Connector
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Runtime.CompilerServices;
    using System.Text;

    /// <summary>
    /// Serves as a bridge between the Pizza and Ingredient classes that share a many to many relationship.
    /// </summary>
    public partial class PizzaIngredientConnector
    {
        /// <summary>
        /// Gets or sets primary key of the PizzaIngredientConenctor class on which the other attributes depend.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets foreign key of pizza.
        /// </summary>
        [ForeignKey(nameof(Pizza))]
        public int PizzaId { get; set; }

        /// <summary>
        /// Gets or sets navigational property of pizza.
        /// </summary>
        [NotMapped]
        public virtual Pizza Pizza { get; set; }

        /// <summary>
        /// Gets or sets foreign key of ingredient.
        /// </summary>
        [ForeignKey(nameof(Ingredient))]
        public int IngredientId { get; set; }

        /// <summary>
        /// Gets or sets navigational property of ingredient.
        /// </summary>
        [NotMapped]
        public virtual Ingredient Ingredient { get; set; }
    }
}
