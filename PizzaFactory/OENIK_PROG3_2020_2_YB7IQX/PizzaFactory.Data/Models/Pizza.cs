﻿// <copyright file="Pizza.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using PizzaFactory.Data.Models.Connector;

    /// <summary>
    /// The Pizza class holds information regarding a specific pizza.
    /// </summary>
    public class Pizza
    {
        /// <summary>
        /// Gets or sets primary key of the person class, on which the other attributes depend.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets name of the pizza which includes its number (not always the same as its Id field).
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets size of the pizza which can either be 30 or 45.
        /// </summary>
        [Required]
        public int Size { get; set; }

        /// <summary>
        /// Gets or sets price multiplier, it contains the cost of a pizza box, manpower wage, oven kWh, etc.
        /// </summary>
        [Required]
        public float FurtherExpenses { get; set; }

        /// <summary>
        /// Gets or sets price of the pizza which is calcualted by the sum of the ingredients it contains which sum is multiplied by the FurtherExpenses field.
        /// </summary>
        [Required]
        public int Price { get; set; }

        /// <summary>
        /// Gets or sets defines a multiplier that affects the price of the pizza, usually decreasing it since it is a sale.
        /// </summary>
        [Required]
        public float SaleMultiplier { get; set; }

        /// <summary>
        /// Gets navigational property.
        /// Collection of connections where this specific pizza is used.
        /// </summary>
        [NotMapped]
        public virtual ICollection<PizzaIngredientConnector> ConnectorIngredient { get; }

        /// <summary>
        /// Gets or sets navigational property.
        /// Collection of connections where this specific pizza is ordered.
        /// </summary>
        public virtual ICollection<OrderPizzaConnector> ConnectorOrder { get; set; }

        /// <summary>
        /// Concatonates the fields into a compact string that represents the instance.
        /// The ingredients are formatted separately to create a nice look for them.
        /// </summary>
        /// <returns>formatted string.</returns>
        public override string ToString()
        {
            // To achive : '( Ingred1, Ingred2, Ingred3 )'
            string allIngred = string.Empty;
            List<PizzaIngredientConnector> ingredientList = this.ConnectorIngredient.ToList();

            for (int i = 0; i < ingredientList.Count; i++)
            {
                if (i == 0)
                {
                    allIngred += $"( {ingredientList[i].Ingredient.Name}, ";
                }
                else if (i == ingredientList.Count - 1)
                {
                    allIngred += $"{ingredientList[i].Ingredient.Name} )";
                }
                else
                {
                    allIngred += $"{ingredientList[i].Ingredient.Name}, ";
                }
            }

            return $"*[Pizza[{this.Id}]]* The {this.Name} contains {allIngred} and it's costs {this.Price} ({this.Size}cm) ***";
        }

        /// <summary>
        /// Every field is represented by an integer value which values are summarzied and being returned.
        /// </summary>
        /// <returns>sum of integer values.</returns>
        public override int GetHashCode()
        {
            return this.Name.Length + this.Price + this.Size + Convert.ToInt32(this.SaleMultiplier) + this.ConnectorIngredient.Count + this.ConnectorOrder.Count;
        }

        /// <summary>
        /// We check if the Ingredients field's hash codes are the same or not.
        /// If they are the same we check the other properties.
        /// </summary>
        /// <param name="obj">Given object that we validate if it is a type of the expected class.</param>
        /// <returns>final boolean value of the operations.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Pizza && obj != null)
            {
                Pizza tmp = obj as Pizza;

                if (this.ConnectorIngredient.GetHashCode() != tmp.ConnectorIngredient.GetHashCode() &&
                    this.ConnectorOrder.GetHashCode() != tmp.ConnectorOrder.GetHashCode())
                {
                    return false;
                }

                if (this.Name == tmp.Name &&
                         this.Size == tmp.Size &&
                         this.Price == tmp.Price &&
                         this.ConnectorOrder.Equals(tmp.ConnectorOrder) &&
                         this.ConnectorIngredient.Equals(tmp.ConnectorIngredient))
                {
                    return true;
                }

                return false;
            }
            else
            {
                return false;
            }
        }
    }
}
