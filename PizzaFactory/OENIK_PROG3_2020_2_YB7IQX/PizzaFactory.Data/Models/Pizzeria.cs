﻿// <copyright file="Pizzeria.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Data.Models
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// This class repsresents each pizzeria in the franchise, yet only containing the crutial, needed data field. Who, where, when, under what name?.
    /// </summary>
    public class Pizzeria
    {
        /// <summary>
        /// Gets or sets priamry key of the pizzeria class, on which the other attributes depend.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets name of the pizzeria given by the owner.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets foreign key of a person, who is the owner.
        /// </summary>
        [Required]
        [MaxLength(50)]
        [Column("owner", TypeName = "int")]
        public int PersonId { get; set; }

        /// <summary>
        /// Gets or sets navigational property of the owner.
        /// </summary>
        [NotMapped]
        public virtual Person Person { get; set; }

        /// <summary>
        /// Gets or sets string representation of an address.
        /// There are no resident addresses ("tartozkodási cím") nor notification address ("értesítési cím"). A person may only have ONE address only.
        /// </summary>
        [MaxLength(200)]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets date of the first opening day of the pizzeria. (not the start of the construction).
        /// </summary>
        [Required]
        public DateTime EstablishedDate { get; set; }

        /// <summary>
        /// Gets navigational property of the orders.
        /// </summary>
        public virtual ICollection<Order> Orders { get; }

        /// <summary>
        /// Concatonates the fields into a compact string that represents the instance.
        /// </summary>
        /// <returns>formatted string.</returns>
        public override string ToString()
        {
            return $"*[Pizzeria[{this.Id}]]* The {this.Name} (Owner : {this.PersonId} is operating under the {this.Address} address since {this.EstablishedDate}. ***";
        }

        /// <summary>
        /// Every field is represented by an integer value which values are summarzied and being returned.
        /// </summary>
        /// <returns>sum of integer values.</returns>
        public override int GetHashCode()
        {
            return this.Name.Length + this.Person.GetHashCode() + this.Address.Length + (int)Math.Round(this.EstablishedDate.ToOADate());
        }

        /// <summary>
        /// We check if the Person object of the owner if it shares the same hash code or not.
        /// If they do and not retired we check for the other fields as well.
        /// </summary>
        /// <param name="obj">Given object that we validate if it is a type of the expected class.</param>
        /// <returns>final boolean value of the operations.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Pizzeria && obj != null)
            {
                Pizzeria tmp = obj as Pizzeria;

                if (!this.Person.Equals(tmp.Person))
                {
                    return false;
                }

                if (this.Name == tmp.Name &&
                    this.EstablishedDate == tmp.EstablishedDate &&
                    this.Address == tmp.Address)
                {
                    return true;
                }

                return false;
            }
            else
            {
                return false;
            }
        }
    }
}
