﻿// <copyright file="Ingredient.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Data.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using PizzaFactory.Data.Models.Connector;

    /// <summary>
    /// Contains the name and the cost of an ingredient.
    /// </summary>
    [Table("ingredient")]
    public class Ingredient
    {
        /// <summary>
        /// Gets or sets primary key of the ingredient class, on which the other attributes depend.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets name of the ingredient.
        /// </summary>
        [Required]
        [MaxLength(20)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets cost of the ingredient.
        /// </summary>
        [Required]
        public int Cost { get; set; }

        /// <summary>
        /// Gets navigational property.
        /// Collection of connections where this specific ingredient is used.
        /// </summary>
        public virtual ICollection<PizzaIngredientConnector> Connector { get; }

        /// <summary>
        /// Concatonates the fields into a compact string that represents the instance.
        /// </summary>
        /// <returns>formatted string.</returns>
        public override string ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Every field is represented by an integer value which values are summarzied and being returned.
        /// </summary>
        /// <returns>sum of integer values.</returns>
        public override int GetHashCode()
        {
            return this.Name.Length + this.Cost;
        }

        /// <summary>
        /// We check if the fields of the two instances are the same or not.
        /// </summary>
        /// <param name="obj">Given object that we validate if it is a type of the expected class.</param>
        /// <returns>final boolean value of the operations.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Ingredient && obj != null)
            {
                Ingredient tmp = obj as Ingredient;

                if (this.Name == tmp.Name &&
                    this.Cost == tmp.Cost)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
