﻿// <copyright file="Order.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using PizzaFactory.Data.Models.Connector;
    using PizzaFactory.Data.Models.Human;

    /// <summary>
    /// This class is capable of stroing orders along with the corresponding employee who palced it and the customer who ordered it.
    /// </summary>
    public class Order
    {
        /// <summary>
        /// Gets or sets primary key of the order class, on which the other attributes depend.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets time stamp of the order. When fully filled and placed.
        /// </summary>
        [Required]
        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// Gets or sets total cost of the order. Sum of Pizza * amount.
        /// </summary>
        [Required]
        public int Cost { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether defines whether the order was canceled (true) or not (false).
        /// </summary>
        public bool IsStorno { get; set; }

        /// <summary>
        /// Gets or sets foreign key of the employee who placed the order.
        /// </summary>
        [Required]
        [ForeignKey(nameof(Employee))]
        public int EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets navigational property of the employee.
        /// </summary>
        [NotMapped]
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Gets or sets customer who ordered.
        /// </summary>
        [Required]
        [ForeignKey(nameof(Customer))]
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets navigational property of the customer.
        /// </summary>
        [NotMapped]
        public virtual Customer Customer { get; set; }

        /// <summary>
        /// Gets or sets foreign key of the pizzeria which manages the order.
        /// </summary>
        [Required]
        [ForeignKey(nameof(Pizzeria))]
        public int PizzeriaId { get; set; }

        /// <summary>
        /// Gets or sets navigational property of the pizzeria.
        /// </summary>
        [NotMapped]
        public virtual Pizzeria Pizzeria { get; set; }

        /// <summary>
        /// Gets or sets navigational property.
        /// Collection of connections where this specific pizza is used.
        /// </summary>
        [NotMapped]
        public virtual ICollection<OrderPizzaConnector> Connector { get; set; }

        /// <summary>
        /// Concatonates the fields into a compact string that represents the instance.
        /// The pizzas are formatted separately to create a nice look for them.
        /// </summary>
        /// <returns>formatted string.</returns>
        public override string ToString()
        {
            // To achive : '( Pizza1, Pizza2, Pizza3 )'
            string allPizza = string.Empty;
            List<OrderPizzaConnector> pizzaList = this.Connector.ToList();

            for (int i = 0; i < pizzaList.Count; i++)
            {
                if (i == 0)
                {
                    allPizza += $"( {pizzaList[i].Pizza.Name}, ";
                }
                else if (i == pizzaList.Count - 1)
                {
                    allPizza += $"{pizzaList[i].Pizza.Name} )";
                }
                else
                {
                    allPizza += $"{pizzaList[i].Pizza.Name}, ";
                }
            }

            return $"[Order[{this.Id}]] | {this.TimeStamp} |\n{this.Employee.Person.FullName} placed an order on behalf of {this.Customer.Person.FullName} at {this.Pizzeria.Name}.\n" +
                   $"Containing the following items: {allPizza}";
        }

        /// <summary>
        /// Every field is represented by an integer value which values are summarzied and being returned.
        /// </summary>
        /// <returns>sum of integer values.</returns>
        public override int GetHashCode()
        {
            return (int)Math.Round(this.TimeStamp.ToOADate()) + this.PizzeriaId + this.Employee.Person.FullName.Length + this.Customer.Person.FullName.Length + this.Cost;
        }

        /// <summary>
        /// We check if the employees and customers field's hash codes are the same or not.
        /// If they are the same we check the other properties.
        /// </summary>
        /// <param name="obj">Given object that we validate if it is a type of the expected class.</param>
        /// <returns>final boolean value of the operations.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Order && obj != null)
            {
                Order tmp = obj as Order;

                if (this.Employee.GetHashCode() != tmp.GetHashCode() &&
                    this.Customer.GetHashCode() != tmp.GetHashCode())
                {
                    return false;
                }

                if (this.Employee.Equals(tmp.Employee) &&
                   this.Customer.Equals(tmp.Customer) &&
                   this.Pizzeria.Equals(tmp.Pizzeria) &&
                   this.Connector.Equals(tmp.Connector) &&
                   this.TimeStamp == tmp.TimeStamp &&
                   this.Cost == tmp.Cost)
                {
                    return true;
                }

                return false;
            }
            else
            {
                return false;
            }
        }
    }
}