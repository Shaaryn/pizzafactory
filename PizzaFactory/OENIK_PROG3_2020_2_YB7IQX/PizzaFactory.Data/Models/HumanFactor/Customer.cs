﻿// <copyright file="Customer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Data.Models.Human
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Customer is any individual who orders from a specific pizzeria.
    /// </summary>
    [Table("customer")]
    public class Customer
    {
        /// <summary>
        /// Gets or sets primary key of the customer class, on which the other attributes depend.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets foreign key of the person.
        /// </summary>
        [Required]
        [ForeignKey(nameof(Person))]
        public int PersonId { get; set; }

        /// <summary>
        /// Gets or sets navigational property of the person.
        /// </summary>
        [NotMapped]
        public virtual Person Person { get; set; }

        /// <summary>
        /// Gets or sets date when the customer first places an order.
        /// </summary>
        [Required]
        public DateTime RegisteredOn { get; set; }

        /// <summary>
        /// Gets or sets navigational property of the orders.
        /// </summary>
        public virtual ICollection<Order> Orders { get; set; }

        /// <summary>
        /// Concatonates the fields into a compact string that represents the instance.
        /// </summary>
        /// <returns>formatted string.</returns>
        public override string ToString()
        {
            return $"*[Customer[{this.Id}]]* {this.Person.FullName} (Pn: {this.Person.PhoneNumber}) first ordered on {this.RegisteredOn} ***";
        }

        /// <summary>
        /// Every field is represented by an integer value which values are summarzied and being returned.
        /// </summary>
        /// <returns>sum of integer values.</returns>
        public override int GetHashCode()
        {
            return this.Person.FullName.Length + (int)Math.Round(this.RegisteredOn.ToOADate());
        }

        /// <summary>
        /// We check if the Person object of the customers shares the same hash code or not.
        /// If they do we check for the other fields as well.
        /// </summary>
        /// <param name="obj">Given object that we validate if it is a type of the expected class.</param>
        /// <returns>final boolean value of the operations.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Customer && obj != null)
            {
                Customer tmp = obj as Customer;

                if (this.Person.GetHashCode() != tmp.GetHashCode())
                {
                    return false;
                }

                if (this.Person.Equals(tmp.Person) &&
                    this.RegisteredOn == tmp.RegisteredOn)
                {
                    return true;
                }

                return false;
            }
            else
            {
                return false;
            }
        }
    }
}
