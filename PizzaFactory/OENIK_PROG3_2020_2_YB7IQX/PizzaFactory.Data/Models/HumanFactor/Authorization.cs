﻿// <copyright file="Authorization.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Data.Models.Human
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// The Authorization enum determines the access level of an employee. Restrict which menu options can be used.
    /// Each level in the hierarchy inhreits the access of its subordinate level.
    /// </summary>
    public enum Authorization
    {
        /// <summary>
        /// Admins can use any menu options without any restrictions.
        /// Level : 0
        /// </summary>
        Admin,

        /// <summary>
        /// Managers can use any menu options except the User management and Settings options.
        /// Level : 1
        /// </summary>
        Manager,

        /// <summary>
        /// Users can see, create and manage new and already existing orders.
        /// Level : 2
        /// </summary>
        User,

        /// <summary>
        /// Assistants can only see the orders, but are unable to modify or create new ones.
        /// Level : 3
        /// </summary>
        Assistant,

        /// <summary>
        /// Retired or fired employees' field.
        /// Level : 4
        /// </summary>
        RoF,
    }
}
