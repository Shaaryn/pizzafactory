﻿// <copyright file="Role.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Data.Models.Human
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Every Employee has to have a role in which she or he works. Each role comes with different responsibilities, hence different access levels in the program.
    /// </summary>
    public enum Role
    {
        /// <summary>
        /// Operator of the Terminal. Hopefully a Programmer.
        /// Level : 0
        /// </summary>
        Admin,

        /// <summary>
        /// Shift manager responsible for organizing the shifts, handing out the shift schedules.
        /// Level : 1
        /// </summary>
        Smanager,

        /// <summary>
        /// A person who answeres the calls, places an order accordingly to the customer's needs and confirms online orders.
        /// Level : 2
        /// </summary>
        Dispatcher,

        /// <summary>
        /// The person who does the dough stretching and puts on the topping. Pays attention to the pizzas in the oven. Packs the finished pizzas in boxes.
        /// Level : 2
        /// </summary>
        Cook,

        /// <summary>
        /// Padawan who works under the cook's hands. Helps where she/he can, stretching, topping, oven-check, boxing.
        /// Level : 3
        /// </summary>
        Assistant,

        /// <summary>
        /// Retired or fired employees' field.
        /// Level : 4
        /// </summary>
        RoF,
    }
}
