﻿// <copyright file="Employee.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Data.Models.Human
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The Employee class represents a Person who is working for the company.
    /// </summary>
    [Table("employee")]
    public class Employee
    {
        /// <summary>
        /// Gets or sets primary key of the employee class, on which the other attributes depend.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets foreign key of the person.
        /// </summary>
        [Required]
        [ForeignKey(nameof(Person))]
        public int PersonId { get; set; }

        /// <summary>
        /// Gets or sets navigational property of the person.
        /// </summary>
        [NotMapped]
        public virtual Person Person { get; set; }

        /// <summary>
        /// Gets or sets it is a virtual field to store the id of the employee's role property.
        /// </summary>
        [Required]
        [Column("RoleId")]
        public virtual int RoleOfEmployeeId
        {
            get
            {
                return (int)this.RoleOfEmployee;
            }

            set
            {
                this.RoleOfEmployee = (Role)value;
            }
        }

        /// <summary>
        /// Gets or sets enum field that stores the role of the employee.
        /// </summary>
        [EnumDataType(typeof(Role))]
        [Column("Role")]
        public virtual Role RoleOfEmployee { get; set; }

        /// <summary>
        /// Gets or sets it is a virtual field to store the id of the employee's access level proeprty.
        /// </summary>
        [Required]
        [Column("AuthorizationId")]
        public virtual int AccessLevelValue
        {
            get
            {
                return (int)this.AccessLevel;
            }

            set
            {
                this.AccessLevel = (Authorization)value;
            }
        }

        /// <summary>
        /// Gets or sets enum field that stores the authorization level of the employee.
        /// </summary>
        [EnumDataType(typeof(Authorization))]
        [Column("Authorization")]
        public virtual Authorization AccessLevel { get; set; }

        /// <summary>
        /// Gets or sets starting date of the employee's employement. Date on the signed contract not the first workday.
        /// </summary>
        [Required]
        public DateTime StartOfEmployment { get; set; }

        /// <summary>
        /// Gets or sets end date of the employee's employement. Date of the last workday not the date of termiantion contract/resignation.
        /// </summary>
        public DateTime? EndOfEmployment { get; set; }

        /// <summary>
        /// Gets navigational property of the orders.
        /// </summary>
        public virtual ICollection<Order> Orders { get; }

        /// <summary>
        /// Concatonates the fields into a compact string that represents the instance.
        /// </summary>
        /// <returns>formatted string.</returns>
        public override string ToString()
        {
            if (this.EndOfEmployment == null)
            {
                return $"*[Emplyoee[{this.Id}]]* {this.Person.FullName} works as a {this.RoleOfEmployee}(Auth: {this.AccessLevel}) since {this.StartOfEmployment} ***";
            }
            else
            {
                return $"*[Emplyoee[{this.Id}]]* {this.Person.FullName} has retired since {this.EndOfEmployment} ***";
            }
        }

        /// <summary>
        /// Every field is represented by an integer value which values are summarzied and being returned.
        /// </summary>
        /// <returns>sum of integer values.</returns>
        public override int GetHashCode()
        {
            return this.Person.FullName.Length + (int)this.RoleOfEmployee + (int)this.AccessLevel +
                (int)Math.Round(this.StartOfEmployment.ToOADate());
        }

        /// <summary>
        /// We check if the Person object of the employees shares the same hash code or not.
        /// If they do we check for the other fields as well.
        /// </summary>
        /// <param name="obj">Given object that we validate if it is a type of the expected class.</param>
        /// <returns>final boolean value of the operations.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Employee && obj != null)
            {
                Employee tmp = obj as Employee;

                if (this.Person.GetHashCode() != tmp.Person.GetHashCode())
                {
                    return false;
                }

                if (this.Person.Equals(tmp.Person) &&
                    this.StartOfEmployment == tmp.StartOfEmployment &&
                    this.EndOfEmployment == tmp.EndOfEmployment &&
                    this.RoleOfEmployee == tmp.RoleOfEmployee)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
