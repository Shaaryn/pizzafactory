﻿// <copyright file="Person.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.Data.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using PizzaFactory.Data.Models.Human;

    /// <summary>
    /// The Person class reresents the human factor of the program.
    /// The identity of a customer, chef, dispatcher, assistant, manager or admin will be represented as a Person object.
    /// Regarding the names I would like to follow the Hungarian syntax, first the Surname and then the first name, like 'Példa Béla'.
    /// </summary>
    [Table("person")]
    public class Person
    {
        /// <summary>
        /// Gets or sets primary key of the person class, on which the other attributes depend.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets full name of the person.
        /// </summary>
        [MaxLength(100)]
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets string representation of an address.
        /// There are no resident addresses ("tartozkodási cím") nor notification address ("értesítési cím"). A person may only have ONE address only.
        /// </summary>
        [MaxLength(200)]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets as other things, this will follow the Hungarian standards. No German 12 digit number or African +291:011 nonsenses.
        /// The following syntax shall be followed: 201234567 or 301234567 etc.
        /// No need to append the +36 since it is common in every phone number.
        /// </summary>
        [Required]
        [MaxLength(9)]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets foreign key of employee.
        /// </summary>
        [ForeignKey(nameof(Employee))]
        public int? EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets navigational property of the employee.
        /// </summary>
        [NotMapped]
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Gets or sets foreign key of customer.
        /// </summary>
        [ForeignKey(nameof(Customer))]
        public int? CustomerId { get; set; }

        /// <summary>
        /// Gets or sets navigational property of the customer.
        /// </summary>
        [NotMapped]
        public virtual Customer Customer { get; set; }

        /// <summary>
        /// Concatonates the fields into a compact string that represents the instance.
        /// </summary>
        /// <returns>formatted string.</returns>
        public override string ToString()
        {
            return $"*[Person[{this.Id}]]* {this.FullName} (Pn: {this.PhoneNumber} )lives at " + this.Address.ToString() + " ***";
        }

        /// <summary>
        /// We check if the fields of the two instances are the same or not.
        /// </summary>
        /// <param name="obj">Given object that we validate if it is a type of the expected class.</param>
        /// <returns>final boolean value of the operations.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Person && obj != null)
            {
                Person tmp = obj as Person;

                if (this.FullName == tmp.FullName &&
                        this.PhoneNumber == tmp.PhoneNumber &&
                        this.Address == tmp.Address)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Every field is represented by an integer value which values are summarzied and being returned.
        /// </summary>
        /// <returns>sum of integer values.</returns>
        public override int GetHashCode()
        {
            // Sum up the numbers one by one of the phone number.
            int sumPhone = 0;
            for (int i = 0; i < this.PhoneNumber.Length; i++)
            {
                sumPhone += Convert.ToInt32(this.PhoneNumber[i]);
            }

            return this.FullName.Length + this.Address.Length + sumPhone;
        }
    }
}
