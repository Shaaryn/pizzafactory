﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "Im used to have ctor after fields and properties. I think its more readable.")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1629:Documentation text should end with a period", Justification = "It does end with a period :)")]
[assembly: SuppressMessage("Performance", "CA1812:Avoid uninstantiated internal classes", Justification = "Its not done yet so I dont know, but 100 of this warning is annoying.")]

[assembly: SuppressMessage("Design", "CA1014:Mark assemblies with CLSCompliant", Justification = " https://i.kym-cdn.com/photos/images/original/001/535/068/29d.jpg ")]

[assembly: SuppressMessage(
    "Design",
    "NU1701:Package 'MvvmLightLibs 5.4.1.1' was restored using '.NETFramework,Version=v4.6.1, .NETFramework,Version=v4.6.2, .NETFramework,Version=v4.7, .NETFramework,Version=v4.7.1, .NETFramework,Version=v4.7.2, .NETFramework,Version=v4.8' instead of the project target framework 'net5.0-windows7.0'. This package may not be fully compatible with your project.",
    Justification = "Im glad as hell, truly I am but pls stfu.")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "Didn't see the point of it.", Scope = "type", Target = "~T:PizzaFactory.DesktopApplication.BusinessLogic.PizzaLogic")]
