﻿// <copyright file="IModifyService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.DesktopApplication.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PizzaFactory.DesktopApplication.Model;

    /// <summary>
    /// Interface defining the necessary step of a Modification operation.
    /// </summary>
    public interface IModifyService
    {
        /// <summary>
        /// Evaluates the successfulness of the modification operation.
        /// </summary>
        /// <param name="pizzaToModify">Selected PizzaModel from the collection to be modified.</param>
        /// <returns>True if the operation and False otherwise.</returns>
        bool ModifyPizza(PizzaModel pizzaToModify);
    }
}
