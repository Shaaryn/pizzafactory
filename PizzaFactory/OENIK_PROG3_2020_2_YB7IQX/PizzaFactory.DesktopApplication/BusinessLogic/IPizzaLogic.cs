﻿// <copyright file="IPizzaLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.DesktopApplication.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PizzaFactory.DesktopApplication.Model;

    /// <summary>
    /// Defines the WPF business logic.
    /// </summary>
    internal interface IPizzaLogic
    {
        /// <summary>
        /// Appends a new PizzaModel <see cref="PizzaModel"/> to the end of the collection.
        /// Calls the <see cref="Logic"/> Logic layer afterwards.
        /// </summary>
        /// <param name="collection">Collection of PizzaModel being used by the UI.</param>
        void AddPizza(IList<PizzaModel> collection);

        /// <summary>
        /// Modifies the currently selected PizzaModel <see cref="PizzaModel"/>.
        /// Calls the Logic <see cref="Logic"/> layer afterwards.
        /// </summary>
        /// <param name="pizzaToModify">Selected PizzaModel from the list.</param>
        void ModifyPizza(PizzaModel pizzaToModify);

        /// <summary>
        /// Removes the currently selected PizzaModel <see cref="PizzaModel"/> from the collection.
        /// Calls the Logic <see cref="Logic"/> layer afterwards.
        /// </summary>
        /// <param name="collection">Collection of PizzaModel being used by the UI</param>
        /// <param name="pizzaToRemove">Selected PizzaModel from the lsit.</param>
        void RemovePizza(IList<PizzaModel> collection, PizzaModel pizzaToRemove);
    }
}
