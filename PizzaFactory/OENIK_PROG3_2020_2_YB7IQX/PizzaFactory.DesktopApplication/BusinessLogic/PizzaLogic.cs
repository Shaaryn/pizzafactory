﻿// <copyright file="PizzaLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.DesktopApplication.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using GalaSoft.MvvmLight.Messaging;
    using PizzaFactory.Data.Models;
    using PizzaFactory.DesktopApplication.Model;
    using PizzaFactory.DesktopApplication.Model.Conversion;
    using PizzaFactory.Logic.Classes;
    using PizzaFactory.Logic.Interfaces;
    using PizzaFactory.Repository.Classes;

    /// <summary>
    /// Implements the necessary business logic functions defined in its interface <see cref="IPizzaLogic"/>.
    /// </summary>
    internal class PizzaLogic : IPizzaLogic
    {
        private IModifyService editorService;
        private IMessenger messengerService;    // Aspect
        private PizzaMapper mapperService;

        private PizzaManagement logic;
        private PizzaFactoryDbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="PizzaLogic"/> class.
        /// Constructor responsible for the dependency injection of the necessary interfaces.
        /// </summary>
        /// <param name="editorService">Service used for the modification operation.</param>
        /// <param name="messengerService">Service used for the feedback communication.</param>
        public PizzaLogic(IModifyService editorService, IMessenger messengerService)
        {
            this.ctx = new PizzaFactoryDbContext();
            PizzaRepository repo = new PizzaRepository(this.ctx);
            this.editorService = editorService;
            this.messengerService = messengerService;
            this.mapperService = new PizzaMapper();
            this.logic = new PizzaManagement(repo, null, null, null);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="collection">Collection of PizzaModel being used by the UI.</param>
        public void AddPizza(IList<PizzaModel> collection)
        {
            PizzaModel newPizza = new PizzaModel();
            if (this.editorService.ModifyPizza(newPizza))
            {
                collection.Add(newPizza);

                this.logic.NewPizza = new Pizza();  // Otherwise keeps the instance data and pre-sets the id which would cause an inenrException as Identity_key is OFF.
                this.logic.NewPizza.Name = newPizza.Name;
                this.logic.NewPizza.Size = newPizza.Size;
                this.logic.NewPizza.Price = newPizza.Price;
                this.logic.NewPizza.FurtherExpenses = newPizza.FurtherExpenses;
                this.logic.NewPizza.SaleMultiplier = newPizza.SaleMultiplier;
                this.logic.SubmitNewPizza();

                collection[collection.Count - 1].Id = this.logic.NewPizza.Id;

                this.messengerService.Send("Addition successfull.", "Logic.Add result.");
            }
            else
            {
                this.messengerService.Send("Addition failed.", "Logic.Add result.");
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="pizzaToModify">Selected PizzaModel from the list.</param>
        public void ModifyPizza(PizzaModel pizzaToModify)
        {
            if (pizzaToModify == null)
            {
                this.messengerService.Send("Modify failed.", "Logic.Modify result.");
                return;
            }
            else
            {
                PizzaModel tmpClone = new PizzaModel();
                tmpClone.CopyFrom(pizzaToModify);

                if (this.editorService.ModifyPizza(tmpClone))
                {
                    pizzaToModify.CopyFrom(tmpClone);

                    Pizza convertedPizza = this.mapperService.Mapper.Map<Model.PizzaModel, Data.Models.Pizza>(pizzaToModify);
                    this.logic.ModifyPizza(convertedPizza, "name", tmpClone.Name);

                    this.messengerService.Send("Modify successfull.", "Logic.Modify result.");
                }
                else
                {
                    this.messengerService.Send("Modify failed.", "Logic.Modify result.");
                }
            }

            // Logic !!!
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="collection">Collection of PizzaModel being used by the UI</param>
        /// <param name="pizzaToRemove">Selected PizzaModel from the lsit.</param>
        public void RemovePizza(IList<PizzaModel> collection, PizzaModel pizzaToRemove)
        {
            if (pizzaToRemove == null || !collection.Remove(pizzaToRemove))
            {
                this.messengerService.Send("Remove failed.", "Logic.Remove result.");
            }
            else
            {
                Pizza convertedPizza = this.mapperService.Mapper.Map<Model.PizzaModel, Data.Models.Pizza>(pizzaToRemove);
                this.logic.RemovePizza(this.logic.GetOnePizza(convertedPizza.Id));

                this.messengerService.Send("Remove successfull.", "Logic.Remvoe result.");
            }

            // Logic !!!
        }
    }
}
