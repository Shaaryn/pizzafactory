﻿// <copyright file="PizzaMapper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.DesktopApplication.Model.Conversion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AutoMapper;
    using PizzaFactory.Data.Models;

    /// <summary>
    /// Mapper aspect.
    /// Responsible for mapping the Windows Application's model to its corresponding Data layer entity.
    /// </summary>
    internal class PizzaMapper : IPizzaMapper
    {
        /// <summary>
        /// Gets the mapper property that holds the necessary information for conversion.
        /// </summary>
        public IMapper Mapper { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PizzaMapper"/> class.
        /// Constructor of the Mapper class which sets the mapping configuration.
        /// </summary>
        public PizzaMapper()
        {
            this.Mapper = this.EstablishMapping();
        }

        /// <summary>
        /// Actually confugres the mapper as we want it to work.
        /// </summary>
        /// <returns>Returns the configured mapper.</returns>
        public IMapper EstablishMapping()
        {
            // NOT containing .Ingredients as it is not a realy proeprty, it should be a connector table.
            var config = new MapperConfiguration(conf =>
            {
                conf.CreateMap<Data.Models.Pizza, PizzaModel>().
                     ForMember(dest => dest.Id, from => from.MapFrom(src => src.Id)).
                     ForMember(dest => dest.Name, from => from.MapFrom(src => src.Name)).
                     ForMember(dest => dest.Price, from => from.MapFrom(src => src.Price)).
                     ForMember(dest => dest.FurtherExpenses, from => from.MapFrom(src => src.FurtherExpenses)).
                     ForMember(dest => dest.SaleMultiplier, from => from.MapFrom(src => src.SaleMultiplier));
            });

            return config.CreateMapper();
        }
    }
}
