﻿// <copyright file="IPizzaMapper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.DesktopApplication.Model.Conversion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// The PizzaMapper interface adumbrates that its inheritance will force a class to be responsible for the Data -- Model conversion.
    /// </summary>
    internal interface IPizzaMapper
    {
        /// <summary>
        /// Configures the Mapper (AutoMapper NuGet <see cref="AutoMapper"/>) according to the conversion.
        /// </summary>
        /// <returns>Returns the configured mapper.</returns>
        IMapper EstablishMapping();
    }
}
