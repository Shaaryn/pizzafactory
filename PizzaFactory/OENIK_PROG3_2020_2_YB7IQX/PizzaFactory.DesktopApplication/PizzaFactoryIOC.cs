﻿// <copyright file="PizzaFactoryIOC.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.DesktopApplication
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;

    /// <summary>
    /// IOC class.
    /// </summary>
    public class PizzaFactoryIOC : SimpleIoc, IServiceLocator
    {
        /// <summary>
        /// Gets the ugly PS instance for the IOC.
        /// </summary>
        public static PizzaFactoryIOC Instance { get; private set; } = new PizzaFactoryIOC();
    }
}
