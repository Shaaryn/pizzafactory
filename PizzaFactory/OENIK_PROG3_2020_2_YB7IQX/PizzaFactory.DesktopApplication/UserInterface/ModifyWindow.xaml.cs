﻿// <copyright file="ModifyWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.DesktopApplication.UserInterface
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using PizzaFactory.DesktopApplication.Model;
    using PizzaFactory.DesktopApplication.ViewModel;

    /// <summary>
    /// Interaction logic for ModifyWindows.xaml.
    /// </summary>
    public partial class ModifyWindow : Window
    {
        private ModifyViewModel modifyVM;

        /// <summary>
        /// Gets the property of the currently used PizzaModel.
        /// </summary>
        public PizzaModel Pizza => this.modifyVM.Pizza;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModifyWindow"/> class.
        /// Defines the ViewModel.
        /// </summary>
        public ModifyWindow()
        {
            this.InitializeComponent();

            this.modifyVM = this.FindResource("ModifyVM") as ModifyViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModifyWindow"/> class.
        /// Defines the ViewModel AND the PizzaModel to be used.
        /// </summary>
        /// <param name="pizzaToModify">Selected PizzaModel from the colelction.</param>
        public ModifyWindow(PizzaModel pizzaToModify)
            : this()
        {
            this.modifyVM.Pizza = pizzaToModify;
        }

        private void ContinueClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
