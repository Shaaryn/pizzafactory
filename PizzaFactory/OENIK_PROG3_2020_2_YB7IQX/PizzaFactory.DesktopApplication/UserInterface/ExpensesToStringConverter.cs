﻿// <copyright file="ExpensesToStringConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.DesktopApplication.UserInterface
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;

    /// <summary>
    /// Responsible for the value conversion between the UI <see cref="UserInterface"/> and the Model <see cref="Model"/>.
    /// </summary>
    internal class ExpensesToStringConverter : IValueConverter
    {
        /// <summary>
        /// Converts the given value to its UI representation.
        /// </summary>
        /// <param name="value">Value given by the UI.</param>
        /// <param name="targetType">Not used1.</param>
        /// <param name="parameter">Not used2.</param>
        /// <param name="culture">Not used3.</param>
        /// <returns>UI representation of the given value.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is float)
            {
                float percentage = ((float)value * 100) - 100;
                int result = System.Convert.ToInt32(percentage);
                return result + "%";
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Converts the given value to its Model representation.
        /// </summary>
        /// <param name="value">Value given by the Model.</param>
        /// <param name="targetType">Not used1.</param>
        /// <param name="parameter">Not used2.</param>
        /// <param name="culture">Not used3.</param>
        /// <returns>Model representation of the given value.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string)
            {
                string tmp = (string)value;

                // One element array :/
                string[] resultArray = tmp.Split("%");

                return resultArray[0];
            }
            else
            {
                return null;
            }
        }
    }
}
