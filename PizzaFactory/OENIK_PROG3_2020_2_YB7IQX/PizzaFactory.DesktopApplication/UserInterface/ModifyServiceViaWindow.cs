﻿// <copyright file="ModifyServiceViaWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.DesktopApplication.UserInterface
{
    using PizzaFactory.DesktopApplication.BusinessLogic;
    using PizzaFactory.DesktopApplication.Model;

    /// <summary>
    /// Implements the IModifyService.
    /// </summary>
    internal class ModifyServiceViaWindow : IModifyService
    {
        /// <inheritdoc/>
        public bool ModifyPizza(PizzaModel pizzaToModify)
        {
            ModifyWindow window = new ModifyWindow(pizzaToModify);
            return window.ShowDialog() ?? false;
        }
    }
}
