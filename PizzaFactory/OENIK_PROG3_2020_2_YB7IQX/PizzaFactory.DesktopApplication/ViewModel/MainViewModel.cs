﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.DesktopApplication.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using PizzaFactory.DesktopApplication.BusinessLogic;
    using PizzaFactory.DesktopApplication.Model;

    /// <summary>
    /// ViewModel responsible for the UI flow.
    /// </summary>
    internal class MainViewModel : ViewModelBase
    {
        private IPizzaLogic logic;
        private PizzaModel selectedPizza;

        /// <summary>
        /// Gets or sets PizzaModel <see cref="PizzaModel"/> selected in the UI.
        /// </summary>
        public PizzaModel SelectedPizza
        {
            get { return this.selectedPizza; }
            set => this.Set(ref this.selectedPizza, value);
        }

        /// <summary>
        /// Gets the collection of PizzaModel <see cref="PizzaModel"/> which populates the <ListBox/>.
        /// </summary>
        public ObservableCollection<PizzaModel> Menu { get; private set; }

        /// <summary>
        /// Gets the Command responsible for adding a PizzaModel <see cref="PizzaModel"/>.
        /// </summary>
        public ICommand AddCommand { get; private set; }

        /// <summary>
        /// Gets the Command responsible for modifying a PizzaModel <see cref="PizzaModel"/>.
        /// </summary>
        public ICommand ModifyCommand { get; private set; }

        /// <summary>
        /// Gets the Command responsible for removing a PizzaModel <see cref="PizzaModel"/>.
        /// </summary>
        public ICommand RemoveCommand { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// Dependency Injection of the logic.
        /// </summary>
        /// <param name="logic">Used logic to manipulate the data.</param>
        public MainViewModel(IPizzaLogic logic)
        {
            this.logic = logic;

            this.Menu = new ObservableCollection<PizzaModel>();  // logic.GetAllPizzas() !

            if (this.IsInDesignMode)
            {
                PizzaModel design1 = new PizzaModel() { Id = 1, Name = "Design 1 Pizza", Size = 32, Price = 3500, FurtherExpenses = 1.0f, SaleMultiplier = 0.5f };
                PizzaModel design2 = new PizzaModel() { Id = 2, Name = "Design 2 Pizza", Size = 45, Price = 4250, FurtherExpenses = 1.2f, SaleMultiplier = 0.8f };
                this.Menu.Add(design1);
                this.Menu.Add(design2);
            }

            this.AddCommand = new RelayCommand(() => this.logic.AddPizza(this.Menu));
            this.ModifyCommand = new RelayCommand(() => this.logic.ModifyPizza(this.SelectedPizza));
            this.RemoveCommand = new RelayCommand(() => this.logic.RemovePizza(this.Menu, this.SelectedPizza));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// Dependency Injection of the logic AND provides the necessary service WHEN the program is running.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IPizzaLogic>())
        {
        }
    }
}
