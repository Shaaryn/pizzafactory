﻿// <copyright file="ModifyViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.DesktopApplication.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;
    using PizzaFactory.DesktopApplication.Model;

    /// <summary>
    /// ViewModel responsible for the UI flow.
    /// </summary>
    internal class ModifyViewModel : ViewModelBase
    {
        private PizzaModel pizza;

        /// <summary>
        /// Gets or sets PizzaModel being used.
        /// </summary>
        public PizzaModel Pizza
        {
            get { return this.pizza; }
            set { this.Set(ref this.pizza, value); }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModifyViewModel"/> class.
        /// Instantiates a PizzaModel to avoid null reference.
        /// </summary>
        public ModifyViewModel()
        {
            this.pizza = new PizzaModel();

            if (this.IsInDesignMode)
            {
                this.pizza.Id = 0;
                this.pizza.Name = "Random Pizza";
                this.pizza.Size = 32;
                this.pizza.Price = 2500;
                this.pizza.FurtherExpenses = 1.1f;
                this.pizza.SaleMultiplier = 0.8f;
            }
        }
    }
}
