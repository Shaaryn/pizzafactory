﻿// <copyright file="App.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaFactory.DesktopApplication
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using AutoMapper;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;
    using GalaSoft.MvvmLight.Messaging;
    using PizzaFactory.DesktopApplication.BusinessLogic;
    using PizzaFactory.DesktopApplication.Model.Conversion;
    using PizzaFactory.DesktopApplication.UserInterface;

    /// <summary>
    /// Interaction logic for App.xaml
    /// Aspect!.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// Defines the necessary configuration for the mapper.
        /// </summary>
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => PizzaFactoryIOC.Instance);

            PizzaFactoryIOC.Instance.Register<IModifyService, ModifyServiceViaWindow>();
            PizzaFactoryIOC.Instance.Register<IMessenger>(() => Messenger.Default);
            PizzaFactoryIOC.Instance.Register<IPizzaMapper, PizzaMapper>();
            PizzaFactoryIOC.Instance.Register<IPizzaLogic, PizzaLogic>();
        }
    }
}