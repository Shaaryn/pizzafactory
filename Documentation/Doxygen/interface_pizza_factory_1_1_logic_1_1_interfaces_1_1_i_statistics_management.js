var interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_statistics_management =
[
    [ "GetAllIngredients", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_statistics_management.html#a54cdc7898dc832fb70150c992a8c6a72", null ],
    [ "PizzaSearch", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_statistics_management.html#a78078981ffa2cc841b4b15bb0d328c2d", null ],
    [ "PizzaSearchAsync", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_statistics_management.html#a53cdc4955214d78ffc12368a898a8969", null ],
    [ "RenewableMoney", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_statistics_management.html#a1c92a69b42260adab6eba89966575deb", null ],
    [ "RenewableMoneyAsync", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_statistics_management.html#a50d689ef3317e16d92cac0047062d248", null ],
    [ "ToppingLovers", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_statistics_management.html#a414f4894b027e8ec19eeecea48a4f66f", null ],
    [ "ToppingLoversAync", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_statistics_management.html#ac82e44ca15957fef90f3ee4fc3a01404", null ],
    [ "IList< Pizza >", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_statistics_management.html#aa6dbdaa2081810190008adfa319ff02b", null ]
];