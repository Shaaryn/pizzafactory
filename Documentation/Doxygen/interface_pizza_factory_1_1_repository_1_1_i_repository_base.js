var interface_pizza_factory_1_1_repository_1_1_i_repository_base =
[
    [ "GetAll", "interface_pizza_factory_1_1_repository_1_1_i_repository_base.html#aa3f76dd8d800982b2b3759770abdc7fc", null ],
    [ "GetOne", "interface_pizza_factory_1_1_repository_1_1_i_repository_base.html#a616c40a379f77141ca0e5c8f64cd38d7", null ],
    [ "Insert", "interface_pizza_factory_1_1_repository_1_1_i_repository_base.html#ade2ac3e8845da7a22b01780faf9b5215", null ],
    [ "Remove", "interface_pizza_factory_1_1_repository_1_1_i_repository_base.html#a53b22dfd58a6c0c44df164a332c1924b", null ]
];