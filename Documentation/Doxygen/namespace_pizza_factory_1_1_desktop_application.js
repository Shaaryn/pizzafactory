var namespace_pizza_factory_1_1_desktop_application =
[
    [ "BusinessLogic", "namespace_pizza_factory_1_1_desktop_application_1_1_business_logic.html", "namespace_pizza_factory_1_1_desktop_application_1_1_business_logic" ],
    [ "Model", "namespace_pizza_factory_1_1_desktop_application_1_1_model.html", "namespace_pizza_factory_1_1_desktop_application_1_1_model" ],
    [ "UserInterface", "namespace_pizza_factory_1_1_desktop_application_1_1_user_interface.html", "namespace_pizza_factory_1_1_desktop_application_1_1_user_interface" ],
    [ "ViewModel", "namespace_pizza_factory_1_1_desktop_application_1_1_view_model.html", "namespace_pizza_factory_1_1_desktop_application_1_1_view_model" ],
    [ "App", "class_pizza_factory_1_1_desktop_application_1_1_app.html", "class_pizza_factory_1_1_desktop_application_1_1_app" ],
    [ "MainWindow", "class_pizza_factory_1_1_desktop_application_1_1_main_window.html", "class_pizza_factory_1_1_desktop_application_1_1_main_window" ],
    [ "PizzaFactoryIOC", "class_pizza_factory_1_1_desktop_application_1_1_pizza_factory_i_o_c.html", "class_pizza_factory_1_1_desktop_application_1_1_pizza_factory_i_o_c" ]
];