var class_pizza_factory_1_1_wpf_client_1_1_pizza_v_m =
[
    [ "CopyFrom", "class_pizza_factory_1_1_wpf_client_1_1_pizza_v_m.html#a5fb643b356f7f8f69c1f7a9c1a896f40", null ],
    [ "FurtherExpenses", "class_pizza_factory_1_1_wpf_client_1_1_pizza_v_m.html#a651eb24fe29f7bfbde5fd907b33b805d", null ],
    [ "Id", "class_pizza_factory_1_1_wpf_client_1_1_pizza_v_m.html#ab0a620a976977ebbb6da291db6114700", null ],
    [ "Name", "class_pizza_factory_1_1_wpf_client_1_1_pizza_v_m.html#a679da3372090d34e432107ca560e707c", null ],
    [ "Price", "class_pizza_factory_1_1_wpf_client_1_1_pizza_v_m.html#afc34848df050c1effef8c8cf2917638c", null ],
    [ "SaleMultiplier", "class_pizza_factory_1_1_wpf_client_1_1_pizza_v_m.html#a708e35db0df6a1ebfd66c30ea8c45b12", null ],
    [ "Size", "class_pizza_factory_1_1_wpf_client_1_1_pizza_v_m.html#ac1688a2cf9fefb33f97271d954199283", null ],
    [ "PropertyChanged", "class_pizza_factory_1_1_wpf_client_1_1_pizza_v_m.html#a0da2fc3b36523fc28c258c01c605c8c8", null ]
];