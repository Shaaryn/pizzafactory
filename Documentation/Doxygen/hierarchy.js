var hierarchy =
[
    [ "PizzaFactory.Web.Models.ApiResult", "class_pizza_factory_1_1_web_1_1_models_1_1_api_result.html", null ],
    [ "Application", null, [
      [ "PizzaFactory.DesktopApplication.App", "class_pizza_factory_1_1_desktop_application_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "PizzaFactory.DesktopApplication.App", "class_pizza_factory_1_1_desktop_application_1_1_app.html", null ],
      [ "PizzaFactory.DesktopApplication.App", "class_pizza_factory_1_1_desktop_application_1_1_app.html", null ],
      [ "PizzaFactory.DesktopApplication.App", "class_pizza_factory_1_1_desktop_application_1_1_app.html", null ],
      [ "PizzaFactory.WpfClient.App", "class_pizza_factory_1_1_wpf_client_1_1_app.html", null ],
      [ "PizzaFactory.WpfClient.App", "class_pizza_factory_1_1_wpf_client_1_1_app.html", null ],
      [ "PizzaFactory.WpfClient.App", "class_pizza_factory_1_1_wpf_client_1_1_app.html", null ],
      [ "PizzaFactory.WpfClient.App", "class_pizza_factory_1_1_wpf_client_1_1_app.html", null ]
    ] ],
    [ "Controller", null, [
      [ "PizzaFactory.Web.Controllers.HomeController", "class_pizza_factory_1_1_web_1_1_controllers_1_1_home_controller.html", null ],
      [ "PizzaFactory.Web.Controllers.PizzaAPIController", "class_pizza_factory_1_1_web_1_1_controllers_1_1_pizza_a_p_i_controller.html", null ],
      [ "PizzaFactory.Web.Controllers.PizzasController", "class_pizza_factory_1_1_web_1_1_controllers_1_1_pizzas_controller.html", null ]
    ] ],
    [ "PizzaFactory.Data.Models.Human.Customer", "class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_customer.html", null ],
    [ "DbContext", null, [
      [ "PizzaFactory.Data.Models.PizzaFactoryDbContext", "class_pizza_factory_1_1_data_1_1_models_1_1_pizza_factory_db_context.html", null ]
    ] ],
    [ "PizzaFactory.Data.Models.Human.Employee", "class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_employee.html", null ],
    [ "PizzaFactory.Web.Models.ErrorViewModel", "class_pizza_factory_1_1_web_1_1_models_1_1_error_view_model.html", null ],
    [ "Exception", null, [
      [ "PizzaFactory.Logic.Exceptions.LogInEmployeeNotFoundException", "class_pizza_factory_1_1_logic_1_1_exceptions_1_1_log_in_employee_not_found_exception.html", null ],
      [ "PizzaFactory.Repository.Exceptions.EmployeeAlreadyTerminatedException", "class_pizza_factory_1_1_repository_1_1_exceptions_1_1_employee_already_terminated_exception.html", null ],
      [ "PizzaFactory.Repository.Exceptions.EmployeeNotFoundException", "class_pizza_factory_1_1_repository_1_1_exceptions_1_1_employee_not_found_exception.html", null ],
      [ "PizzaFactory.Repository.Exceptions.UnderValueException", "class_pizza_factory_1_1_repository_1_1_exceptions_1_1_under_value_exception.html", null ]
    ] ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "PizzaFactory.DesktopApplication.MainWindow", "class_pizza_factory_1_1_desktop_application_1_1_main_window.html", null ],
      [ "PizzaFactory.DesktopApplication.MainWindow", "class_pizza_factory_1_1_desktop_application_1_1_main_window.html", null ],
      [ "PizzaFactory.DesktopApplication.MainWindow", "class_pizza_factory_1_1_desktop_application_1_1_main_window.html", null ],
      [ "PizzaFactory.DesktopApplication.UserInterface.ModifyWindow", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_modify_window.html", null ],
      [ "PizzaFactory.DesktopApplication.UserInterface.ModifyWindow", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_modify_window.html", null ],
      [ "PizzaFactory.DesktopApplication.UserInterface.ModifyWindow", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_modify_window.html", null ],
      [ "PizzaFactory.DesktopApplication.UserInterface.ModifyWindow", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_modify_window.html", null ],
      [ "PizzaFactory.WpfClient.EditorWindow", "class_pizza_factory_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "PizzaFactory.WpfClient.EditorWindow", "class_pizza_factory_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "PizzaFactory.WpfClient.MainWindow", "class_pizza_factory_1_1_wpf_client_1_1_main_window.html", null ],
      [ "PizzaFactory.WpfClient.MainWindow", "class_pizza_factory_1_1_wpf_client_1_1_main_window.html", null ],
      [ "PizzaFactory.WpfClient.MainWindow", "class_pizza_factory_1_1_wpf_client_1_1_main_window.html", null ]
    ] ],
    [ "PizzaFactory.Logic.Interfaces.ILogIn", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_log_in.html", [
      [ "PizzaFactory.Logic.Classes.LogIn", "class_pizza_factory_1_1_logic_1_1_classes_1_1_log_in.html", null ]
    ] ],
    [ "PizzaFactory.DesktopApplication.BusinessLogic.IModifyService", "interface_pizza_factory_1_1_desktop_application_1_1_business_logic_1_1_i_modify_service.html", [
      [ "PizzaFactory.DesktopApplication.UserInterface.ModifyServiceViaWindow", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_modify_service_via_window.html", null ]
    ] ],
    [ "PizzaFactory.Data.Models.Ingredient", "class_pizza_factory_1_1_data_1_1_models_1_1_ingredient.html", null ],
    [ "INotifyPropertyChanged", null, [
      [ "PizzaFactory.DesktopApplication.Model.PizzaModel", "class_pizza_factory_1_1_desktop_application_1_1_model_1_1_pizza_model.html", null ],
      [ "PizzaFactory.WpfClient.PizzaVM", "class_pizza_factory_1_1_wpf_client_1_1_pizza_v_m.html", null ]
    ] ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "PizzaFactory.Logic.Interfaces.IOrderManagement", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management.html", [
      [ "PizzaFactory.Logic.Classes.OrderManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management.html", null ]
    ] ],
    [ "PizzaFactory.Program.Interfaces.IOrderManagementView", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_order_management_view.html", [
      [ "PizzaFactory.Program.Classes.OrderManagementView", "class_pizza_factory_1_1_program_1_1_classes_1_1_order_management_view.html", null ]
    ] ],
    [ "PizzaFactory.DesktopApplication.BusinessLogic.IPizzaLogic", "interface_pizza_factory_1_1_desktop_application_1_1_business_logic_1_1_i_pizza_logic.html", [
      [ "PizzaFactory.DesktopApplication.BusinessLogic.PizzaLogic", "class_pizza_factory_1_1_desktop_application_1_1_business_logic_1_1_pizza_logic.html", null ]
    ] ],
    [ "PizzaFactory.Logic.Interfaces.IPizzaManagement", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management.html", [
      [ "PizzaFactory.Logic.Classes.PizzaManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management.html", null ]
    ] ],
    [ "PizzaFactory.Program.Interfaces.IPizzaManagementView", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_pizza_management_view.html", [
      [ "PizzaFactory.Program.Classes.PizzaManagementView", "class_pizza_factory_1_1_program_1_1_classes_1_1_pizza_management_view.html", null ]
    ] ],
    [ "PizzaFactory.DesktopApplication.Model.Conversion.IPizzaMapper", "interface_pizza_factory_1_1_desktop_application_1_1_model_1_1_conversion_1_1_i_pizza_mapper.html", [
      [ "PizzaFactory.DesktopApplication.Model.Conversion.PizzaMapper", "class_pizza_factory_1_1_desktop_application_1_1_model_1_1_conversion_1_1_pizza_mapper.html", null ]
    ] ],
    [ "PizzaFactory.Repository.IRepositoryBase< T >", "interface_pizza_factory_1_1_repository_1_1_i_repository_base.html", [
      [ "PizzaFactory.Repository.Classes.RepositoryBase< T >", "class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base.html", null ]
    ] ],
    [ "PizzaFactory.Repository.IRepositoryBase< Customer >", "interface_pizza_factory_1_1_repository_1_1_i_repository_base.html", [
      [ "PizzaFactory.Repository.Interfaces.ICustomerRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_customer_repository.html", [
        [ "PizzaFactory.Repository.Classes.CustomerRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_customer_repository.html", null ]
      ] ]
    ] ],
    [ "PizzaFactory.Repository.IRepositoryBase< Employee >", "interface_pizza_factory_1_1_repository_1_1_i_repository_base.html", [
      [ "PizzaFactory.Repository.Interfaces.IEmployeeRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_employee_repository.html", [
        [ "PizzaFactory.Repository.Classes.EmployeeRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_employee_repository.html", null ]
      ] ]
    ] ],
    [ "PizzaFactory.Repository.IRepositoryBase< Ingredient >", "interface_pizza_factory_1_1_repository_1_1_i_repository_base.html", [
      [ "PizzaFactory.Repository.Interfaces.IIngredientRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_ingredient_repository.html", [
        [ "PizzaFactory.Repository.Classes.IngredientRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_ingredient_repository.html", null ]
      ] ]
    ] ],
    [ "PizzaFactory.Repository.IRepositoryBase< Order >", "interface_pizza_factory_1_1_repository_1_1_i_repository_base.html", [
      [ "PizzaFactory.Repository.Interfaces.IOrderRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_order_repository.html", [
        [ "PizzaFactory.Repository.Classes.OrderRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_order_repository.html", null ]
      ] ]
    ] ],
    [ "PizzaFactory.Repository.IRepositoryBase< OrderPizzaConnector >", "interface_pizza_factory_1_1_repository_1_1_i_repository_base.html", [
      [ "PizzaFactory.Repository.Interfaces.IConnectorOrderPizzaRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_connector_order_pizza_repository.html", [
        [ "PizzaFactory.Repository.Classes.ConnectorOrderPizzaRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_connector_order_pizza_repository.html", null ]
      ] ]
    ] ],
    [ "PizzaFactory.Repository.IRepositoryBase< Person >", "interface_pizza_factory_1_1_repository_1_1_i_repository_base.html", [
      [ "PizzaFactory.Repository.IPersonRepository", "interface_pizza_factory_1_1_repository_1_1_i_person_repository.html", [
        [ "PizzaFactory.Repository.Classes.PersonRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_person_repository.html", null ]
      ] ]
    ] ],
    [ "PizzaFactory.Repository.IRepositoryBase< Pizza >", "interface_pizza_factory_1_1_repository_1_1_i_repository_base.html", [
      [ "PizzaFactory.Repository.Interfaces.IPizzaRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_pizza_repository.html", [
        [ "PizzaFactory.Repository.Classes.PizzaRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_pizza_repository.html", null ]
      ] ]
    ] ],
    [ "PizzaFactory.Repository.IRepositoryBase< PizzaIngredientConnector >", "interface_pizza_factory_1_1_repository_1_1_i_repository_base.html", [
      [ "PizzaFactory.Repository.Interfaces.IConnectorPizzaIngredientRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_connector_pizza_ingredient_repository.html", [
        [ "PizzaFactory.Repository.Classes.ConnectorPizzaIngredientRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_connector_pizza_ingredient_repository.html", null ]
      ] ]
    ] ],
    [ "PizzaFactory.Repository.IRepositoryBase< Pizzeria >", "interface_pizza_factory_1_1_repository_1_1_i_repository_base.html", [
      [ "PizzaFactory.Repository.Interfaces.IPizzeriaRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_pizzeria_repository.html", [
        [ "PizzaFactory.Repository.Classes.PizzeriaRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_pizzeria_repository.html", null ]
      ] ]
    ] ],
    [ "IServiceLocator", null, [
      [ "PizzaFactory.DesktopApplication.PizzaFactoryIOC", "class_pizza_factory_1_1_desktop_application_1_1_pizza_factory_i_o_c.html", null ]
    ] ],
    [ "PizzaFactory.Logic.Interfaces.IStatisticsManagement", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_statistics_management.html", [
      [ "PizzaFactory.Logic.Classes.StatisticsManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_statistics_management.html", null ]
    ] ],
    [ "PizzaFactory.Program.Interfaces.IStatisticsManagementView", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_statistics_management_view.html", [
      [ "PizzaFactory.Program.Classes.StatisticsManagementView", "class_pizza_factory_1_1_program_1_1_classes_1_1_statistics_management_view.html", null ]
    ] ],
    [ "PizzaFactory.Logic.Interfaces.IUserManagement", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_user_management.html", [
      [ "PizzaFactory.Logic.Classes.UserManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_user_management.html", null ]
    ] ],
    [ "PizzaFactory.Program.Interfaces.IUserManagementView", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_user_management_view.html", [
      [ "PizzaFactory.Program.Classes.UserManagementView", "class_pizza_factory_1_1_program_1_1_classes_1_1_user_management_view.html", null ]
    ] ],
    [ "IValueConverter", null, [
      [ "PizzaFactory.DesktopApplication.UserInterface.ExpensesToStringConverter", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_expenses_to_string_converter.html", null ],
      [ "PizzaFactory.DesktopApplication.UserInterface.SaleToStringConverter", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_sale_to_string_converter.html", null ],
      [ "PizzaFactory.DesktopApplication.UserInterface.SizeToStringConverter", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_size_to_string_converter.html", null ]
    ] ],
    [ "PizzaFactory.WpfClient.MainLogic", "class_pizza_factory_1_1_wpf_client_1_1_main_logic.html", null ],
    [ "PizzaFactory.MainProgram", "class_pizza_factory_1_1_main_program.html", null ],
    [ "PizzaFactory.Logic.Tests.MainTest", "class_pizza_factory_1_1_logic_1_1_tests_1_1_main_test.html", null ],
    [ "PizzaFactory.Web.Models.MapperFactory", "class_pizza_factory_1_1_web_1_1_models_1_1_mapper_factory.html", null ],
    [ "PizzaFactory.Data.Models.Order", "class_pizza_factory_1_1_data_1_1_models_1_1_order.html", null ],
    [ "PizzaFactory.Logic.Tests.Classes.OrderManagementTests", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_order_management_tests.html", null ],
    [ "PizzaFactory.Data.Models.Connector.OrderPizzaConnector", "class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_order_pizza_connector.html", null ],
    [ "PizzaFactory.Data.Models.Person", "class_pizza_factory_1_1_data_1_1_models_1_1_person.html", null ],
    [ "PizzaFactory.Data.Models.Pizza", "class_pizza_factory_1_1_data_1_1_models_1_1_pizza.html", null ],
    [ "PizzaFactory.Web.Models.Pizza", "class_pizza_factory_1_1_web_1_1_models_1_1_pizza.html", null ],
    [ "PizzaFactory.Data.Models.Connector.PizzaIngredientConnector", "class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_pizza_ingredient_connector.html", null ],
    [ "PizzaFactory.Logic.Tests.Classes.PizzaManagementTests", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_pizza_management_tests.html", null ],
    [ "PizzaFactory.Web.Models.PizzasViewModel", "class_pizza_factory_1_1_web_1_1_models_1_1_pizzas_view_model.html", null ],
    [ "PizzaFactory.Data.Models.Pizzeria", "class_pizza_factory_1_1_data_1_1_models_1_1_pizzeria.html", null ],
    [ "PizzaFactory.Web.Program", "class_pizza_factory_1_1_web_1_1_program.html", null ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage", null, [
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Pizzas_PizzasDetails", "class_asp_net_core_1_1_views___pizzas___pizzas_details.html", null ],
      [ "AspNetCore.Views_Pizzas_PizzasDetails", "class_asp_net_core_1_1_views___pizzas___pizzas_details.html", null ],
      [ "AspNetCore.Views_Pizzas_PizzasEdit", "class_asp_net_core_1_1_views___pizzas___pizzas_edit.html", null ],
      [ "AspNetCore.Views_Pizzas_PizzasEdit", "class_asp_net_core_1_1_views___pizzas___pizzas_edit.html", null ],
      [ "AspNetCore.Views_Pizzas_PizzasIndex", "class_asp_net_core_1_1_views___pizzas___pizzas_index.html", null ],
      [ "AspNetCore.Views_Pizzas_PizzasIndex", "class_asp_net_core_1_1_views___pizzas___pizzas_index.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ]
    ] ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage< IEnumerable< PizzaFactory.Web.Models.Pizza >>", null, [
      [ "AspNetCore.Views_Pizzas_PizzasList", "class_asp_net_core_1_1_views___pizzas___pizzas_list.html", null ],
      [ "AspNetCore.Views_Pizzas_PizzasList", "class_asp_net_core_1_1_views___pizzas___pizzas_list.html", null ]
    ] ],
    [ "PizzaFactory.Logic.Classes.RenewableEmployees", "class_pizza_factory_1_1_logic_1_1_classes_1_1_renewable_employees.html", null ],
    [ "PizzaFactory.Repository.Classes.RepositoryBase< Customer >", "class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base.html", [
      [ "PizzaFactory.Repository.Classes.CustomerRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_customer_repository.html", null ]
    ] ],
    [ "PizzaFactory.Repository.Classes.RepositoryBase< Employee >", "class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base.html", [
      [ "PizzaFactory.Repository.Classes.EmployeeRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_employee_repository.html", null ]
    ] ],
    [ "PizzaFactory.Repository.Classes.RepositoryBase< Ingredient >", "class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base.html", [
      [ "PizzaFactory.Repository.Classes.IngredientRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_ingredient_repository.html", null ]
    ] ],
    [ "PizzaFactory.Repository.Classes.RepositoryBase< Order >", "class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base.html", [
      [ "PizzaFactory.Repository.Classes.OrderRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_order_repository.html", null ]
    ] ],
    [ "PizzaFactory.Repository.Classes.RepositoryBase< OrderPizzaConnector >", "class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base.html", [
      [ "PizzaFactory.Repository.Classes.ConnectorOrderPizzaRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_connector_order_pizza_repository.html", null ]
    ] ],
    [ "PizzaFactory.Repository.Classes.RepositoryBase< Person >", "class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base.html", [
      [ "PizzaFactory.Repository.Classes.PersonRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_person_repository.html", null ]
    ] ],
    [ "PizzaFactory.Repository.Classes.RepositoryBase< Pizza >", "class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base.html", [
      [ "PizzaFactory.Repository.Classes.PizzaRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_pizza_repository.html", null ]
    ] ],
    [ "PizzaFactory.Repository.Classes.RepositoryBase< PizzaIngredientConnector >", "class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base.html", [
      [ "PizzaFactory.Repository.Classes.ConnectorPizzaIngredientRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_connector_pizza_ingredient_repository.html", null ]
    ] ],
    [ "PizzaFactory.Repository.Classes.RepositoryBase< Pizzeria >", "class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base.html", [
      [ "PizzaFactory.Repository.Classes.PizzeriaRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_pizzeria_repository.html", null ]
    ] ],
    [ "SimpleIoc", null, [
      [ "PizzaFactory.DesktopApplication.PizzaFactoryIOC", "class_pizza_factory_1_1_desktop_application_1_1_pizza_factory_i_o_c.html", null ]
    ] ],
    [ "PizzaFactory.Web.Startup", "class_pizza_factory_1_1_web_1_1_startup.html", null ],
    [ "PizzaFactory.Logic.Tests.Classes.StatisticsManagementTests", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_statistics_management_tests.html", null ],
    [ "PizzaFactory.Logic.Tests.Classes.UserManagementTests", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_user_management_tests.html", null ],
    [ "ViewModelBase", null, [
      [ "PizzaFactory.DesktopApplication.ViewModel.MainViewModel", "class_pizza_factory_1_1_desktop_application_1_1_view_model_1_1_main_view_model.html", null ],
      [ "PizzaFactory.DesktopApplication.ViewModel.ModifyViewModel", "class_pizza_factory_1_1_desktop_application_1_1_view_model_1_1_modify_view_model.html", null ],
      [ "PizzaFactory.WpfClient.MainVM", "class_pizza_factory_1_1_wpf_client_1_1_main_v_m.html", null ]
    ] ],
    [ "System.Windows.Window", null, [
      [ "PizzaFactory.DesktopApplication.MainWindow", "class_pizza_factory_1_1_desktop_application_1_1_main_window.html", null ],
      [ "PizzaFactory.DesktopApplication.MainWindow", "class_pizza_factory_1_1_desktop_application_1_1_main_window.html", null ],
      [ "PizzaFactory.DesktopApplication.MainWindow", "class_pizza_factory_1_1_desktop_application_1_1_main_window.html", null ],
      [ "PizzaFactory.DesktopApplication.UserInterface.ModifyWindow", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_modify_window.html", null ],
      [ "PizzaFactory.DesktopApplication.UserInterface.ModifyWindow", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_modify_window.html", null ],
      [ "PizzaFactory.DesktopApplication.UserInterface.ModifyWindow", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_modify_window.html", null ],
      [ "PizzaFactory.DesktopApplication.UserInterface.ModifyWindow", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_modify_window.html", null ],
      [ "PizzaFactory.DesktopApplication.UserInterface.ModifyWindow", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_modify_window.html", null ],
      [ "PizzaFactory.WpfClient.EditorWindow", "class_pizza_factory_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "PizzaFactory.WpfClient.EditorWindow", "class_pizza_factory_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "PizzaFactory.WpfClient.EditorWindow", "class_pizza_factory_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "PizzaFactory.WpfClient.MainWindow", "class_pizza_factory_1_1_wpf_client_1_1_main_window.html", null ],
      [ "PizzaFactory.WpfClient.MainWindow", "class_pizza_factory_1_1_wpf_client_1_1_main_window.html", null ],
      [ "PizzaFactory.WpfClient.MainWindow", "class_pizza_factory_1_1_wpf_client_1_1_main_window.html", null ],
      [ "PizzaFactory.WpfClient.MainWindow", "class_pizza_factory_1_1_wpf_client_1_1_main_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "PizzaFactory.DesktopApplication.MainWindow", "class_pizza_factory_1_1_desktop_application_1_1_main_window.html", null ]
    ] ]
];