var indexSectionsWithContent =
{
  0: "._acdefghijlmnoprstuvx",
  1: "aceghilmoprsuv",
  2: "apx",
  3: "._aceghilmoprsu",
  4: "acdefghilmoprstuv",
  5: "cfilnps",
  6: "ar",
  7: "acdmrsu",
  8: "acdefhijlmnoprstu",
  9: "p",
  10: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "events",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties",
  9: "Events",
  10: "Pages"
};

