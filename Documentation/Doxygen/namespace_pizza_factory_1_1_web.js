var namespace_pizza_factory_1_1_web =
[
    [ "Controllers", "namespace_pizza_factory_1_1_web_1_1_controllers.html", "namespace_pizza_factory_1_1_web_1_1_controllers" ],
    [ "Models", "namespace_pizza_factory_1_1_web_1_1_models.html", "namespace_pizza_factory_1_1_web_1_1_models" ],
    [ "Program", "class_pizza_factory_1_1_web_1_1_program.html", "class_pizza_factory_1_1_web_1_1_program" ],
    [ "Startup", "class_pizza_factory_1_1_web_1_1_startup.html", "class_pizza_factory_1_1_web_1_1_startup" ]
];