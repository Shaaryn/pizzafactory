var class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_customer =
[
    [ "Equals", "class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_customer.html#af58906353a3a2ccdd37651862ed7742f", null ],
    [ "GetHashCode", "class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_customer.html#ae7fa84b25dba8007c0c2c34c44ed4923", null ],
    [ "ToString", "class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_customer.html#aeb42c2fce211375f85292e265883345d", null ],
    [ "Id", "class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_customer.html#aeeec9aa5b8ccb27a7ac3c00aaa07f2f8", null ],
    [ "Orders", "class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_customer.html#a2f119b23a748a16ca5c0a7a2cab5613c", null ],
    [ "Person", "class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_customer.html#a03f2ae86ebeab7c97ee48eb381a66841", null ],
    [ "PersonId", "class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_customer.html#a39e50712f89c4fe67a3c57e049569706", null ],
    [ "RegisteredOn", "class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_customer.html#a457ebd191b199728425e49ad22cede93", null ]
];