var class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base =
[
    [ "RepositoryBase", "class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base.html#a2e9b151fa69153fa059dab469d2a0ce0", null ],
    [ "GetAll", "class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base.html#a2048ef900db176e42b9480875331c2f8", null ],
    [ "GetOne", "class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base.html#a6bca54be3a1803a17bb06a623c5608ee", null ],
    [ "Insert", "class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base.html#a4f1d9aff848b1733952723aec9a3b3e6", null ],
    [ "Remove", "class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base.html#ac0d8c4d881a380ede2512b5f0058e0e5", null ],
    [ "ctx", "class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base.html#aafe1f778e3da2fef8f828693af7c535d", null ]
];