var class_pizza_factory_1_1_data_1_1_models_1_1_pizzeria =
[
    [ "Equals", "class_pizza_factory_1_1_data_1_1_models_1_1_pizzeria.html#a5e4f5459341b5c308d60a85d60273dc0", null ],
    [ "GetHashCode", "class_pizza_factory_1_1_data_1_1_models_1_1_pizzeria.html#ae5b07c4f07e84ff5364552a7ef759c84", null ],
    [ "ToString", "class_pizza_factory_1_1_data_1_1_models_1_1_pizzeria.html#a599ea1596f97dc8c70ca413581720e0d", null ],
    [ "Address", "class_pizza_factory_1_1_data_1_1_models_1_1_pizzeria.html#af02f5f569366c30fb84b0b24c1fc78f2", null ],
    [ "EstablishedDate", "class_pizza_factory_1_1_data_1_1_models_1_1_pizzeria.html#a1506b93e25836f9782ec98131102263e", null ],
    [ "Id", "class_pizza_factory_1_1_data_1_1_models_1_1_pizzeria.html#a01e9ec9790e3f81b23c2ea4515c9c83b", null ],
    [ "Name", "class_pizza_factory_1_1_data_1_1_models_1_1_pizzeria.html#ad1782f66372e55a18418d055e404f7a5", null ],
    [ "Orders", "class_pizza_factory_1_1_data_1_1_models_1_1_pizzeria.html#adcf68271e1ee5db0cc8c29c323357c22", null ],
    [ "Person", "class_pizza_factory_1_1_data_1_1_models_1_1_pizzeria.html#ae2670cb945c098271ccbf102def43c8c", null ],
    [ "PersonId", "class_pizza_factory_1_1_data_1_1_models_1_1_pizzeria.html#aeef5b614777cb98186cdb453ab229723", null ]
];