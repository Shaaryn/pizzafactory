var class_pizza_factory_1_1_logic_1_1_classes_1_1_statistics_management =
[
    [ "StatisticsManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_statistics_management.html#ad7b9fc8eb198a55ff57d4454dec5f080", null ],
    [ "GetAllIngredients", "class_pizza_factory_1_1_logic_1_1_classes_1_1_statistics_management.html#acba0bf8d5ed22515a03cc8a6cd558bab", null ],
    [ "PizzaSearch", "class_pizza_factory_1_1_logic_1_1_classes_1_1_statistics_management.html#ae6f87b8f12f4babb6014a57789f4aa94", null ],
    [ "PizzaSearchAsync", "class_pizza_factory_1_1_logic_1_1_classes_1_1_statistics_management.html#a53b1aca30368ba6293d925c0241d595e", null ],
    [ "RenewableMoney", "class_pizza_factory_1_1_logic_1_1_classes_1_1_statistics_management.html#a3223ce0a79e0bbb2ea5749e60de0c8de", null ],
    [ "RenewableMoneyAsync", "class_pizza_factory_1_1_logic_1_1_classes_1_1_statistics_management.html#a4cb6e53f6a154352e7c7e3290a82650c", null ],
    [ "ToppingLoversAync", "class_pizza_factory_1_1_logic_1_1_classes_1_1_statistics_management.html#ae78036755e58f4d34abbefca73f5366a", null ],
    [ "IList< Pizza >", "class_pizza_factory_1_1_logic_1_1_classes_1_1_statistics_management.html#a6f132239bf6e313be2adfebef0beed96", null ]
];