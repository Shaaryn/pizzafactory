var namespace_pizza_factory_1_1_web_1_1_models =
[
    [ "ApiResult", "class_pizza_factory_1_1_web_1_1_models_1_1_api_result.html", "class_pizza_factory_1_1_web_1_1_models_1_1_api_result" ],
    [ "ErrorViewModel", "class_pizza_factory_1_1_web_1_1_models_1_1_error_view_model.html", "class_pizza_factory_1_1_web_1_1_models_1_1_error_view_model" ],
    [ "MapperFactory", "class_pizza_factory_1_1_web_1_1_models_1_1_mapper_factory.html", "class_pizza_factory_1_1_web_1_1_models_1_1_mapper_factory" ],
    [ "Pizza", "class_pizza_factory_1_1_web_1_1_models_1_1_pizza.html", "class_pizza_factory_1_1_web_1_1_models_1_1_pizza" ],
    [ "PizzasViewModel", "class_pizza_factory_1_1_web_1_1_models_1_1_pizzas_view_model.html", "class_pizza_factory_1_1_web_1_1_models_1_1_pizzas_view_model" ]
];