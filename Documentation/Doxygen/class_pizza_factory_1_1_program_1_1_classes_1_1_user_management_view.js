var class_pizza_factory_1_1_program_1_1_classes_1_1_user_management_view =
[
    [ "AddEmployee", "class_pizza_factory_1_1_program_1_1_classes_1_1_user_management_view.html#a3821b69cf5845aec623df9ca84b3efd1", null ],
    [ "ManageUsers", "class_pizza_factory_1_1_program_1_1_classes_1_1_user_management_view.html#af14002b8edf439880f7c92bfd1c4dd83", null ],
    [ "ModifyEmployee", "class_pizza_factory_1_1_program_1_1_classes_1_1_user_management_view.html#a2136a3435dbb46c04c41ce24dd0364a9", null ],
    [ "ModifyEmployeePerson", "class_pizza_factory_1_1_program_1_1_classes_1_1_user_management_view.html#afc443b1efa3b26f4210cc44294b5a294", null ],
    [ "ModifyEmployeePersonPart", "class_pizza_factory_1_1_program_1_1_classes_1_1_user_management_view.html#a03ec641869c2f6c83e7a814d26797a8d", null ],
    [ "ModifyEmployeeRole", "class_pizza_factory_1_1_program_1_1_classes_1_1_user_management_view.html#a2913faf0eda8d383ad8f4857cb5b2c9b", null ],
    [ "SuccessfullOperationMessage", "class_pizza_factory_1_1_program_1_1_classes_1_1_user_management_view.html#a01744836585cefba53f39fa2c2135aa0", null ]
];