var interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management =
[
    [ "AddCustomer", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management.html#a2359975de825058ddf950140ef1d946f", null ],
    [ "GetAllOrders", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management.html#abd510fbad8952c4febe60d423864c11a", null ],
    [ "GetAllPizzas", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management.html#aed2dc550405ac63f090a17aa462bb719", null ],
    [ "GetShoppingCart", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management.html#aa28ff7605999ad8714241291564ca000", null ],
    [ "PlaceOrder", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management.html#a8af14ff8a914361cef85aeb111dc386e", null ],
    [ "RemoveOrder", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management.html#acbfe4a3f451efda1f552a80f38554fc7", null ],
    [ "RemoveShoppingCartItem", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management.html#a84ed83298c109881f75613939b4b20fb", null ],
    [ "SearchCustomer", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management.html#a27a2e17ccf961da43902ee063a81a4db", null ],
    [ "SetShoppingCartItem", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management.html#a296578ff9b0d632f104bd56a85dfd372", null ],
    [ "Customer", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management.html#a476cb323c3e0e29c0552f7a516611c9c", null ],
    [ "List< Pizza >", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management.html#a37a05404587c25659bd363686c990f0e", null ]
];