var dir_a8daf82a9bce973f2127bd5472cf17fc =
[
    [ "LogIn.cs", "_log_in_8cs.html", [
      [ "LogIn", "class_pizza_factory_1_1_logic_1_1_classes_1_1_log_in.html", "class_pizza_factory_1_1_logic_1_1_classes_1_1_log_in" ]
    ] ],
    [ "OrderManagement.cs", "_order_management_8cs.html", [
      [ "OrderManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management.html", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management" ]
    ] ],
    [ "PizzaManagement.cs", "_pizza_management_8cs.html", [
      [ "PizzaManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management.html", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management" ]
    ] ],
    [ "RenewableEmployees.cs", "_renewable_employees_8cs.html", [
      [ "RenewableEmployees", "class_pizza_factory_1_1_logic_1_1_classes_1_1_renewable_employees.html", "class_pizza_factory_1_1_logic_1_1_classes_1_1_renewable_employees" ]
    ] ],
    [ "StatisticsManagement.cs", "_statistics_management_8cs.html", [
      [ "StatisticsManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_statistics_management.html", "class_pizza_factory_1_1_logic_1_1_classes_1_1_statistics_management" ]
    ] ],
    [ "UserManagement.cs", "_user_management_8cs.html", [
      [ "UserManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_user_management.html", "class_pizza_factory_1_1_logic_1_1_classes_1_1_user_management" ]
    ] ]
];