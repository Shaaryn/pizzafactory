var interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management =
[
    [ "AddPizza", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management.html#a23ecf72f6277da1fd35d99d94232f672", null ],
    [ "AddSelectedItem", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management.html#aee317738bab83d3e7ac435cf91ee7025", null ],
    [ "EditPizza", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management.html#a237881b0a998cd0b8d71d74b16a7d0f0", null ],
    [ "GetAllIngredients", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management.html#ad057c90ded76788a826b2436edfcf43e", null ],
    [ "GetAllPizzas", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management.html#a6131a369a50359f9c166c0c7cf7949f6", null ],
    [ "GetOnePizza", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management.html#ab61b3233e3aacf4d17bd63b5b35a0155", null ],
    [ "GetSelectedIngredients", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management.html#a04c9bbc78061188ba2de70d901814f33", null ],
    [ "ModifyPizza", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management.html#a9e315760a20db9295e380eae2e03fb8b", null ],
    [ "RemovePizza", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management.html#a0e99d50d19deb7b35443332a4a86aa3c", null ],
    [ "RemoveSelectedItem", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management.html#aece4a8ce2fe3fc75c323a928b65fae10", null ],
    [ "SubmitNewPizza", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management.html#a8a2a7ecd160e9ab73b1ccfb8d947410d", null ]
];