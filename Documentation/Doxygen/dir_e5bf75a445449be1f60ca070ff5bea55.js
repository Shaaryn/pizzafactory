var dir_e5bf75a445449be1f60ca070ff5bea55 =
[
    [ "ILogIn.cs", "_i_log_in_8cs.html", [
      [ "ILogIn", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_log_in.html", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_log_in" ]
    ] ],
    [ "IOrderManagement.cs", "_i_order_management_8cs.html", [
      [ "IOrderManagement", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management.html", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management" ]
    ] ],
    [ "IPizzaManagement.cs", "_i_pizza_management_8cs.html", [
      [ "IPizzaManagement", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management.html", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management" ]
    ] ],
    [ "IStatisticsManagement.cs", "_i_statistics_management_8cs.html", [
      [ "IStatisticsManagement", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_statistics_management.html", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_statistics_management" ]
    ] ],
    [ "IUserManagement.cs", "_i_user_management_8cs.html", [
      [ "IUserManagement", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_user_management.html", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_user_management" ]
    ] ]
];