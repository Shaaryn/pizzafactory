var interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_order_management_view =
[
    [ "AddCustomer", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_order_management_view.html#a5d4c08b94f4090287e51f946b690c2f1", null ],
    [ "ContinueWithCustomer", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_order_management_view.html#a120fe6be299a471a7eace61df0e7565f", null ],
    [ "ManageShoppingCart", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_order_management_view.html#a739200ec69fb5d39efe872cec40954bc", null ],
    [ "OrderCheckout", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_order_management_view.html#a26103d863f9e5540eb6be6b8e33a68dc", null ],
    [ "OrderPreview", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_order_management_view.html#a762adfe7a7a05265faa17c77420e5883", null ],
    [ "SelectCustomer", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_order_management_view.html#a5aabb8e2160a26225de7d8336e23d7fa", null ],
    [ "SelectPizzas", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_order_management_view.html#a8ab8e0f478ce0b340d32a2ca3a0d3443", null ],
    [ "ViewOrders", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_order_management_view.html#a146a7a58095f277bae7a1cce7d90633c", null ]
];