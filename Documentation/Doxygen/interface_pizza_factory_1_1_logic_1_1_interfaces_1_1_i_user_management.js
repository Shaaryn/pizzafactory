var interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_user_management =
[
    [ "AddEmployee", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_user_management.html#ac5ed6a34a56e34f4985850e369687092", null ],
    [ "GetAllUsers", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_user_management.html#ad8a658cb49751b11537f084a678e58ff", null ],
    [ "ModifyEmployeePerson", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_user_management.html#a78470892d488ebbc331c022813e87d13", null ],
    [ "ModifyEmployeeRole", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_user_management.html#a704c02e0631049b641a3bd55d424e652", null ]
];