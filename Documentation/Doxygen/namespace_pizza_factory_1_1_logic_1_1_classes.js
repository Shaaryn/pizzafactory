var namespace_pizza_factory_1_1_logic_1_1_classes =
[
    [ "LogIn", "class_pizza_factory_1_1_logic_1_1_classes_1_1_log_in.html", "class_pizza_factory_1_1_logic_1_1_classes_1_1_log_in" ],
    [ "OrderManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management.html", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management" ],
    [ "PizzaManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management.html", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management" ],
    [ "RenewableEmployees", "class_pizza_factory_1_1_logic_1_1_classes_1_1_renewable_employees.html", "class_pizza_factory_1_1_logic_1_1_classes_1_1_renewable_employees" ],
    [ "StatisticsManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_statistics_management.html", "class_pizza_factory_1_1_logic_1_1_classes_1_1_statistics_management" ],
    [ "UserManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_user_management.html", "class_pizza_factory_1_1_logic_1_1_classes_1_1_user_management" ]
];