var class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management =
[
    [ "OrderManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management.html#a33864eb5e91b3f963860763b1b6a6060", null ],
    [ "AddCustomer", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management.html#a7dd91c67aaf66f8057ca015732e9bf52", null ],
    [ "GetAllOrders", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management.html#ae3d5129e66dfc42074f10dda33e72e90", null ],
    [ "GetAllPizzas", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management.html#a8a5df027df0ded8df943d004225052b9", null ],
    [ "PlaceOrder", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management.html#a61b4b901a7995893564e0ccee4455879", null ],
    [ "RemoveOrder", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management.html#a387a9d906b4dd85026eb5146bc9bff51", null ],
    [ "RemoveShoppingCartItem", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management.html#a1ea6c311909bbf1c9d649bbb8bb28b88", null ],
    [ "SetShoppingCartItem", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management.html#aa25575465a794f50f276430271325319", null ],
    [ "Customer", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management.html#a00b8c87b8e2e4c852fd24d5c605f0163", null ],
    [ "List< Pizza >", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management.html#a0dbc796fedc118847be276972cc63a88", null ],
    [ "OrderedPizzas", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management.html#ad8fc32f5cd7eab1d3ba5739af8b42637", null ],
    [ "OrderingCustomer", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management.html#ad20cfbcbf58442bde1aaad356aa47961", null ]
];