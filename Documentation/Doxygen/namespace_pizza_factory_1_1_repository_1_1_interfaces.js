var namespace_pizza_factory_1_1_repository_1_1_interfaces =
[
    [ "IConnectorOrderPizzaRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_connector_order_pizza_repository.html", null ],
    [ "IConnectorPizzaIngredientRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_connector_pizza_ingredient_repository.html", null ],
    [ "ICustomerRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_customer_repository.html", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_customer_repository" ],
    [ "IEmployeeRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_employee_repository.html", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_employee_repository" ],
    [ "IIngredientRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_ingredient_repository.html", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_ingredient_repository" ],
    [ "IOrderRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_order_repository.html", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_order_repository" ],
    [ "IPizzaRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_pizza_repository.html", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_pizza_repository" ],
    [ "IPizzeriaRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_pizzeria_repository.html", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_pizzeria_repository" ]
];