var interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_user_management_view =
[
    [ "AddEmployee", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_user_management_view.html#aa21b10e0eb22e0a8be75342e4a8da0c3", null ],
    [ "ManageUsers", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_user_management_view.html#a941452574b4ce0c13a8d55bb05c84c42", null ],
    [ "ModifyEmployee", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_user_management_view.html#aa4a9799042d77272832ecb353de6631d", null ],
    [ "ModifyEmployeePerson", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_user_management_view.html#a62303dee1bb0460b9100b83fc87ca796", null ],
    [ "ModifyEmployeePersonPart", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_user_management_view.html#adf233c13bcf415943a93ea0bb8953fb4", null ],
    [ "ModifyEmployeeRole", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_user_management_view.html#abf59b4970fdc45e6cb707b9d1a0f5e27", null ],
    [ "SuccessfullOperationMessage", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_user_management_view.html#a689d8a01834a59cc1366a7ae7d364fab", null ]
];