var dir_9ebc42b496f82333105d7da94467f6c9 =
[
    [ "obj", "dir_d2a2414062f721df1e0c32bd2c733950.html", "dir_d2a2414062f721df1e0c32bd2c733950" ],
    [ "App.xaml.cs", "_pizza_factory_8_wpf_client_2_app_8xaml_8cs.html", [
      [ "App", "class_pizza_factory_1_1_wpf_client_1_1_app.html", "class_pizza_factory_1_1_wpf_client_1_1_app" ]
    ] ],
    [ "AssemblyInfo.cs", "_pizza_factory_8_wpf_client_2_assembly_info_8cs.html", null ],
    [ "EditorWindow.xaml.cs", "_editor_window_8xaml_8cs.html", [
      [ "EditorWindow", "class_pizza_factory_1_1_wpf_client_1_1_editor_window.html", "class_pizza_factory_1_1_wpf_client_1_1_editor_window" ]
    ] ],
    [ "GlobalSuppressions.cs", "_pizza_factory_8_wpf_client_2_global_suppressions_8cs.html", null ],
    [ "MainLogic.cs", "_main_logic_8cs.html", [
      [ "MainLogic", "class_pizza_factory_1_1_wpf_client_1_1_main_logic.html", "class_pizza_factory_1_1_wpf_client_1_1_main_logic" ]
    ] ],
    [ "MainVM.cs", "_main_v_m_8cs.html", [
      [ "MainVM", "class_pizza_factory_1_1_wpf_client_1_1_main_v_m.html", "class_pizza_factory_1_1_wpf_client_1_1_main_v_m" ]
    ] ],
    [ "MainWindow.xaml.cs", "_pizza_factory_8_wpf_client_2_main_window_8xaml_8cs.html", [
      [ "MainWindow", "class_pizza_factory_1_1_wpf_client_1_1_main_window.html", "class_pizza_factory_1_1_wpf_client_1_1_main_window" ]
    ] ],
    [ "PizzaVM.cs", "_pizza_v_m_8cs.html", [
      [ "PizzaVM", "class_pizza_factory_1_1_wpf_client_1_1_pizza_v_m.html", "class_pizza_factory_1_1_wpf_client_1_1_pizza_v_m" ]
    ] ]
];