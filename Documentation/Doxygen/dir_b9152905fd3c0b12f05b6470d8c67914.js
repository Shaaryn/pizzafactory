var dir_b9152905fd3c0b12f05b6470d8c67914 =
[
    [ "BusinessLogic", "dir_2e211ff77799404cfa2f742f42e26d4e.html", "dir_2e211ff77799404cfa2f742f42e26d4e" ],
    [ "Model", "dir_af7a87bdd1476c9866ef5484923aecd0.html", "dir_af7a87bdd1476c9866ef5484923aecd0" ],
    [ "obj", "dir_e5f22b23976564d7bb0717e44ae89d34.html", "dir_e5f22b23976564d7bb0717e44ae89d34" ],
    [ "UserInterface", "dir_a17f58347bcb537db6b488d72d0dd995.html", "dir_a17f58347bcb537db6b488d72d0dd995" ],
    [ "ViewModel", "dir_ce6a0ae96872c32fa8dade8af9218af5.html", "dir_ce6a0ae96872c32fa8dade8af9218af5" ],
    [ "App.xaml.cs", "_pizza_factory_8_desktop_application_2_app_8xaml_8cs.html", [
      [ "App", "class_pizza_factory_1_1_desktop_application_1_1_app.html", "class_pizza_factory_1_1_desktop_application_1_1_app" ]
    ] ],
    [ "AssemblyInfo.cs", "_pizza_factory_8_desktop_application_2_assembly_info_8cs.html", null ],
    [ "GlobalSuppressions.cs", "_pizza_factory_8_desktop_application_2_global_suppressions_8cs.html", null ],
    [ "MainWindow.xaml.cs", "_pizza_factory_8_desktop_application_2_main_window_8xaml_8cs.html", [
      [ "MainWindow", "class_pizza_factory_1_1_desktop_application_1_1_main_window.html", "class_pizza_factory_1_1_desktop_application_1_1_main_window" ]
    ] ],
    [ "PizzaFactoryIOC.cs", "_pizza_factory_i_o_c_8cs.html", [
      [ "PizzaFactoryIOC", "class_pizza_factory_1_1_desktop_application_1_1_pizza_factory_i_o_c.html", "class_pizza_factory_1_1_desktop_application_1_1_pizza_factory_i_o_c" ]
    ] ]
];