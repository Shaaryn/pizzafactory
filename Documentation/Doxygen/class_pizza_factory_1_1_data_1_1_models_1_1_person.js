var class_pizza_factory_1_1_data_1_1_models_1_1_person =
[
    [ "Equals", "class_pizza_factory_1_1_data_1_1_models_1_1_person.html#aaac34fa9b0ba32762ee63d322cbdf74a", null ],
    [ "GetHashCode", "class_pizza_factory_1_1_data_1_1_models_1_1_person.html#a0cbd20e3dff9b4a432f67b976a9cac8a", null ],
    [ "ToString", "class_pizza_factory_1_1_data_1_1_models_1_1_person.html#ac2d8e7abbbeebdec397936f5de210bc0", null ],
    [ "Address", "class_pizza_factory_1_1_data_1_1_models_1_1_person.html#a7392694044a871bad124a1824450aeaa", null ],
    [ "Customer", "class_pizza_factory_1_1_data_1_1_models_1_1_person.html#a7440d4f5b7662ef66b1825494a4a1ed2", null ],
    [ "CustomerId", "class_pizza_factory_1_1_data_1_1_models_1_1_person.html#a46477c22193dcb994a5557993a932a9e", null ],
    [ "Employee", "class_pizza_factory_1_1_data_1_1_models_1_1_person.html#a6c42d28b0c99e12f1647d2d3fc3a308f", null ],
    [ "EmployeeId", "class_pizza_factory_1_1_data_1_1_models_1_1_person.html#a2fcf7bb3fa040d0626beadff1fd143fd", null ],
    [ "FullName", "class_pizza_factory_1_1_data_1_1_models_1_1_person.html#acaa9f2293951cbabc0fcfd237f1ada68", null ],
    [ "Id", "class_pizza_factory_1_1_data_1_1_models_1_1_person.html#ad296556bd195667f8bf5be15f1c4829d", null ],
    [ "PhoneNumber", "class_pizza_factory_1_1_data_1_1_models_1_1_person.html#aa7a16472d4358c89c8e17b78bdd4564e", null ]
];