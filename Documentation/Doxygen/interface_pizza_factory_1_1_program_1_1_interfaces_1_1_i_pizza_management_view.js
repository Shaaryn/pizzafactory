var interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_pizza_management_view =
[
    [ "AddPizza", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_pizza_management_view.html#a4613b41c41189f471a07d184fdece7a0", null ],
    [ "FinalizePizzaAddition", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_pizza_management_view.html#a066047140776dd206a445d58cc3f7c33", null ],
    [ "ManageIngredientSelection", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_pizza_management_view.html#a9114680b4041a971e371c12004cd28af", null ],
    [ "ModifyOptions", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_pizza_management_view.html#a7fc4fd673ea86853fd3ff712f0c65fe1", null ],
    [ "ModifyProcess", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_pizza_management_view.html#a10d9efae0f2976994b34fee714e8189c", null ],
    [ "PizzaAdditionPreview", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_pizza_management_view.html#a2d5fdfd22c4568a53d028951d818074c", null ],
    [ "SelectIngredients", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_pizza_management_view.html#ab97c6f38cc48ccf0f81fb218f039f426", null ],
    [ "SuccessfullOperationMessage", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_pizza_management_view.html#a80d998c90f4fac83b5d782d8a662bc54", null ],
    [ "ViewPizzas", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_pizza_management_view.html#ae4d90e8a9a07e0bd1f9cd6fd81ae7863", null ]
];