var class_pizza_factory_1_1_desktop_application_1_1_view_model_1_1_main_view_model =
[
    [ "MainViewModel", "class_pizza_factory_1_1_desktop_application_1_1_view_model_1_1_main_view_model.html#a306bfc40bf27ae9fa3c1e4202ded7508", null ],
    [ "MainViewModel", "class_pizza_factory_1_1_desktop_application_1_1_view_model_1_1_main_view_model.html#af6238ebefab73252e440b119592a742a", null ],
    [ "AddCommand", "class_pizza_factory_1_1_desktop_application_1_1_view_model_1_1_main_view_model.html#af306ffce1df92498ae7e91517cee4c98", null ],
    [ "Menu", "class_pizza_factory_1_1_desktop_application_1_1_view_model_1_1_main_view_model.html#aa23fbbe2ba8616332865e62d1eaead84", null ],
    [ "ModifyCommand", "class_pizza_factory_1_1_desktop_application_1_1_view_model_1_1_main_view_model.html#af1ea72e6ef0a23b8cdb87567907b73cb", null ],
    [ "RemoveCommand", "class_pizza_factory_1_1_desktop_application_1_1_view_model_1_1_main_view_model.html#a05528a1136a03ad47472e2e7bdaf00d7", null ],
    [ "SelectedPizza", "class_pizza_factory_1_1_desktop_application_1_1_view_model_1_1_main_view_model.html#a477526f4a65725c5270d298d9473b579", null ]
];