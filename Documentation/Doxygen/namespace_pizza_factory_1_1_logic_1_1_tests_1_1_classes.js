var namespace_pizza_factory_1_1_logic_1_1_tests_1_1_classes =
[
    [ "OrderManagementTests", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_order_management_tests.html", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_order_management_tests" ],
    [ "PizzaManagementTests", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_pizza_management_tests.html", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_pizza_management_tests" ],
    [ "StatisticsManagementTests", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_statistics_management_tests.html", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_statistics_management_tests" ],
    [ "UserManagementTests", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_user_management_tests.html", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_user_management_tests" ]
];