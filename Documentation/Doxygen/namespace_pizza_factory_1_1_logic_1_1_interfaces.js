var namespace_pizza_factory_1_1_logic_1_1_interfaces =
[
    [ "ILogIn", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_log_in.html", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_log_in" ],
    [ "IOrderManagement", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management.html", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management" ],
    [ "IPizzaManagement", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management.html", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management" ],
    [ "IStatisticsManagement", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_statistics_management.html", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_statistics_management" ],
    [ "IUserManagement", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_user_management.html", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_user_management" ]
];