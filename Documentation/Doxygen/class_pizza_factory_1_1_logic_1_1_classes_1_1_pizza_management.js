var class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management =
[
    [ "PizzaManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management.html#a0f7208e56fc54364b6de4da1d9dfd7ac", null ],
    [ "AddPizza", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management.html#a156e101d1c34f5d7643bbeb55c0d74be", null ],
    [ "AddSelectedItem", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management.html#a5249060ed28d83fc5c1e70165517acdb", null ],
    [ "EditPizza", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management.html#ae84a0ffbe53e2fbec5ecb25dcdd2f4f2", null ],
    [ "GetAllIngredients", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management.html#a1bd8ab1827e2ecc29f0a256c2db7dbdd", null ],
    [ "GetAllPizzas", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management.html#a358a00401b404657a3c219d507d8a562", null ],
    [ "GetOnePizza", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management.html#af23a198232b3f9dbd1f4c872180e4380", null ],
    [ "GetSelectedIngredients", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management.html#a74c3c272180436255623cc0e8023090f", null ],
    [ "ModifyPizza", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management.html#a7f4e0d92bd34ea436f859c83d7a834c9", null ],
    [ "RemovePizza", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management.html#aa3e10a47748f899e2654a40336154d4a", null ],
    [ "RemoveSelectedItem", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management.html#a9782744157104b7a2c51ed2ffa7b9e87", null ],
    [ "SubmitNewPizza", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management.html#a1758cddc2dd585f9f623fad79c49760d", null ],
    [ "NewPizza", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management.html#adbf95e80877b3802dad412c22b3d0087", null ]
];