var class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_order_pizza_connector =
[
    [ "Amount", "class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_order_pizza_connector.html#a2a6ecdcaebba1f36d11743da946901f8", null ],
    [ "Id", "class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_order_pizza_connector.html#a90fa86a44eabfe1d39c1456e4d472ba7", null ],
    [ "Order", "class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_order_pizza_connector.html#ac0f205794021f0fd85ade4f6b187b9ea", null ],
    [ "OrderId", "class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_order_pizza_connector.html#a3e80f421849c78bcd63b3a68b49ab1c0", null ],
    [ "Pizza", "class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_order_pizza_connector.html#ac3682579a465cc4fd53eff8e1ae1653a", null ],
    [ "PizzaId", "class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_order_pizza_connector.html#a2159126655280d5e0cd918164d78fad3", null ],
    [ "TotalPrice", "class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_order_pizza_connector.html#ae0b46aebdf742b5ea25a1f9f2a2aebeb", null ]
];