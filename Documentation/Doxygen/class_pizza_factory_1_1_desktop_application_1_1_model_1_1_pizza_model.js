var class_pizza_factory_1_1_desktop_application_1_1_model_1_1_pizza_model =
[
    [ "CopyFrom", "class_pizza_factory_1_1_desktop_application_1_1_model_1_1_pizza_model.html#a7baa52b8d5faff32c081b194a5b89a0c", null ],
    [ "FurtherExpenses", "class_pizza_factory_1_1_desktop_application_1_1_model_1_1_pizza_model.html#a00b35ff11ce1e009000025959d8d3178", null ],
    [ "Id", "class_pizza_factory_1_1_desktop_application_1_1_model_1_1_pizza_model.html#ad3654b38185c3fd271cc19821f8469c3", null ],
    [ "Name", "class_pizza_factory_1_1_desktop_application_1_1_model_1_1_pizza_model.html#a690bd55ddbe53586115323abfcaad959", null ],
    [ "Price", "class_pizza_factory_1_1_desktop_application_1_1_model_1_1_pizza_model.html#a355ac2462562fbecbefe194c3af59cdf", null ],
    [ "SaleMultiplier", "class_pizza_factory_1_1_desktop_application_1_1_model_1_1_pizza_model.html#a2a1e85ac5fd82a4b4d82135103380fa2", null ],
    [ "Size", "class_pizza_factory_1_1_desktop_application_1_1_model_1_1_pizza_model.html#a6e9d081b96e8452123fe95801e9dc409", null ],
    [ "PropertyChanged", "class_pizza_factory_1_1_desktop_application_1_1_model_1_1_pizza_model.html#a6763b609a5992fdd1574d6d4a19261c8", null ]
];