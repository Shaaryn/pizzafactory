var annotated_dup =
[
    [ "AspNetCore", "namespace_asp_net_core.html", [
      [ "Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", "class_asp_net_core_1_1_views_____view_imports" ],
      [ "Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", "class_asp_net_core_1_1_views_____view_start" ],
      [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
      [ "Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", "class_asp_net_core_1_1_views___home___privacy" ],
      [ "Views_Pizzas_PizzasDetails", "class_asp_net_core_1_1_views___pizzas___pizzas_details.html", "class_asp_net_core_1_1_views___pizzas___pizzas_details" ],
      [ "Views_Pizzas_PizzasEdit", "class_asp_net_core_1_1_views___pizzas___pizzas_edit.html", "class_asp_net_core_1_1_views___pizzas___pizzas_edit" ],
      [ "Views_Pizzas_PizzasIndex", "class_asp_net_core_1_1_views___pizzas___pizzas_index.html", "class_asp_net_core_1_1_views___pizzas___pizzas_index" ],
      [ "Views_Pizzas_PizzasList", "class_asp_net_core_1_1_views___pizzas___pizzas_list.html", "class_asp_net_core_1_1_views___pizzas___pizzas_list" ],
      [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ],
      [ "Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial" ],
      [ "Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", "class_asp_net_core_1_1_views___shared___error" ]
    ] ],
    [ "PizzaFactory", "namespace_pizza_factory.html", [
      [ "Data", "namespace_pizza_factory_1_1_data.html", [
        [ "Models", "namespace_pizza_factory_1_1_data_1_1_models.html", [
          [ "Connector", "namespace_pizza_factory_1_1_data_1_1_models_1_1_connector.html", [
            [ "OrderPizzaConnector", "class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_order_pizza_connector.html", "class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_order_pizza_connector" ],
            [ "PizzaIngredientConnector", "class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_pizza_ingredient_connector.html", "class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_pizza_ingredient_connector" ]
          ] ],
          [ "Human", "namespace_pizza_factory_1_1_data_1_1_models_1_1_human.html", [
            [ "Customer", "class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_customer.html", "class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_customer" ],
            [ "Employee", "class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_employee.html", "class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_employee" ]
          ] ],
          [ "Person", "class_pizza_factory_1_1_data_1_1_models_1_1_person.html", "class_pizza_factory_1_1_data_1_1_models_1_1_person" ],
          [ "Ingredient", "class_pizza_factory_1_1_data_1_1_models_1_1_ingredient.html", "class_pizza_factory_1_1_data_1_1_models_1_1_ingredient" ],
          [ "Order", "class_pizza_factory_1_1_data_1_1_models_1_1_order.html", "class_pizza_factory_1_1_data_1_1_models_1_1_order" ],
          [ "Pizza", "class_pizza_factory_1_1_data_1_1_models_1_1_pizza.html", "class_pizza_factory_1_1_data_1_1_models_1_1_pizza" ],
          [ "PizzaFactoryDbContext", "class_pizza_factory_1_1_data_1_1_models_1_1_pizza_factory_db_context.html", "class_pizza_factory_1_1_data_1_1_models_1_1_pizza_factory_db_context" ],
          [ "Pizzeria", "class_pizza_factory_1_1_data_1_1_models_1_1_pizzeria.html", "class_pizza_factory_1_1_data_1_1_models_1_1_pizzeria" ]
        ] ]
      ] ],
      [ "DesktopApplication", "namespace_pizza_factory_1_1_desktop_application.html", [
        [ "BusinessLogic", "namespace_pizza_factory_1_1_desktop_application_1_1_business_logic.html", [
          [ "IModifyService", "interface_pizza_factory_1_1_desktop_application_1_1_business_logic_1_1_i_modify_service.html", "interface_pizza_factory_1_1_desktop_application_1_1_business_logic_1_1_i_modify_service" ],
          [ "IPizzaLogic", "interface_pizza_factory_1_1_desktop_application_1_1_business_logic_1_1_i_pizza_logic.html", "interface_pizza_factory_1_1_desktop_application_1_1_business_logic_1_1_i_pizza_logic" ],
          [ "PizzaLogic", "class_pizza_factory_1_1_desktop_application_1_1_business_logic_1_1_pizza_logic.html", "class_pizza_factory_1_1_desktop_application_1_1_business_logic_1_1_pizza_logic" ]
        ] ],
        [ "Model", "namespace_pizza_factory_1_1_desktop_application_1_1_model.html", [
          [ "Conversion", "namespace_pizza_factory_1_1_desktop_application_1_1_model_1_1_conversion.html", [
            [ "IPizzaMapper", "interface_pizza_factory_1_1_desktop_application_1_1_model_1_1_conversion_1_1_i_pizza_mapper.html", "interface_pizza_factory_1_1_desktop_application_1_1_model_1_1_conversion_1_1_i_pizza_mapper" ],
            [ "PizzaMapper", "class_pizza_factory_1_1_desktop_application_1_1_model_1_1_conversion_1_1_pizza_mapper.html", "class_pizza_factory_1_1_desktop_application_1_1_model_1_1_conversion_1_1_pizza_mapper" ]
          ] ],
          [ "PizzaModel", "class_pizza_factory_1_1_desktop_application_1_1_model_1_1_pizza_model.html", "class_pizza_factory_1_1_desktop_application_1_1_model_1_1_pizza_model" ]
        ] ],
        [ "UserInterface", "namespace_pizza_factory_1_1_desktop_application_1_1_user_interface.html", [
          [ "ModifyWindow", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_modify_window.html", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_modify_window" ],
          [ "ExpensesToStringConverter", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_expenses_to_string_converter.html", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_expenses_to_string_converter" ],
          [ "ModifyServiceViaWindow", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_modify_service_via_window.html", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_modify_service_via_window" ],
          [ "SaleToStringConverter", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_sale_to_string_converter.html", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_sale_to_string_converter" ],
          [ "SizeToStringConverter", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_size_to_string_converter.html", "class_pizza_factory_1_1_desktop_application_1_1_user_interface_1_1_size_to_string_converter" ]
        ] ],
        [ "ViewModel", "namespace_pizza_factory_1_1_desktop_application_1_1_view_model.html", [
          [ "MainViewModel", "class_pizza_factory_1_1_desktop_application_1_1_view_model_1_1_main_view_model.html", "class_pizza_factory_1_1_desktop_application_1_1_view_model_1_1_main_view_model" ],
          [ "ModifyViewModel", "class_pizza_factory_1_1_desktop_application_1_1_view_model_1_1_modify_view_model.html", "class_pizza_factory_1_1_desktop_application_1_1_view_model_1_1_modify_view_model" ]
        ] ],
        [ "App", "class_pizza_factory_1_1_desktop_application_1_1_app.html", "class_pizza_factory_1_1_desktop_application_1_1_app" ],
        [ "MainWindow", "class_pizza_factory_1_1_desktop_application_1_1_main_window.html", "class_pizza_factory_1_1_desktop_application_1_1_main_window" ],
        [ "PizzaFactoryIOC", "class_pizza_factory_1_1_desktop_application_1_1_pizza_factory_i_o_c.html", "class_pizza_factory_1_1_desktop_application_1_1_pizza_factory_i_o_c" ]
      ] ],
      [ "Logic", "namespace_pizza_factory_1_1_logic.html", [
        [ "Classes", "namespace_pizza_factory_1_1_logic_1_1_classes.html", [
          [ "LogIn", "class_pizza_factory_1_1_logic_1_1_classes_1_1_log_in.html", "class_pizza_factory_1_1_logic_1_1_classes_1_1_log_in" ],
          [ "OrderManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management.html", "class_pizza_factory_1_1_logic_1_1_classes_1_1_order_management" ],
          [ "PizzaManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management.html", "class_pizza_factory_1_1_logic_1_1_classes_1_1_pizza_management" ],
          [ "RenewableEmployees", "class_pizza_factory_1_1_logic_1_1_classes_1_1_renewable_employees.html", "class_pizza_factory_1_1_logic_1_1_classes_1_1_renewable_employees" ],
          [ "StatisticsManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_statistics_management.html", "class_pizza_factory_1_1_logic_1_1_classes_1_1_statistics_management" ],
          [ "UserManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_user_management.html", "class_pizza_factory_1_1_logic_1_1_classes_1_1_user_management" ]
        ] ],
        [ "Exceptions", "namespace_pizza_factory_1_1_logic_1_1_exceptions.html", [
          [ "LogInEmployeeNotFoundException", "class_pizza_factory_1_1_logic_1_1_exceptions_1_1_log_in_employee_not_found_exception.html", "class_pizza_factory_1_1_logic_1_1_exceptions_1_1_log_in_employee_not_found_exception" ]
        ] ],
        [ "Interfaces", "namespace_pizza_factory_1_1_logic_1_1_interfaces.html", [
          [ "ILogIn", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_log_in.html", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_log_in" ],
          [ "IOrderManagement", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management.html", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_order_management" ],
          [ "IPizzaManagement", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management.html", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_pizza_management" ],
          [ "IStatisticsManagement", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_statistics_management.html", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_statistics_management" ],
          [ "IUserManagement", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_user_management.html", "interface_pizza_factory_1_1_logic_1_1_interfaces_1_1_i_user_management" ]
        ] ],
        [ "Tests", "namespace_pizza_factory_1_1_logic_1_1_tests.html", [
          [ "Classes", "namespace_pizza_factory_1_1_logic_1_1_tests_1_1_classes.html", [
            [ "OrderManagementTests", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_order_management_tests.html", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_order_management_tests" ],
            [ "PizzaManagementTests", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_pizza_management_tests.html", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_pizza_management_tests" ],
            [ "StatisticsManagementTests", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_statistics_management_tests.html", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_statistics_management_tests" ],
            [ "UserManagementTests", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_user_management_tests.html", "class_pizza_factory_1_1_logic_1_1_tests_1_1_classes_1_1_user_management_tests" ]
          ] ],
          [ "MainTest", "class_pizza_factory_1_1_logic_1_1_tests_1_1_main_test.html", "class_pizza_factory_1_1_logic_1_1_tests_1_1_main_test" ]
        ] ]
      ] ],
      [ "Program", "namespace_pizza_factory_1_1_program.html", [
        [ "Classes", "namespace_pizza_factory_1_1_program_1_1_classes.html", [
          [ "OrderManagementView", "class_pizza_factory_1_1_program_1_1_classes_1_1_order_management_view.html", "class_pizza_factory_1_1_program_1_1_classes_1_1_order_management_view" ],
          [ "PizzaManagementView", "class_pizza_factory_1_1_program_1_1_classes_1_1_pizza_management_view.html", "class_pizza_factory_1_1_program_1_1_classes_1_1_pizza_management_view" ],
          [ "StatisticsManagementView", "class_pizza_factory_1_1_program_1_1_classes_1_1_statistics_management_view.html", "class_pizza_factory_1_1_program_1_1_classes_1_1_statistics_management_view" ],
          [ "UserManagementView", "class_pizza_factory_1_1_program_1_1_classes_1_1_user_management_view.html", "class_pizza_factory_1_1_program_1_1_classes_1_1_user_management_view" ]
        ] ],
        [ "Interfaces", "namespace_pizza_factory_1_1_program_1_1_interfaces.html", [
          [ "IOrderManagementView", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_order_management_view.html", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_order_management_view" ],
          [ "IPizzaManagementView", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_pizza_management_view.html", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_pizza_management_view" ],
          [ "IStatisticsManagementView", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_statistics_management_view.html", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_statistics_management_view" ],
          [ "IUserManagementView", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_user_management_view.html", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_user_management_view" ]
        ] ]
      ] ],
      [ "Repository", "namespace_pizza_factory_1_1_repository.html", [
        [ "Classes", "namespace_pizza_factory_1_1_repository_1_1_classes.html", [
          [ "ConnectorOrderPizzaRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_connector_order_pizza_repository.html", "class_pizza_factory_1_1_repository_1_1_classes_1_1_connector_order_pizza_repository" ],
          [ "ConnectorPizzaIngredientRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_connector_pizza_ingredient_repository.html", "class_pizza_factory_1_1_repository_1_1_classes_1_1_connector_pizza_ingredient_repository" ],
          [ "CustomerRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_customer_repository.html", "class_pizza_factory_1_1_repository_1_1_classes_1_1_customer_repository" ],
          [ "EmployeeRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_employee_repository.html", "class_pizza_factory_1_1_repository_1_1_classes_1_1_employee_repository" ],
          [ "IngredientRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_ingredient_repository.html", "class_pizza_factory_1_1_repository_1_1_classes_1_1_ingredient_repository" ],
          [ "OrderRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_order_repository.html", "class_pizza_factory_1_1_repository_1_1_classes_1_1_order_repository" ],
          [ "PersonRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_person_repository.html", "class_pizza_factory_1_1_repository_1_1_classes_1_1_person_repository" ],
          [ "PizzaRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_pizza_repository.html", "class_pizza_factory_1_1_repository_1_1_classes_1_1_pizza_repository" ],
          [ "PizzeriaRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_pizzeria_repository.html", "class_pizza_factory_1_1_repository_1_1_classes_1_1_pizzeria_repository" ],
          [ "RepositoryBase", "class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base.html", "class_pizza_factory_1_1_repository_1_1_classes_1_1_repository_base" ]
        ] ],
        [ "Exceptions", "namespace_pizza_factory_1_1_repository_1_1_exceptions.html", [
          [ "EmployeeAlreadyTerminatedException", "class_pizza_factory_1_1_repository_1_1_exceptions_1_1_employee_already_terminated_exception.html", "class_pizza_factory_1_1_repository_1_1_exceptions_1_1_employee_already_terminated_exception" ],
          [ "EmployeeNotFoundException", "class_pizza_factory_1_1_repository_1_1_exceptions_1_1_employee_not_found_exception.html", "class_pizza_factory_1_1_repository_1_1_exceptions_1_1_employee_not_found_exception" ],
          [ "UnderValueException", "class_pizza_factory_1_1_repository_1_1_exceptions_1_1_under_value_exception.html", "class_pizza_factory_1_1_repository_1_1_exceptions_1_1_under_value_exception" ]
        ] ],
        [ "Interfaces", "namespace_pizza_factory_1_1_repository_1_1_interfaces.html", [
          [ "IConnectorOrderPizzaRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_connector_order_pizza_repository.html", null ],
          [ "IConnectorPizzaIngredientRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_connector_pizza_ingredient_repository.html", null ],
          [ "ICustomerRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_customer_repository.html", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_customer_repository" ],
          [ "IEmployeeRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_employee_repository.html", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_employee_repository" ],
          [ "IIngredientRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_ingredient_repository.html", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_ingredient_repository" ],
          [ "IOrderRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_order_repository.html", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_order_repository" ],
          [ "IPizzaRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_pizza_repository.html", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_pizza_repository" ],
          [ "IPizzeriaRepository", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_pizzeria_repository.html", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_pizzeria_repository" ]
        ] ],
        [ "IPersonRepository", "interface_pizza_factory_1_1_repository_1_1_i_person_repository.html", "interface_pizza_factory_1_1_repository_1_1_i_person_repository" ],
        [ "IRepositoryBase", "interface_pizza_factory_1_1_repository_1_1_i_repository_base.html", "interface_pizza_factory_1_1_repository_1_1_i_repository_base" ]
      ] ],
      [ "Web", "namespace_pizza_factory_1_1_web.html", [
        [ "Controllers", "namespace_pizza_factory_1_1_web_1_1_controllers.html", [
          [ "HomeController", "class_pizza_factory_1_1_web_1_1_controllers_1_1_home_controller.html", "class_pizza_factory_1_1_web_1_1_controllers_1_1_home_controller" ],
          [ "PizzaAPIController", "class_pizza_factory_1_1_web_1_1_controllers_1_1_pizza_a_p_i_controller.html", "class_pizza_factory_1_1_web_1_1_controllers_1_1_pizza_a_p_i_controller" ],
          [ "PizzasController", "class_pizza_factory_1_1_web_1_1_controllers_1_1_pizzas_controller.html", "class_pizza_factory_1_1_web_1_1_controllers_1_1_pizzas_controller" ]
        ] ],
        [ "Models", "namespace_pizza_factory_1_1_web_1_1_models.html", [
          [ "ApiResult", "class_pizza_factory_1_1_web_1_1_models_1_1_api_result.html", "class_pizza_factory_1_1_web_1_1_models_1_1_api_result" ],
          [ "ErrorViewModel", "class_pizza_factory_1_1_web_1_1_models_1_1_error_view_model.html", "class_pizza_factory_1_1_web_1_1_models_1_1_error_view_model" ],
          [ "MapperFactory", "class_pizza_factory_1_1_web_1_1_models_1_1_mapper_factory.html", "class_pizza_factory_1_1_web_1_1_models_1_1_mapper_factory" ],
          [ "Pizza", "class_pizza_factory_1_1_web_1_1_models_1_1_pizza.html", "class_pizza_factory_1_1_web_1_1_models_1_1_pizza" ],
          [ "PizzasViewModel", "class_pizza_factory_1_1_web_1_1_models_1_1_pizzas_view_model.html", "class_pizza_factory_1_1_web_1_1_models_1_1_pizzas_view_model" ]
        ] ],
        [ "Program", "class_pizza_factory_1_1_web_1_1_program.html", "class_pizza_factory_1_1_web_1_1_program" ],
        [ "Startup", "class_pizza_factory_1_1_web_1_1_startup.html", "class_pizza_factory_1_1_web_1_1_startup" ]
      ] ],
      [ "WpfClient", "namespace_pizza_factory_1_1_wpf_client.html", [
        [ "App", "class_pizza_factory_1_1_wpf_client_1_1_app.html", "class_pizza_factory_1_1_wpf_client_1_1_app" ],
        [ "EditorWindow", "class_pizza_factory_1_1_wpf_client_1_1_editor_window.html", "class_pizza_factory_1_1_wpf_client_1_1_editor_window" ],
        [ "MainLogic", "class_pizza_factory_1_1_wpf_client_1_1_main_logic.html", "class_pizza_factory_1_1_wpf_client_1_1_main_logic" ],
        [ "MainVM", "class_pizza_factory_1_1_wpf_client_1_1_main_v_m.html", "class_pizza_factory_1_1_wpf_client_1_1_main_v_m" ],
        [ "MainWindow", "class_pizza_factory_1_1_wpf_client_1_1_main_window.html", "class_pizza_factory_1_1_wpf_client_1_1_main_window" ],
        [ "PizzaVM", "class_pizza_factory_1_1_wpf_client_1_1_pizza_v_m.html", "class_pizza_factory_1_1_wpf_client_1_1_pizza_v_m" ]
      ] ],
      [ "MainProgram", "class_pizza_factory_1_1_main_program.html", "class_pizza_factory_1_1_main_program" ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];