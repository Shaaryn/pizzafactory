var namespace_pizza_factory_1_1_program_1_1_interfaces =
[
    [ "IOrderManagementView", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_order_management_view.html", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_order_management_view" ],
    [ "IPizzaManagementView", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_pizza_management_view.html", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_pizza_management_view" ],
    [ "IStatisticsManagementView", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_statistics_management_view.html", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_statistics_management_view" ],
    [ "IUserManagementView", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_user_management_view.html", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_user_management_view" ]
];