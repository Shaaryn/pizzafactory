var namespace_pizza_factory_1_1_data_1_1_models =
[
    [ "Connector", "namespace_pizza_factory_1_1_data_1_1_models_1_1_connector.html", "namespace_pizza_factory_1_1_data_1_1_models_1_1_connector" ],
    [ "Human", "namespace_pizza_factory_1_1_data_1_1_models_1_1_human.html", "namespace_pizza_factory_1_1_data_1_1_models_1_1_human" ],
    [ "Person", "class_pizza_factory_1_1_data_1_1_models_1_1_person.html", "class_pizza_factory_1_1_data_1_1_models_1_1_person" ],
    [ "Ingredient", "class_pizza_factory_1_1_data_1_1_models_1_1_ingredient.html", "class_pizza_factory_1_1_data_1_1_models_1_1_ingredient" ],
    [ "Order", "class_pizza_factory_1_1_data_1_1_models_1_1_order.html", "class_pizza_factory_1_1_data_1_1_models_1_1_order" ],
    [ "Pizza", "class_pizza_factory_1_1_data_1_1_models_1_1_pizza.html", "class_pizza_factory_1_1_data_1_1_models_1_1_pizza" ],
    [ "PizzaFactoryDbContext", "class_pizza_factory_1_1_data_1_1_models_1_1_pizza_factory_db_context.html", "class_pizza_factory_1_1_data_1_1_models_1_1_pizza_factory_db_context" ],
    [ "Pizzeria", "class_pizza_factory_1_1_data_1_1_models_1_1_pizzeria.html", "class_pizza_factory_1_1_data_1_1_models_1_1_pizzeria" ]
];