var interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_pizza_repository =
[
    [ "ChangeExpenses", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_pizza_repository.html#a12e05064c6ca6b303dc848213f73a01f", null ],
    [ "ChangePizza", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_pizza_repository.html#ae84d6c0dcce17c50b0425489f1c27653", null ],
    [ "ChangePizzaName", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_pizza_repository.html#a8bc6bee8c7884c9cfb8264c97a3fbd27", null ],
    [ "ChangePrice", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_pizza_repository.html#ab93880513425eaa68bd92ae58f1abfad", null ],
    [ "ChangeSale", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_pizza_repository.html#a5f47f5628a454a5d9c52288ff81a6c4c", null ],
    [ "ChangeSize", "interface_pizza_factory_1_1_repository_1_1_interfaces_1_1_i_pizza_repository.html#a11174bdb8da859341a20ccfe600299c1", null ]
];