var dir_45fb3477b34fdebf81fa946b84ce14f7 =
[
    [ "IOrderManagementView.cs", "_i_order_management_view_8cs.html", [
      [ "IOrderManagementView", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_order_management_view.html", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_order_management_view" ]
    ] ],
    [ "IPizzaManagementView.cs", "_i_pizza_management_view_8cs.html", [
      [ "IPizzaManagementView", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_pizza_management_view.html", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_pizza_management_view" ]
    ] ],
    [ "IStatisticsManagementView.cs", "_i_statistics_management_view_8cs.html", [
      [ "IStatisticsManagementView", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_statistics_management_view.html", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_statistics_management_view" ]
    ] ],
    [ "IUserManagementView.cs", "_i_user_management_view_8cs.html", [
      [ "IUserManagementView", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_user_management_view.html", "interface_pizza_factory_1_1_program_1_1_interfaces_1_1_i_user_management_view" ]
    ] ]
];