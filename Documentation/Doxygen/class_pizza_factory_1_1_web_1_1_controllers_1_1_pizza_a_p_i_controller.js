var class_pizza_factory_1_1_web_1_1_controllers_1_1_pizza_a_p_i_controller =
[
    [ "PizzaAPIController", "class_pizza_factory_1_1_web_1_1_controllers_1_1_pizza_a_p_i_controller.html#a98e9f41a1f0edb139bfb24b78e5d1fe2", null ],
    [ "AddAPizza", "class_pizza_factory_1_1_web_1_1_controllers_1_1_pizza_a_p_i_controller.html#a4e3d2f822aff57ec0e3e7390977fd384", null ],
    [ "DeleteAPizza", "class_pizza_factory_1_1_web_1_1_controllers_1_1_pizza_a_p_i_controller.html#a7a4b5b134aea3b1a09a1c6bb0574bb47", null ],
    [ "GetAll", "class_pizza_factory_1_1_web_1_1_controllers_1_1_pizza_a_p_i_controller.html#a93a28152fe843f077b5c18eb28b2c679", null ],
    [ "ModifyAPizza", "class_pizza_factory_1_1_web_1_1_controllers_1_1_pizza_a_p_i_controller.html#a6a2fd8a5604a8671030fc93828f423fe", null ]
];