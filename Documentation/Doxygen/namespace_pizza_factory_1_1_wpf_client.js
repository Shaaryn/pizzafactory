var namespace_pizza_factory_1_1_wpf_client =
[
    [ "App", "class_pizza_factory_1_1_wpf_client_1_1_app.html", "class_pizza_factory_1_1_wpf_client_1_1_app" ],
    [ "EditorWindow", "class_pizza_factory_1_1_wpf_client_1_1_editor_window.html", "class_pizza_factory_1_1_wpf_client_1_1_editor_window" ],
    [ "MainLogic", "class_pizza_factory_1_1_wpf_client_1_1_main_logic.html", "class_pizza_factory_1_1_wpf_client_1_1_main_logic" ],
    [ "MainVM", "class_pizza_factory_1_1_wpf_client_1_1_main_v_m.html", "class_pizza_factory_1_1_wpf_client_1_1_main_v_m" ],
    [ "MainWindow", "class_pizza_factory_1_1_wpf_client_1_1_main_window.html", "class_pizza_factory_1_1_wpf_client_1_1_main_window" ],
    [ "PizzaVM", "class_pizza_factory_1_1_wpf_client_1_1_pizza_v_m.html", "class_pizza_factory_1_1_wpf_client_1_1_pizza_v_m" ]
];