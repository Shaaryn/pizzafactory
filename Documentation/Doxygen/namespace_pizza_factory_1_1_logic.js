var namespace_pizza_factory_1_1_logic =
[
    [ "Classes", "namespace_pizza_factory_1_1_logic_1_1_classes.html", "namespace_pizza_factory_1_1_logic_1_1_classes" ],
    [ "Exceptions", "namespace_pizza_factory_1_1_logic_1_1_exceptions.html", "namespace_pizza_factory_1_1_logic_1_1_exceptions" ],
    [ "Interfaces", "namespace_pizza_factory_1_1_logic_1_1_interfaces.html", "namespace_pizza_factory_1_1_logic_1_1_interfaces" ],
    [ "Tests", "namespace_pizza_factory_1_1_logic_1_1_tests.html", "namespace_pizza_factory_1_1_logic_1_1_tests" ]
];