var class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_pizza_ingredient_connector =
[
    [ "Id", "class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_pizza_ingredient_connector.html#a8b49207d37c1c0c31cd66ff55ce546a4", null ],
    [ "Ingredient", "class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_pizza_ingredient_connector.html#a74860704b4c042485bce071717ffc6fa", null ],
    [ "IngredientId", "class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_pizza_ingredient_connector.html#a85e8399d56d41361f3e53b4d40c77efb", null ],
    [ "Pizza", "class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_pizza_ingredient_connector.html#a142e22701cb13eb1f58aee6aa7656999", null ],
    [ "PizzaId", "class_pizza_factory_1_1_data_1_1_models_1_1_connector_1_1_pizza_ingredient_connector.html#a87bedf0b8cf2c64cce8e6aace18a7e87", null ]
];