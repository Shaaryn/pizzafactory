var class_pizza_factory_1_1_program_1_1_classes_1_1_pizza_management_view =
[
    [ "AddPizza", "class_pizza_factory_1_1_program_1_1_classes_1_1_pizza_management_view.html#ad1a4254e3f6ff884841881cb6cd77291", null ],
    [ "FinalizePizzaAddition", "class_pizza_factory_1_1_program_1_1_classes_1_1_pizza_management_view.html#a40fb0e2ed9ff8b0bb94a25538f8d5016", null ],
    [ "ManageIngredientSelection", "class_pizza_factory_1_1_program_1_1_classes_1_1_pizza_management_view.html#ab532b2a1b7da1a18deb6c4806d0ac982", null ],
    [ "ModifyOptions", "class_pizza_factory_1_1_program_1_1_classes_1_1_pizza_management_view.html#af327105c24d371b0cf3b2b81366bbe59", null ],
    [ "ModifyProcess", "class_pizza_factory_1_1_program_1_1_classes_1_1_pizza_management_view.html#aba08560482e6f395ff93d6f8ff620e2e", null ],
    [ "PizzaAdditionPreview", "class_pizza_factory_1_1_program_1_1_classes_1_1_pizza_management_view.html#abfddcd8024dd0260200a2482f15430d8", null ],
    [ "SelectIngredients", "class_pizza_factory_1_1_program_1_1_classes_1_1_pizza_management_view.html#a58f83432048da4df9cf5d0b49b570c86", null ],
    [ "SuccessfullOperationMessage", "class_pizza_factory_1_1_program_1_1_classes_1_1_pizza_management_view.html#ad001a8638d69eb0b10556b533e3979a2", null ],
    [ "ViewPizzas", "class_pizza_factory_1_1_program_1_1_classes_1_1_pizza_management_view.html#a5a5e483a4b70683831f541d4bd709c52", null ]
];