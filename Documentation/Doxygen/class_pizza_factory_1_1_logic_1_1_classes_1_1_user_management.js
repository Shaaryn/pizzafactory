var class_pizza_factory_1_1_logic_1_1_classes_1_1_user_management =
[
    [ "UserManagement", "class_pizza_factory_1_1_logic_1_1_classes_1_1_user_management.html#a3449a2cb08cb95636c3503b44d103ae7", null ],
    [ "AddEmployee", "class_pizza_factory_1_1_logic_1_1_classes_1_1_user_management.html#ab0ceab06f0da94560cd7d8eac344db66", null ],
    [ "GetAllUsers", "class_pizza_factory_1_1_logic_1_1_classes_1_1_user_management.html#abdb56df59531fe217687214d8fecf419", null ],
    [ "ModifyEmployeePerson", "class_pizza_factory_1_1_logic_1_1_classes_1_1_user_management.html#a0d8c5bf41b6bcce4317b79e4df568b8f", null ],
    [ "ModifyEmployeeRole", "class_pizza_factory_1_1_logic_1_1_classes_1_1_user_management.html#abd91589e2e7222f546f945fee9829397", null ]
];