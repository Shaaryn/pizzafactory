var class_pizza_factory_1_1_desktop_application_1_1_business_logic_1_1_pizza_logic =
[
    [ "PizzaLogic", "class_pizza_factory_1_1_desktop_application_1_1_business_logic_1_1_pizza_logic.html#addd0a8c4a47352e27d8d36b74d4fe340", null ],
    [ "AddPizza", "class_pizza_factory_1_1_desktop_application_1_1_business_logic_1_1_pizza_logic.html#a6f99399727244c6a8518caa4ccca270f", null ],
    [ "ModifyPizza", "class_pizza_factory_1_1_desktop_application_1_1_business_logic_1_1_pizza_logic.html#a049da711766d126dd5407ae2aa6c5e89", null ],
    [ "RemovePizza", "class_pizza_factory_1_1_desktop_application_1_1_business_logic_1_1_pizza_logic.html#adf1d82fa08e348bb6febbfa456a20deb", null ]
];