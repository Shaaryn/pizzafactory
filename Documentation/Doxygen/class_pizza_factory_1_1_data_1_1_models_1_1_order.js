var class_pizza_factory_1_1_data_1_1_models_1_1_order =
[
    [ "Equals", "class_pizza_factory_1_1_data_1_1_models_1_1_order.html#a60aa1392695d9ed5401a159c4fe981a5", null ],
    [ "GetHashCode", "class_pizza_factory_1_1_data_1_1_models_1_1_order.html#a3dea254eb2569e0832defc660e1f7123", null ],
    [ "ToString", "class_pizza_factory_1_1_data_1_1_models_1_1_order.html#a1d7d35fcc340748606a8f8645a717d73", null ],
    [ "Connector", "class_pizza_factory_1_1_data_1_1_models_1_1_order.html#ad80754f2286a25153cbbc5cbc6597ccc", null ],
    [ "Cost", "class_pizza_factory_1_1_data_1_1_models_1_1_order.html#a6e5f5d7215eef2e0149bf52443346305", null ],
    [ "Customer", "class_pizza_factory_1_1_data_1_1_models_1_1_order.html#af51c8a83bd0fd4b00cd3352cbf9ad963", null ],
    [ "CustomerId", "class_pizza_factory_1_1_data_1_1_models_1_1_order.html#a668df84d4fa2c964cf2ddfc523856af6", null ],
    [ "Employee", "class_pizza_factory_1_1_data_1_1_models_1_1_order.html#af1337dadf90cb0296760474d33eb74d9", null ],
    [ "EmployeeId", "class_pizza_factory_1_1_data_1_1_models_1_1_order.html#a981ece0f770916c52e3954135d8f452e", null ],
    [ "Id", "class_pizza_factory_1_1_data_1_1_models_1_1_order.html#a0dc014ecc85cfa4a32630a12eee71efc", null ],
    [ "IsStorno", "class_pizza_factory_1_1_data_1_1_models_1_1_order.html#a66903910f22cf8314bd7bae320db1088", null ],
    [ "Pizzeria", "class_pizza_factory_1_1_data_1_1_models_1_1_order.html#a75c0e75a55972c969c460f0c53cbbb9b", null ],
    [ "PizzeriaId", "class_pizza_factory_1_1_data_1_1_models_1_1_order.html#a6a50770cf744769c4abc72ad2e4faf98", null ],
    [ "TimeStamp", "class_pizza_factory_1_1_data_1_1_models_1_1_order.html#a42986a5ee46f8f008b532c706d5cc31e", null ]
];