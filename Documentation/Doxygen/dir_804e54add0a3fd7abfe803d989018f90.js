var dir_804e54add0a3fd7abfe803d989018f90 =
[
    [ "PizzaFactory", "dir_f34e2f3faf1a8ee5c8feaa62cfcc1cd8.html", "dir_f34e2f3faf1a8ee5c8feaa62cfcc1cd8" ],
    [ "PizzaFactory.Data", "dir_31352a3e94ca1935c3c72b6a8f8bae14.html", "dir_31352a3e94ca1935c3c72b6a8f8bae14" ],
    [ "PizzaFactory.DesktopApplication", "dir_b9152905fd3c0b12f05b6470d8c67914.html", "dir_b9152905fd3c0b12f05b6470d8c67914" ],
    [ "PizzaFactory.Logic", "dir_c702d2686a2290a1401161fb336017f6.html", "dir_c702d2686a2290a1401161fb336017f6" ],
    [ "PizzaFactory.Repository", "dir_6c4dcbe4f1197a90c9791af0fe25e6f3.html", "dir_6c4dcbe4f1197a90c9791af0fe25e6f3" ],
    [ "PizzaFactory.Tests", "dir_39771b34cbf12d376829c4845b00b192.html", "dir_39771b34cbf12d376829c4845b00b192" ],
    [ "PizzaFactory.Web", "dir_1cc34f46e9800c65f55470a55e04393a.html", "dir_1cc34f46e9800c65f55470a55e04393a" ],
    [ "PizzaFactory.WpfClient", "dir_9ebc42b496f82333105d7da94467f6c9.html", "dir_9ebc42b496f82333105d7da94467f6c9" ]
];