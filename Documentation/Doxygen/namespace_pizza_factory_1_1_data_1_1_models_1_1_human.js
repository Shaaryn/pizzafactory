var namespace_pizza_factory_1_1_data_1_1_models_1_1_human =
[
    [ "Customer", "class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_customer.html", "class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_customer" ],
    [ "Employee", "class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_employee.html", "class_pizza_factory_1_1_data_1_1_models_1_1_human_1_1_employee" ],
    [ "Authorization", "namespace_pizza_factory_1_1_data_1_1_models_1_1_human.html#aece6785cbd767ead705945adcc53c960", [
      [ "Admin", "namespace_pizza_factory_1_1_data_1_1_models_1_1_human.html#aece6785cbd767ead705945adcc53c960ae3afed0047b08059d0fada10f400c1e5", null ],
      [ "Manager", "namespace_pizza_factory_1_1_data_1_1_models_1_1_human.html#aece6785cbd767ead705945adcc53c960aae94be3cd532ce4a025884819eb08c98", null ],
      [ "User", "namespace_pizza_factory_1_1_data_1_1_models_1_1_human.html#aece6785cbd767ead705945adcc53c960a8f9bfe9d1345237cb3b2b205864da075", null ],
      [ "Assistant", "namespace_pizza_factory_1_1_data_1_1_models_1_1_human.html#aece6785cbd767ead705945adcc53c960a9b1363da9503dbd4142c0274a88e8d4b", null ],
      [ "RoF", "namespace_pizza_factory_1_1_data_1_1_models_1_1_human.html#aece6785cbd767ead705945adcc53c960a80fa051e49d3f1bb3c9853315cf929c9", null ]
    ] ],
    [ "Role", "namespace_pizza_factory_1_1_data_1_1_models_1_1_human.html#ab8de714822d9aef11b02261742a1afcb", [
      [ "Admin", "namespace_pizza_factory_1_1_data_1_1_models_1_1_human.html#ab8de714822d9aef11b02261742a1afcbae3afed0047b08059d0fada10f400c1e5", null ],
      [ "Smanager", "namespace_pizza_factory_1_1_data_1_1_models_1_1_human.html#ab8de714822d9aef11b02261742a1afcbae4547c22fcac528d1e0eee6338c264f9", null ],
      [ "Dispatcher", "namespace_pizza_factory_1_1_data_1_1_models_1_1_human.html#ab8de714822d9aef11b02261742a1afcbadbfcc2e96980bb87c34df3809193c62a", null ],
      [ "Cook", "namespace_pizza_factory_1_1_data_1_1_models_1_1_human.html#ab8de714822d9aef11b02261742a1afcba1bf70a91b0b28b92dc52e03d45abf61a", null ],
      [ "Assistant", "namespace_pizza_factory_1_1_data_1_1_models_1_1_human.html#ab8de714822d9aef11b02261742a1afcba9b1363da9503dbd4142c0274a88e8d4b", null ],
      [ "RoF", "namespace_pizza_factory_1_1_data_1_1_models_1_1_human.html#ab8de714822d9aef11b02261742a1afcba80fa051e49d3f1bb3c9853315cf929c9", null ]
    ] ]
];