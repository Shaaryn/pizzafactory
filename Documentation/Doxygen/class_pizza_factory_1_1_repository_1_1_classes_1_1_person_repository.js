var class_pizza_factory_1_1_repository_1_1_classes_1_1_person_repository =
[
    [ "PersonRepository", "class_pizza_factory_1_1_repository_1_1_classes_1_1_person_repository.html#ae0666a61765a7b31aaac678769df065b", null ],
    [ "ChangeAddress", "class_pizza_factory_1_1_repository_1_1_classes_1_1_person_repository.html#ac18690f8b2a8c5b19795648da1664264", null ],
    [ "ChangeFullName", "class_pizza_factory_1_1_repository_1_1_classes_1_1_person_repository.html#a14de3f4fd4403f6844b5bc188ede8985", null ],
    [ "ChangePhoneNumber", "class_pizza_factory_1_1_repository_1_1_classes_1_1_person_repository.html#ace0378cbb8fc187618965eeef9cbe2e2", null ],
    [ "GetOne", "class_pizza_factory_1_1_repository_1_1_classes_1_1_person_repository.html#ad5a93d3741854eda33422dca39f84bd3", null ],
    [ "GetOneByPhone", "class_pizza_factory_1_1_repository_1_1_classes_1_1_person_repository.html#a08294fbe8d7ff79c972b950829eb87ba", null ]
];