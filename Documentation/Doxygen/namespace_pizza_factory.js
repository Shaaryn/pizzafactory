var namespace_pizza_factory =
[
    [ "Data", "namespace_pizza_factory_1_1_data.html", "namespace_pizza_factory_1_1_data" ],
    [ "DesktopApplication", "namespace_pizza_factory_1_1_desktop_application.html", "namespace_pizza_factory_1_1_desktop_application" ],
    [ "Logic", "namespace_pizza_factory_1_1_logic.html", "namespace_pizza_factory_1_1_logic" ],
    [ "Program", "namespace_pizza_factory_1_1_program.html", "namespace_pizza_factory_1_1_program" ],
    [ "Repository", "namespace_pizza_factory_1_1_repository.html", "namespace_pizza_factory_1_1_repository" ],
    [ "Web", "namespace_pizza_factory_1_1_web.html", "namespace_pizza_factory_1_1_web" ],
    [ "WpfClient", "namespace_pizza_factory_1_1_wpf_client.html", "namespace_pizza_factory_1_1_wpf_client" ],
    [ "MainProgram", "class_pizza_factory_1_1_main_program.html", "class_pizza_factory_1_1_main_program" ]
];