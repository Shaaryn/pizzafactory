var class_pizza_factory_1_1_wpf_client_1_1_main_v_m =
[
    [ "MainVM", "class_pizza_factory_1_1_wpf_client_1_1_main_v_m.html#a8c44c1efda1ab15483aef2a767b421d3", null ],
    [ "AddCmd", "class_pizza_factory_1_1_wpf_client_1_1_main_v_m.html#a7ca1d2bfad11e009d4967f64de9fb781", null ],
    [ "AllPizzas", "class_pizza_factory_1_1_wpf_client_1_1_main_v_m.html#a9eb7dba6b462a48457530bc8222a0f90", null ],
    [ "DelCmd", "class_pizza_factory_1_1_wpf_client_1_1_main_v_m.html#afc2a4afdd6db6e20fa89846b9ce0364f", null ],
    [ "EditorFunc", "class_pizza_factory_1_1_wpf_client_1_1_main_v_m.html#af1a1e5bcd9a336a94e665a87339d7044", null ],
    [ "LoadCmd", "class_pizza_factory_1_1_wpf_client_1_1_main_v_m.html#ad02004e80e8045c63d18d1bb1d59e4e4", null ],
    [ "ModCmd", "class_pizza_factory_1_1_wpf_client_1_1_main_v_m.html#a32017a1bf1ce89643a7b325a7e0c21b0", null ],
    [ "SelectedPizza", "class_pizza_factory_1_1_wpf_client_1_1_main_v_m.html#aebd963945ff548a35ae616776062801e", null ]
];