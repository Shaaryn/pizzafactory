## Pizza Factory

The app serves as a terminal for the staff of a pizzeria. First a users has to log in to the terminal, it can be either a dispatcher, the manager or the admin. 
The users -based on its authorization level- then can select from the menu.
The dispatcher can place new orders accordingly to what the customers want. Later these orders can be changed if the changes occurs within the first 10 mins from the time of placement. Otherwise it cannot be edited.
There is another menu option that takes care of the statistics. For this option we need at least a "Manager" role account. Here we can see statistical data regarding the orders, pizzas and customers.
As for user accounts, an admin account has the priviliges to modify any of the user accounts. Here in the User Management option we can change the name, address, username, password, etc properties of a user.

**1.	OrderOverview	--autLevel: (2 - User)**

1.  Order1

2.	Order2

3.	OrderN


(by selecting any of the placed orders, we can modify them, or if we have ran out of time we can storno it.)

**1.1	OrderOverview.Order**

1.	*{timeStamp}(x)*

2.	*{customerName}(x)*

3.	*{customerAddress}(x)*

4.	{orderedPizzas}

5.	{orderCost}


(attributes marked with '(x)' cannot be changed at all)

**2.	PlaceOrders		--autLevel: (2 - User)**

1.	Provide data of a customer.	-> PlaceOrder.CustomerData

2.	Choose the pizzas and their amount.	-> PlaceOrder.Pizza

3.	Place order	-> PlaceOrder.Summarize


(Place order can only be selected if the 2 above options are evaluated to true (they are both filled) )

**2.1	PlaceOrder.CustomerData**

1.	{customerName}

2.	{customerAddress}

3.	{customerPhone}


**2.2	PlaceOrder.Pizza()**

1.	<new pizza>

2.	<another new pizza if the one above is set to a value>

3.	<...>

(There is an empty pizza object initialized, after it is set to a value another empty one appears and so on)

**2.3	PlaceOrder.Summarize()**

(Displays the whole order)

1.	Place order

2.	Cancel

**3.	Statistics		--autLevel: (1 - Manager)**

1.	Most famous pizzas		(Shows the 3 best selling pizzas)
	-	By clicking on a pizza we can set it on a 20% diuscount.
	
2.	Best days				(Shows the best 3 days that the pizzeria had in the past one year interval)

3.	MVP Customers			(Shows a list of the top 5 customers, ranked by the money they have spent at our pizzeria)


**4.	UserManagement 	--autLevel: (0 - admin)**

1.	User1
2.	User2
3.	UserN

(by selecting a User, we can modify its attributes, even its authorization level.)

**4.1	UserManagement.User**

1.	{name}
2.	{address}
3.	{userName}
4.	{password}
5.	{authorization}

(by selecting an attribute, we can modify its value.)
